--	Title: Character View Expanded Stats (CaVES) v2.2
--
--	Author: Alpha_Male (alpha_male@speakeasy.net)
--
--	Description:  Character View Expanded Stats (CaVES) takes the five stats category groups that can only be viewed one
-- 								at a time in the standard Character View and displays them all at once in a single window.  This 
-- 								allows players to see the many stats that change and are affected by single or multiple equipment 
--								changes as well as other stat modification events. CaVES is a perfect tool for all classes and all  
--								levels, allowing easier optimizing of all character stats.
--	
--								CaVES adds a small unobtrusive toggle button or nub to the upper right corner of the Character View.
--								CaVES is able to be opened and closed using this nub, or it can be configured to show automatically 
--								when the Character View is shown.
--
--								The original stats window is not removed or replaced and CaVES duplicates all of the functionality 
-- 								of the original stats window, such as tooltips, and makes several improvements and enhancements. 
--								Enhancements include the display and color coding of difference values that show whether the stat has 
-- 								made a + or - change.  Additional features include a set of reference stats which allows players to make 
--								an analysis	of the effects of their equipment and stat changes compared to a set of stat values that are 
--								saved depending on the user selected options.
--
--	Features: * Ability to be easily shown/hidden
-- 						* Recent(Current) Stat Value and Difference Display
--						* Reference Stat Value and Difference Display
--					 	* Context sensitive tooltips for Reference Value description based on user selected options
--						* Stat tooltips and mouseovers from Character View also work on stats displayed in CaVES
-- 						* Color coding for stat values and difference values, including resistance caps
-- 						* +/- symbols display for easier readability and for players that have color blindness
--						* Configurable User Options
--							* Show CaVES on Character View display
--							* Stat Reference value persistency settings
--							* General and selective display of tooltips
--						* Supports Persistent Stat Reference values saved for multiple characters across multiple servers
--						* Right and left hand melee weapons stats have been separated into their own stats (4 total now). This
-- 							makes these stat values easier to read.
-- 								* Right Hand DPS
--								* Right Hand Speed
-- 								* Left Hand DPS
--								* Left Hand Speed
--								* New/updated tooltip descriptions for all of these stats
--						* Displays "power" stats and associated differences, including associated tooltip descriptions and 
--							calculated contributions for the following:
--								* Fortitude
--								* Melee Power
--								* Ranged Power
--								* Magic Power
--								* Healing Power
--						* Reference and Recent(Current) stat display reset buttons
--						* Ability to filter out or ignore Talismans installed on gear and equipment. This allows more accurate
--							comparisons of equipped gear and equipment or different load out configurations.
--						* Ability to filter out or ignore equipped Event slot item.
-- 						* Preserves original Character View stats window and functionality
-- 						* Compatible with Character View Dye Merchant, Brags, and Dungeon countdown windows/tabs
--						* Compatible with and supports the following addons:
-- 								* AnywhereTrainer
--								* RvRStatsTab
-- 								* RvRStatsUpdated
--						* Multiple Language Support (currently includes English, German, and Italian)
--						* Includes User Manual
--
--	Files: \language\CaVESLocalization.lua
--	 			 \manual\CaVES_Manual.pdf
--				 \source\CaVES.lua
--	 			 \source\CaVES.xml
--	 			 \CaVES.mod
--	 			 \CaVES_Install.txt
--	 			 \CaVES_Readme.txt
--			
--	Version History: 1.0 - Initial Release
--									 1.1 - Fixes for stat background transparency issues caused by WAR 1.2 patch
--											 - Created new tintable background .xml template with preset color and alpha
--											 - New tintable background template allowed removal of lua script that manually 
--												 set color and alpha
--											 - Removed deprecated .xml template for stat category labels
--									 1.2 - Localization updates
--											 - Added German localization
--											 - Added Italian localization
--											 - Added multiple user support when using the Persistent Reference Stats 
--												 option. Values are saved on a unique name per server basis so players
--												 can use the same character name across different servers and the values
--												 will not be overwritten.
--											 - Reworked how initial or starting Reference Stat Values were being handled,
--												 it was possible for persistent or saved values to not get saved upon choosing
--												 this option especially if the game or interface were shutdown abruptly.
--											 - Added additional hooks to handle interface shutdown better, hitting
--												 the close window button (X) when running the game in windowed mode 
--												 is handled now.
--											 - Fixed a global variable that should have been declared locally 
--												 (was not causing any issues but is proper way to set it up).
--											 - Added version to SavedVariables.lua
--											 - Removed some commented out debug lines
--											 - Reorganized variable organization	
--											 - Updated and added additional script comments	
--									 1.3 - Implemented new VersionSettings support for .mod file
--											 - Implemented new "global" optional attribute for SavedVariables used by CaVES. CaVES still 
--												 maintains its multiple characters across multiple servers saved stats and options support
--												 and it is all contained under \Warhammer Online - Age of Reckoning\user\settings\GLOBAL\CaVES. 
--												 Other instances of the SavedVariables.lua file for CaVES and its associated save directory 
--												 can be removed.
--											 - Reworked and updated stat calculations, mirroring changes made to the Character View window 
--												 in the 1.2.1 patch.
--											 - Updated resistances stats color coding introduced in 1.2.1 for diminished resistances stats. 
--												 These values appear as orange when a resistance value has hit the diminished value cap.
--											 - Fixed Defensive stats mouseover tooltips that were broken as a result of changes
--												 made to the Character Viewwindow in the 1.2.1 patch.
--											 - Added the ability to filter out or ignore Talismans that have been installed on equipment. 
--												 The option appears on the main CaVES window and its state is saved between sessions.
--											 - Added localization text for currently supported languages for new Ignore Talisman interface strings.
--											 - Created a robust and dedicated supported/compatible mods system for CaVES to allow easier
--												 mod support/compatibility in the future if necessary.
--											 - Fixed function hooks into Character View window to prevent function call chain being possibly 
--												 broken, this is more robust support for CaVES and other mods.	
--											 - Brought over AnywhereTrainer support into new mod compatiblility system
--											 - Added mod support/compatibility for RvRStatsTab by Ermite Chevelu. This should fix any issues
--												 other players were experiencing when having CaVES and this mod installed and enabled.
--											 - Added mod support/compatibility for RvRStatsUpdated. This should fix any issues
--												 other players were experiencing when having CaVES and this mod installed and enabled.
--											 - Changes and additions to declaration of global variables for stats.
--											 - General cleanup and additional script comments.
--											 - Updated CaVES_Manual.pdf and associated images.
--									 1.4 - Fix for Character Viewer window dimension bugs found in v1.3
--											 - Tightened up the bottom frame for the main CaVES window
--											 - Fix for RvRStatsTab support that adjusts Character Viewer window width properly depending on if
--												 AnywhereTrainer is installed/enabled or not installed/not enabled									 
--									 1.5 - Updated stat calculations, mirroring changes made to the Character Viewer in WAR v1.3 update
--											 - Reworked Ignore Talisman functionality due to stat calculation changes in WAR v1.3 update
--											 - Removed deprecated functions, overall due to WAR v1.3 update and CaVES changes, CaVES stat
--												 calculations and display should be faster
--									 1.6 - Floating point display fix for resists stats
--											 - Fixed critical hit bonuses for ranged, magic damage, and magic heal showing as twice their values
--									 1.7 - Added support for 1.3.1 WARInfo Categories and Careers (CaVES.mod file)
--											 - Damage Bonus tooltips working again for both Character View window and CaVES (result of Mythic Fix)
--											 - Fixed bonus value calculations for Melee Damage Bonus
--											 - Fixed bonus value calculations for Ranged Damage Bonus
--											 - Fixed bonus value calculations for Magic Damage Bonus
--											 - Fixed stat value calculation and display for Magic Critical Hit Bonus (Heal)
--											 - Fixed tooltip display for Magic Critical Hit Bonus (Heal)
--											 - Added new Healing Bonus Stat and associated tooltip/mouseover to the Magic Stat section
--											 - Changed color coding for Weapon Speed stat changes (DECREASES are GREEN, INCREASES are RED)
--											 - Fixed Melee Weapon Speed stat calculation to be inline with recent changes (both Right and Left hands)
--											 - Fixed Ranged Weapon Speed stat calculation to be inline with recent changes
--									 1.8 - Updated version to 1.3.2
--											 - New feature, ignore equipped Event item in the stats calculation
--											 - Updated localization for new ignore Event item functionality
--									 1.9 - All stats should now display soft cap color coding properly
--											 - Healing Bonus stat has been updated to no longer be displayed as a percentage
--											 - Fixed Dye Window bottom panel misalignment when CaVES is displayed (result of
--												 latest patch changes to Character View window)
--											 - Fixed additional Dye Window bottom panel misalignment issues when CaVES is displayed and
--												 AnywhereTrainer is also installed (result of latest patch changes to Character View window)
--											 - Fixed tab misalignment for RvRStatsTab when CaVES showing and AnywhereTrainer installed
--									 2.0 - Updated version to 1.3.4
--											 - Changed string formatting for version number display
--									 2.1 - Maintenance Update and Fixes
--											 - Updated version to 1.4.0
--											 - CaVES window breaking Character View window (due to patch related Character View window dimension change)
--											 - Fix for Ignore Talismans bug where items with 2 or more Talisman slots were not having
--												 the Talisman stat values and differences for slots 2 and above properly calculated and displayed
--												 when Ignore Talismans was enabled or disabled
--											 - Fix for potential issue with displaying proper color coded value for Recent stat differences
--											 - Fix for Healing Bonus value not calculating and displaying proper stat and difference values
--											 - Fix for Magic Critical Hit Bonus value not calculating and displaying proper stat and difference values
--											 - 5 New Bonus stats and associated tooltips with new descriptions added for display:
--												 - Fortitude (new in game patch 1.4.0)
--												 - Melee Power
--												 - Ranged Power
--												 - Magic Power
--												 - Healing Power
--									 2.2 - New Features and Bug Fixes
--											 - Updated version to 1.4.3
--											 - Added Options menu item to hide CaVES window when using the Dye Merchant Window
--											 - Fixed effective stat value calculation based on battle level with renown, this fixed some stat calculations/display
--											 - Fixed Weapon Speed mouseover tooltip value due to autoattack haste contribution for both Left and Right Hands
--											 - Optimizations and consolidation of output lines
--											 - Fixes and changes to initialization of Options window and values
--											 - Updates to documentation and manual for new features
--
--	 
--  Supported Versions: Warhammer Online v1.4.3
--
--	Dependencies: None
--
--	Addon Compatability: Designed to be compatible with:
--											 - AnywhereTrainer by DarthVon (thedpui02@sneakemail.com)
--											 - RvRStatsTab by ErmiteChevelu (ermite_chevelu@hotmail.com)
--											 - RvRStatsUpdated maintained by Corwynn_Maelstrom, original author Felyza (felyza@gmail.com)
--
--	Future Features: Continued updates for multiple language support.
--
--	Known Issues:  Minor issue.  The CaVES window is a popup window.  This was necessary in order to allow compatibility 
--							 	 with other mods, allow proper spacing of other activated windows such as the the character's
--							 	 item bags, and allows the Character View window to never block the use of CaVES or take focus away.
--							 	 As a result, CaVES will draw over top of other windows such as the map, etc...  The dye window for
--							 	 the Character View window does this also, probably for many of the same reasons.
--
--	Additional Credits: EA/Mythic - Several functions used/duplicated/modified in CaVES are from their core/default scripts,													
--										  related functions are with regards to stat calculation and display. 
--											DarthVon (thedpui02@sneakemail.com) - Function used/duplicated from AnywhereTrainer, solved
--											UI window positioning issues and allowed AnywhereTrainer compatibility with CaVES.
--											Thurwell and the DuffTimer team - For simplicity and non-dependency on other mods or libraries,
--											CaVES uses the localization format and functions used by DuffTimer. The variable names have been
--											changed to be specific to the CaVES mod and for personal preference with regards to naming conventions.
--											AmonCarroburg for CaVES German localization
--											Ziomav for CaVES Italian localization
--
--	Special Thanks:	EA/Mythic for a great game and for releasing the API specs
--								  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
--									www.curse.com and www.curseforge.com for hosting WAR mod files and projects
--									Morituri Guild for testing and feedback
--									Ominous Latin Name guild on Iron Rock for testing and feedback
--									Trouble guild on Iron Rock for support
--
--	License/Disclaimers: This addon and it's relevant parts and files are released under the 
--											 GNU General Public License version 3 (GPLv3).
--
--

------------------------------------------------------------------
----  Global Variables
------------------------------------------------------------------
if not CaVES then
	CaVES = {}
end
CaVESWindow = {}
CaVESWindow_CharacterWindow_Nub = {}
CaVESWindowOptions = {}
CaVES.anchors = {}

-- Initialize variable used for handling what CaVES-supported mods are installed.
CaVES.modsList = {}

-- Addon Info
CaVES.Title = L"CaVES"
CaVES.Version = 2.2

------------------------------------------------------------------
----  Local Variables
------------------------------------------------------------------

-- Localization Text
local LOC_TEXT
if not LOC_TEXT then
	LOC_TEXT = CaVES.wstrings[SystemData.Settings.Language.active] 
end

-- Name shortcut variables
local windowName = "CaVESWindow"
local nubName = "CaVESWindow_CharacterWindow_Nub"

-- Set variable used for hooking the Character View window update mode.
local oldCharacterWindowUpdate = nil

-- Hook variables used for hooking Character View shown/hidden to CaVES shown/hidden and
-- for handling interface shutdown.
local hookShown
local hookHidden
local hookShutdownPlayInterface

-- Initialize the boolean that indicates if CaVES is being initialized.  This can be as
-- a result of logging in or reloading the interface.
local bCaVESInitialization = true

-- Initialize the variable used for tracking whether CaVES.Settings.RefStatsValueSetting 3
-- should update the Reference Values
local bRefStatsValueSetting3Reset = true

-- Initialize a variable that is used to track when Reference Values should be saved
-- with regards to using Persistent Reference Stats (SavedVariables.lua).
local bSaveRefValues = false

-- Initialize variable that is used to track when Talisman bonuses should be ignored
-- in stat value calculations.
local bIgnoreTalismans = false

-- Initialize variable that is used to track when Event Slot Item bonuses should be ignored
-- in stat value calculations.
local bIgnoreEventSlotItem = false

-- Initialize a boolean that tracks whether or not and event handler has been set up
-- for an item set update. This boolean is also used in the tracking of whether or not
-- the initial start or reference values are correct since the item set values must be
-- included in the initial stat calculation.
local bRegisteredForItemSetData = false

-- Initialize variables used in saving character persistent stats.
local serverName = WStringToString( SystemData.Server.Name )
local characterName = WStringToString( GameData.Player.name )

-- Value Reset Boolean Variable
local bResetReferenceValues = false

-- Initialize start stats value variables.

-- Stats
local startStatsStrengthVal  = nil
local startStatsBallisticSkillVal  = nil
local startStatsIntelligenceVal  = nil
local startStatsToughnessVal  = nil
local startStatsFortitudeVal = nil
local startStatsWeaponSkillVal  = nil
local startStatsInitiativeVal  = nil
local startStatsWillpowerVal  = nil
local startStatsWoundsVal  = nil

-- Defense      
local startDefenseArmorVal  = nil
local startDefenseSpiritualResistanceVal  = nil
local startDefenseCorporealResistanceVal  = nil
local startDefenseElementalResistanceVal  = nil
local startDefenseBlockVal  = nil
local startDefenseParryVal  = nil
local startDefenseDodgeVal  = nil
local startDefenseDisruptVal  = nil

-- Melee             
local startMeleeWeaponDPSRHVal  = nil
local startMeleeSpeedRHVal  = nil
local startMeleeWeaponDPSLHVal = nil
local startMeleeSpeedLHVal  = nil
local startMeleePowerBonusVal  = nil
local startMeleeDamageBonusVal  = nil
local startMeleeArmorPenetrationVal  = nil
local startMeleeCriticalHitBonusVal  = nil

-- Ranged                   
local startRangedWeaponDPSVal  = nil
local startRangedSpeedVal  = nil
local startRangedPowerVal  = nil
local startRangedDamageBonusVal  = nil
local startRangedCriticalHitBonusVal  = nil

-- Magic
local startMagicPowerBonusVal  = nil                      
local startMagicDamageBonusVal  = nil
local startMagicCriticalHitBonusAttackVal  = nil
local startMagicHealingPowerBonusVal = nil
local startMagicHealingBonusVal = nil
local startMagicCriticalHitBonusHealVal  = nil


-- Initialize previous stats value variables
local prevRefStatsValueSetting

-- Stats
local prevStatsStrengthVal = nil
local prevStatsBallisticSkillVal = nil
local prevStatsIntelligenceVal = nil
local prevStatsToughnessVal = nil
local prevStatsFortitudeVal = nil
local prevStatsWeaponSkillVal = nil
local prevStatsInitiativeVal = nil
local prevStatsWillpowerVal = nil
local prevStatsWoundsVal = nil

-- Defense
local prevDefenseArmorVal = nil
local prevDefenseSpiritualResistanceVal = nil
local prevDefenseCorporealResistanceVal = nil
local prevDefenseElementalResistanceVal = nil
local prevDefenseBlockVal = nil
local prevDefenseParryVal = nil
local prevDefenseDodgeVal = nil
local prevDefenseDisruptVal = nil

-- Melee
local prevMeleeWeaponDPSRHVal = nil
local prevMeleeSpeedRHVal = nil
local prevMeleeWeaponDPSLHVal = nil
local prevMeleeSpeedLHVal = nil
local prevMeleePowerBonusVal = nil
local prevMeleeDamageBonusVal = nil
local prevMeleeArmorPenetrationVal = nil
local prevMeleeCriticalHitBonusVal = nil

-- Ranged
local prevRangedWeaponDPSVal = nil
local prevRangedSpeedVal = nil
local prevRangedPowerBonusVal = nil
local prevRangedDamageBonusVal = nil
local prevRangedCriticalHitBonusVal = nil

-- Magic
local prevMagicPowerBonusVal = nil
local prevMagicDamageBonusVal = nil
local prevMagicCriticalHitBonusAttackVal = nil
local prevMagicHealingPowerBonusVal = nil
local prevMagicHealingBonusVal = nil
local prevMagicCriticalHitBonusHealVal = nil

-- Initialize current stats value variables

-- Stats
local currentStatsStrengthVal = nil
local currentStatsBallisticSkillVal = nil
local currentStatsIntelligenceVal = nil
local currentStatsToughnessVal = nil
local currentStatsFortitudeVal = nil
local currentStatsWeaponSkillVal = nil
local currentStatsInitiativeVal = nil
local currentStatsWillpowerVal = nil
local currentStatsWoundsVal = nil

-- Defense
local currentDefenseArmorVal = nil
local currentDefenseSpiritualResistanceVal = nil
local currentDefenseCorporealResistanceVal = nil
local currentDefenseElementalResistanceVal = nil
local currentDefenseBlockVal = nil
local currentDefenseParryVal = nil
local currentDefenseDodgeVal = nil
local currentDefenseDisruptVal = nil

-- Melee
local currentMeleeWeaponDPSRHVal = nil
local currentMeleeSpeedRHVal = nil
local currentMeleeWeaponDPSLHVal = nil
local currentMeleeSpeedLHVal = nil
local currentMeleePowerBonusVal = nil
local currentMeleeDamageBonusVal = nil
local currentMeleeArmorPenetrationVal = nil
local currentMeleeCriticalHitBonusVal = nil

-- Ranged
local currentRangedWeaponDPSVal = nil
local currentRangedSpeedVal = nil
local currentRangedPowerBonusVal = nil
local currentRangedDamageBonusVal = nil
local currentRangedCriticalHitBonusVal = nil

-- Magic
local currentMagicPowerBonusVal = nil
local currentMagicDamageBonusVal = nil
local currentMagicCriticalHitBonusAttackVal = nil
local currentMagicHealingPowerBonusVal = nil
local currentMagicHealingBonusVal = nil
local currentMagicCriticalHitBonusHealVal = nil

-- Update Event Boolean Variables
local bPlayerEquipmentSlotUpdated = false
local bPlayerStatsUpdated = false
local bPlayerTrophySlotUpdated = false
local bPlayerEffectsUpdated = false

------------------------------------------------------------------
----  Misc Global/Local Variable Initializations and Functions
------------------------------------------------------------------

-- Custom Tooltip Definitions
CaVES.Tooltips = {}
local widthStatsWindow = 520 		-- width of stats window template CaVESWindow_StatsWindow
local widthStatsIconWindow = 22 -- width of stats icon window template CaVESWindow_StatsIcon
CaVES.Tooltips.ANCHOR_ICON_WINDOW_STATS_WINDOW_OFFSET_RIGHT = { Point = "topright", RelativeTo = "", RelativePoint = "topleft", XOffset = (widthStatsWindow - widthStatsIconWindow), YOffset = 0 }

-- Custom Color Definitions
CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN = { r=0, g=255, b=0 }
CaVESWindow.COLOR_CODE_STAT_CHANGE_RED 	 = { r=206, g=44, b=44 }

-- Mod support booleans

-- Support for the mod AnywhereTrainer by DarthVon (thedpui02@sneakemail.com).
local bAnywhereTrainer = nil -- boolean that denotes that AnywhereTrainer is installed

-- Support for the mod RvRStatsTab by Ermite Chevelu.
local bRvRStatsTab = nil
-- Support for the mod RvRStats by Corwynn Maelstrom (Variant of RvRStats by Felyza).
-- NOTE: Original RvRStats by Felyza is already compatible with CaVES.
local bRvRStats = nil 

-- Set various dimension variables for windows
-- NOTE: Use hardcoded values taken from .xml files for the various windows since window
-- dimensions could already have their values changed before CaVES script is executed.
local width_CharacterWindow_NORMAL = 575
local width_CharacterWindow_DYE = 745
local width_CaVESWindow = 540
local width_AnywhereTrainer = 43  -- support value for AnywhereTrainer by DarthVon (thedpui02@sneakemail.com)

-- Variables that establish the necessary width of Character View window when taking into account CaVES and any supported mods.
local CharacterWindow_TotalWidth_CharacterMode = nil
local CharacterWindow_TotalWidth_DyeMode = nil

------------------------------------------------------------------
----  Core Functions
------------------------------------------------------------------

-- Timer Functions and Variables (located here for easier tweaking)
local bTimerEnable = false
local timer = 0

local timeDelay = 0.125 		-- run every 0.125 seconds upon activation
local timeLeft = timeDelay 	-- set timeLeft to timeDelay value

-- Timer used as a pause to allow multiple item/stat update events to be queued up.
function CaVESWindow.UpdateTimer(timeElapsed)
    timeLeft = timeLeft - timeElapsed
    if timeLeft > 0 then
        return
    end
    
    if bTimerEnable == true then
    	timer = timer + timeDelay
    	--DEBUG(towstring( "TIME ELAPSED: "..timer))
    	
    	if timer >= 0.25 then	-- this is the "pause" or wait time duration
    			bTimerEnable = false -- disable the timer
    			CaVESWindow.UpdateStatsLabelsQueue()
    	end
    else
    	timer = 0
    end
    
    timeLeft = timeDelay -- reset modified timeLeft to the timeDelay value
end

-- Function that monitors when the game has loaded and player is in the world
-- and then initializes CaVES and the CaVES Window and stats.  This allows the
-- stats to be calculated after the player has entered the world, preventing
-- zero or incorrect stat values from being calculated.
function CaVES.Start()
	RegisterEventHandler( SystemData.Events.ENTER_WORLD, "CaVES.Initialize" )
	RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "CaVES.Initialize" )
end

function CaVES.Initialize()
		TextLogAddEntry( "Chat", 0, CaVES.Title..L" Version "..(wstring.format( L"%.01f",CaVES.Version))..L" Initialized" )
		DEBUG ( towstring( "CaVES loaded" ))
		
		-- Get supported mod data
		CaVES.GetSupportedModData()
		
		-- Initialize saved data (SavedVariables.lua).
		
		-- Options settings
		if not CaVES.Settings then
				CaVES.Settings = {
	    			OpenWithCV = false,
	    			HideWithDyeMerchant = true,
	    			RefStatsValueSetting = 1,
	        	ShowStatToolTips = true,
	        	ShowStatToolTipsOnStatIconOnly = false,
	        	IgnoreTalismans = false,
	        	IgnoreEventSlotItem = false,
	        	Version = CaVES.Version,
	  		}
		end
		
		-- Handle previous versions of CaVES that did not support saving 
		-- multi-character persistent Reference Stats saving.
		if (CaVES.Settings.Version == nil) or (CaVES.Settings.Version < 1.2) then
			-- Append the version into the saved settings for future use.
				CaVES.Settings.Version = CaVES.Version
			-- Delete the previously saved single character Reference Values.
				CaVESWindow.RefValues = {}
		end

		if (CaVES.Settings.Version < CaVES.Version) then
				CaVES.Settings.Version = CaVES.Version
		end

		-- Handle previous versions of CaVES that did not support Talisman filtering.
		if (CaVES.Settings.IgnoreTalismans == nil) or (CaVES.Settings.Version < 1.4) then
				CaVES.Settings.IgnoreTalismans = false
		end
		
		-- Handle previous versions of CaVES that did not support Event item filtering.
		if (CaVES.Settings.IgnoreEventSlotItem == nil) or (CaVES.Settings.Version < 1.8) then
				CaVES.Settings.IgnoreEventSlotItem = false
		end 

		-- Handle previous versions of CaVES that did not support hiding the CaVES window when using the Dye Merchant
		if (CaVES.Settings.HideWithDyeMerchant == nil) or (CaVES.Settings.Version < 2.2) then
				CaVES.Settings.HideWithDyeMerchant = true
		end 

		-- Boolean Variable to Ignore Talismans in Stat Calculations
		bIgnoreTalismans = CaVES.Settings.IgnoreTalismans

		-- Boolean Variable to Ignore Event Slot item in Stat Calculations
		bIgnoreEventSlotItem = CaVES.Settings.IgnoreEventSlotItem
		
		-- Initialize initial value setup for Reference Stats.
		prevRefStatsValueSetting = CaVES.Settings.RefStatsValueSetting

		-- Initialize the Reference Values that are saved per unique
		-- character name per server in the SavedVariables.lua when using 
		-- Persistence Reference Stats.
		if (CaVESWindow.RefValues == nil) then
			CaVESWindow.RefValues = {}
		end
		
		if (CaVESWindow.RefValues[serverName] == nil) then 
			CaVESWindow.RefValues[serverName] = {} 
		end
		
		if (CaVESWindow.RefValues[serverName][characterName]) == nil then
			CaVESWindow.RefValues[serverName][characterName] = {}
		end

		-- Unregister the initial start up event handlers.
		UnregisterEventHandler( SystemData.Events.ENTER_WORLD, "CaVES.Initialize" )
		UnregisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "CaVES.Initialize" )
		
		-- CaVES has now initialized so initialize the CaVES Window and as a result 
		-- all associated stat values.
		CaVESWindow.Initialize()
		
		-- Initialize the CaVES Option Window
		CaVESWindowOptions.Initialize()
			
end

function CaVESWindow.Initialize()
		-- Hooks into the Character View window
		hookShown = CharacterWindow.OnShown
		hookHidden = CharacterWindow.OnHidden
		
		CharacterWindow.OnShown = CaVES.OnShown
		CharacterWindow.OnHidden = CaVES.OnHidden

   	oldCharacterWindowUpdate = CharacterWindow.UpdateMode
   	CharacterWindow.UpdateMode = CaVESWindow.CharacterWindow_UpdateMode
		
		-- Hooks into Play Interface Shutdown
		hookShutdownPlayInterface = InterfaceCore.ShutdownPlayInterface
		InterfaceCore.ShutdownPlayInterface = CaVES.Shutdown

		-- Register events, these are all the events that can cause a stat value to change or update.
		WindowRegisterEventHandler( "CaVESWindow", SystemData.Events.PLAYER_EQUIPMENT_SLOT_UPDATED, "CaVESWindow.StatChangeEventEquipment" )
		WindowRegisterEventHandler( "CaVESWindow", SystemData.Events.PLAYER_STATS_UPDATED, "CaVESWindow.StatChangeEventStats" )
		WindowRegisterEventHandler( "CaVESWindow", SystemData.Events.PLAYER_EFFECTS_UPDATED, "CaVESWindow.StatChangeEventEffects" ) 
		WindowRegisterEventHandler( "CaVESWindow", SystemData.Events.PLAYER_TROPHY_SLOT_UPDATED, "CaVESWindow.StatChangeEventTrophy" )

	  -- CaVES Window Labels
	  ButtonSetText( windowName.."ToggleOptions", GetString( StringTables.Default.LABEL_CHAT_OPTIONS )) 
		ButtonSetText( windowName.."ResetRefValues", GetString( StringTables.Default.LABEL_RESET ))
		ButtonSetText( windowName.."ResetRecentValues", GetString( StringTables.Default.LABEL_RESET ))
		LabelSetText( windowName.."IgnoreTitle", LOC_TEXT.TITLE_IGNORE )  
	  LabelSetText( windowName.."ToggleTalismansLabel", LOC_TEXT.OPTIONS_IGNORETALISMANS )
	 	LabelSetText( windowName.."ToggleEventSlotLabel", LOC_TEXT.OPTIONS_IGNOREEVENT )
	
		-- Set the Options Window to be hidden.
	  WindowSetShowing( windowName.."OptionsWindow", false )
		
		-- Define and set header labels.
		LabelSetText( windowName.."TitleBarText", LOC_TEXT.TITLE_CAVESWINDOW )
		LabelSetText( windowName.."ListHeaderStat", LOC_TEXT.HEADER_STAT )
		LabelSetText( windowName.."ListHeaderRef", LOC_TEXT.HEADER_REFERENCE )
		LabelSetText( windowName.."ListHeaderRecent", LOC_TEXT.HEADER_RECENT )
	
	  -- Define Stat Icons and Labels (Use labels and icons variables defined by Character View window (characterwindow.lua)).
		CaVESWindow.DisplayStatIcon( windowName.."StatsStrengthIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.STRENGTH].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."StatsBallisticSkillIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.BALLISTICSKILL].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."StatsIntelligenceIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.INTELLIGENCE].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."StatsToughnessIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.TOUGHNESS].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."StatsFortitudeIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.TOUGHNESS].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."StatsWeaponSkillIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.WEAPONSKILL].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."StatsInitiativeIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.INITIATIVE].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."StatsWillpowerIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.WILLPOWER].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."StatsWoundsIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.WOUNDS].iconNum )
	
		LabelSetText( windowName.."StatsStrengthWindowStatLabel", GetString( StringTables.Default.LABEL_STRENGTH ))
	  LabelSetText( windowName.."StatsBallisticSkillWindowStatLabel", GetString( StringTables.Default.LABEL_BALLISTICSKILL ))
	  LabelSetText( windowName.."StatsIntelligenceWindowStatLabel", GetString( StringTables.Default.LABEL_INTELLIGENCE ))
	  LabelSetText( windowName.."StatsToughnessWindowStatLabel", GetString( StringTables.Default.LABEL_TOUGHNESS ))
	  LabelSetText( windowName.."StatsFortitudeWindowStatLabel", GetString( StringTables.Default.LABEL_FORTITUDE ))
		LabelSetText( windowName.."StatsWeaponSkillWindowStatLabel", GetString( StringTables.Default.LABEL_WEAPONSKILL ))
	  LabelSetText( windowName.."StatsInitiativeWindowStatLabel", GetString( StringTables.Default.LABEL_INITIATIVE ))
	  LabelSetText( windowName.."StatsWillpowerWindowStatLabel", GetString( StringTables.Default.LABEL_WILLPOWER ))
	  LabelSetText( windowName.."StatsWoundsWindowStatLabel", GetString( StringTables.Default.LABEL_WOUNDS ))
	  
		CaVESWindow.DisplayStatIcon( windowName.."DefenseArmorIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_ARMOR].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."DefenseSpiritualResistanceIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.SPIRITRESIST].iconNum )     
	  CaVESWindow.DisplayStatIcon( windowName.."DefenseCorporealResistanceIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.CORPOREALRESIST].iconNum )  
	  CaVESWindow.DisplayStatIcon( windowName.."DefenseElementalResistanceIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.ELEMENTALRESIST].iconNum )  
	  CaVESWindow.DisplayStatIcon( windowName.."DefenseBlockIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.BLOCKSKILL].iconNum )       
	  CaVESWindow.DisplayStatIcon( windowName.."DefenseParryIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.PARRYSKILL].iconNum )       
	  CaVESWindow.DisplayStatIcon( windowName.."DefenseDodgeIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.EVADESKILL].iconNum )       
	  CaVESWindow.DisplayStatIcon( windowName.."DefenseDisruptIconWindowIcon", CharacterWindow.StatIconInfo[GameData.Stats.DISRUPTSKILL].iconNum )     
	
	  LabelSetText( windowName.."DefenseArmorWindowStatLabel", GetString( StringTables.Default.LABEL_ARMOR ))
	  LabelSetText( windowName.."DefenseSpiritualResistanceWindowStatLabel", GetString( StringTables.Default.LABEL_SPIRITRESIST ))
	  LabelSetText( windowName.."DefenseCorporealResistanceWindowStatLabel", GetString( StringTables.Default.LABEL_CORPOREALRESIST ))
	  LabelSetText( windowName.."DefenseElementalResistanceWindowStatLabel", GetString( StringTables.Default.LABEL_ELEMENTALRESIST ))
		LabelSetText( windowName.."DefenseBlockWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_BLOCK ))
	  LabelSetText( windowName.."DefenseParryWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_PARRY ))
	  LabelSetText( windowName.."DefenseDodgeWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_EVADE ))
	  LabelSetText( windowName.."DefenseDisruptWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_DISRUPT ))
	
		CaVESWindow.DisplayStatIcon( windowName.."MeleeWeaponDPSRHIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_WEAPON_DPS].iconNum )       
	  CaVESWindow.DisplayStatIcon( windowName.."MeleeSpeedRHIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_MELEE_SPEED].iconNum )
		CaVESWindow.DisplayStatIcon( windowName.."MeleeWeaponDPSLHIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_WEAPON_DPS].iconNum )       
	  CaVESWindow.DisplayStatIcon( windowName.."MeleeSpeedLHIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_MELEE_SPEED].iconNum )           
	  CaVESWindow.DisplayStatIcon( windowName.."MeleePowerBonusIconWindowIcon",	CharacterWindow.StatIconInfo[CharacterWindow.LABEL_MELEE_BONUS].iconNum )           
	  CaVESWindow.DisplayStatIcon( windowName.."MeleeDamageBonusIconWindowIcon",	CharacterWindow.StatIconInfo[CharacterWindow.LABEL_MELEE_BONUS].iconNum )           
	  CaVESWindow.DisplayStatIcon( windowName.."MeleeArmorPenetrationIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_ARMOR_PENETRATION].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."MeleeCriticalHitBonusIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_MELEE_CRIT_BONUS].iconNum )
	  
	  LabelSetText( windowName.."MeleeWeaponDPSRHWindowStatLabel", GetString( StringTables.Default.LABEL_WEAPON_DPS )..L" ( "..GetString(StringTables.Default.LABEL_RIGHT_HAND)..L" )" ) 
	  LabelSetText( windowName.."MeleeSpeedRHWindowStatLabel", GetString( StringTables.Default.LABEL_SPEED )..L" ( "..GetString(StringTables.Default.LABEL_RIGHT_HAND)..L" )" ) 
	  LabelSetText( windowName.."MeleeWeaponDPSLHWindowStatLabel", GetString( StringTables.Default.LABEL_WEAPON_DPS )..L" ( "..GetString(StringTables.Default.LABEL_LEFT_HAND)..L" )" ) 
	  LabelSetText( windowName.."MeleeSpeedLHWindowStatLabel", GetString( StringTables.Default.LABEL_SPEED )..L" ( "..GetString(StringTables.Default.LABEL_LEFT_HAND)..L" )" ) 
	  LabelSetText( windowName.."MeleePowerBonusWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_DAMAGE_MELEE ))
	  LabelSetText( windowName.."MeleeDamageBonusWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS ))
	  LabelSetText( windowName.."MeleeArmorPenetrationWindowStatLabel", GetString( StringTables.Default.LABEL_MELEE_ARMOR_PENETRATION ))
		LabelSetText( windowName.."MeleeCriticalHitBonusWindowStatLabel", GetString( StringTables.Default.LABEL_CRIT_BONUS ))
	
		CaVESWindow.DisplayStatIcon( windowName.."RangedWeaponDPSIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_RANGED].iconNum )      
	  CaVESWindow.DisplayStatIcon( windowName.."RangedSpeedIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_RANGED_SPEED].iconNum )
	  CaVESWindow.DisplayStatIcon( windowName.."RangedPowerBonusIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_RANGED_BONUS].iconNum )       
	  CaVESWindow.DisplayStatIcon( windowName.."RangedDamageBonusIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_RANGED_BONUS].iconNum )     
	  CaVESWindow.DisplayStatIcon( windowName.."RangedCriticalHitBonusIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_RANGED_CRIT_BONUS].iconNum )
	
	  LabelSetText( windowName.."RangedWeaponDPSWindowStatLabel", GetString( StringTables.Default.LABEL_WEAPON_DPS ))
	  LabelSetText( windowName.."RangedSpeedWindowStatLabel", GetString( StringTables.Default.LABEL_SPEED ))
	  LabelSetText( windowName.."RangedPowerBonusWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_DAMAGE_RANGED ))
	  LabelSetText( windowName.."RangedDamageBonusWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS ))
	  LabelSetText( windowName.."RangedCriticalHitBonusWindowStatLabel", GetString( StringTables.Default.LABEL_CRIT_BONUS ))

		CaVESWindow.DisplayStatIcon( windowName.."MagicPowerBonusIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_SPELL_BONUS].iconNum )            	
		CaVESWindow.DisplayStatIcon( windowName.."MagicDamageBonusIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_SPELL_BONUS].iconNum )            
	  CaVESWindow.DisplayStatIcon( windowName.."MagicCriticalHitBonusAttackIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_DAMAGE_CRIT_PERCENT].iconNum ) 
		CaVESWindow.DisplayStatIcon( windowName.."MagicHealingPowerBonusIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_SPELL_HEALING_BONUS].iconNum )   
		CaVESWindow.DisplayStatIcon( windowName.."MagicHealingBonusIconWindowIcon", CharacterWindow.StatIconInfo[CharacterWindow.LABEL_SPELL_HEALING_BONUS].iconNum )   
	  CaVESWindow.DisplayStatIcon( windowName.."MagicCriticalHitBonusHealIconWindowIcon", CharacterWindow.StatIconInfo[StringTables.Default.LABEL_HEAL_CRIT_PERCENT].iconNum )   

	  LabelSetText( windowName.."MagicPowerBonusWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_DAMAGE_MAGIC ))	
	  LabelSetText( windowName.."MagicDamageBonusWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS ))
	  LabelSetText( windowName.."MagicCriticalHitBonusAttackWindowStatLabel", GetString( StringTables.Default.LABEL_CRIT_BONUS )..L" ( "..GetString(StringTables.Default.LABEL_PET_ATTACK)..L" )" ) 
		LabelSetText( windowName.."MagicHealingPowerBonusWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_HEALING_POWER ))
		LabelSetText( windowName.."MagicHealingBonusWindowStatLabel", GetString( StringTables.Default.LABEL_BONUS_HEALING ))
	  LabelSetText( windowName.."MagicCriticalHitBonusHealWindowStatLabel", GetString( StringTables.Default.LABEL_CRIT_BONUS )..L" ( "..GetString(StringTables.Default.LABEL_HEAL)..L" )" )
	  
	  -- Set the Ignore Talisman button state.
	  ButtonSetPressedFlag( windowName.."ToggleTalismansButton", CaVES.Settings.IgnoreTalismans )
	 	-- Set the Ignore Event Item button state.
	 	ButtonSetPressedFlag( windowName.."ToggleEventSlotButton", CaVES.Settings.IgnoreEventSlotItem )

	  -- Update the Stats Labels (icons, labels, and all values).
	  CaVESWindow.UpdateStatsLabels()
	  
end

function CaVESWindow.DisplayStatIcon( wndName, iconNum )
		local texture, x, y = GetIconData( iconNum )
	    
	  DynamicImageSetTexture( wndName, texture, x, y )
	  WindowSetShowing( wndName, true )
end

function CaVES.OnShown( ... )
	  hookShown( ... )

	  local showing = WindowGetShowing( windowName )
	  
	  -- Safeguard against Character View window dimensions erroneously including the CaVES Window width, this
	  -- usually occurs as a result of the dye merchant window closing and associated actions by the Character View window
	  -- that do not allow CaVES to make adjustments when setting the proper Character View window width.
	  if ( showing == false ) then
	  	local w, h = WindowGetDimensions( "CharacterWindow" )
	  	if w > (CharacterWindow_TotalWidth_CharacterMode - width_CaVESWindow) then
				WindowSetDimensions( "CharacterWindow", w - width_CaVESWindow, h )
	  	end
	  end

	  WindowSetShowing( nubName, true )
	  WindowSetShowing( windowName, false )
	  WindowSetShowing( windowName.."OptionsWindow", false )
	
	  CaVESWindow.UpdateStatsLabels()

	  if CaVES.Settings.OpenWithCV == true then
	  	CaVESWindow.ToggleShowing()
	  end
	
end

-- only time not to show caves is when hide caves with dye is true and dye window is up

function CaVES.OnHidden( ... )
			-- Test for case for CharacterWindow being closed while
			-- CaVES was still showing and only adjust CharacterWindow
			-- dimensions and anchors if CaVES was showing at the time. 
		  local showing = WindowGetShowing( windowName )
		 	if ( showing == true ) then
			  	local w, h = WindowGetDimensions( "CharacterWindow" )
			  	WindowSetDimensions( "CharacterWindow", w - width_CaVESWindow, h )
			  	CaVESWindow.ReadjustWindowAnchors( "CharacterWindowBackground", "bottomright", width_CaVESWindow, 0 )
			  	CaVESWindow.ReadjustWindowAnchors( "DyeMerchantButtons", "bottom", (width_CaVESWindow/2), 0 )
		  end

		 	hookHidden( ... )
		
			-- Force update value reset variables just in case.
			bResetReferenceValues = false
		
			-- Force update event boolean variables just in case.
			bPlayerEquipmentSlotUpdated = false
			bPlayerStatsUpdated = false
			bPlayerTrophySlotUpdated = false
			bPlayerEffectsUpdated = false
		
			-- Force timer related variables just in case.
			bTimerEnable = false
			timer = 0
			
			-- Force the variable used for tracking whether CaVES.Settings.RefStatsValueSetting 3 
			-- should update the Reference Values just in case.
	  	bRefStatsValueSetting3Reset = true

			-- Set visible states of all the windows.
		  WindowSetShowing( nubName, false )
		  WindowSetShowing( windowName, false )
		  WindowSetShowing( windowName.."OptionsWindow", false )
end

function CaVESWindow.ToggleShowing()
    local showing = WindowGetShowing( windowName )
    if( showing == true ) then
        WindowSetShowing( windowName, false )
        WindowSetShowing( windowName.."OptionsWindow", false )
        WindowSetShowing( nubName, true )
      	local w, h = WindowGetDimensions( "CharacterWindow" )
        WindowSetDimensions( "CharacterWindow", w - width_CaVESWindow, h )
        CaVESWindow.ReadjustWindowAnchors( "CharacterWindowBackground", "bottomright", width_CaVESWindow, 0 )
        CaVESWindow.ReadjustWindowAnchors( "DyeMerchantButtons", "bottom", (width_CaVESWindow/2), 0 )
        
        -- Set the variable used for tracking whether CaVES.Settings.RefStatsValueSetting 3 
				-- should update the Reference Values.
	  		bRefStatsValueSetting3Reset = true
    else
        WindowSetShowing( windowName, true )
        WindowSetShowing( windowName.."OptionsWindow", false )
        WindowSetShowing( nubName, false )
        local w, h = WindowGetDimensions( "CharacterWindow" )
        if w >= CharacterWindow_TotalWidth_CharacterMode then
        	CaVESWindow.ReadjustWindowAnchors( "CharacterWindowBackground", "bottomright", -(width_CaVESWindow), 0 )
        	CaVESWindow.ReadjustWindowAnchors( "DyeMerchantButtons", "bottom", -(width_CaVESWindow/2), 0 )
        else
        	WindowSetDimensions( "CharacterWindow", w + width_CaVESWindow, h )
        	CaVESWindow.ReadjustWindowAnchors( "CharacterWindowBackground", "bottomright", -(width_CaVESWindow), 0 )
        	CaVESWindow.ReadjustWindowAnchors( "DyeMerchantButtons", "bottom", -(width_CaVESWindow/2), 0 )
				end
        -- Set the variable used for tracking whether CaVES.Settings.RefStatsValueSetting 3 should update
				-- the Reference Values.
	  		bRefStatsValueSetting3Reset = false
    end
end

function CaVES.Shutdown( ... )
		-- Unregister all of the events that cause a stat change or update.
		WindowUnregisterEventHandler( "CaVESWindow", SystemData.Events.PLAYER_EQUIPMENT_SLOT_UPDATED, "CaVESWindow.StatChangeEventEquipment" )
		WindowUnregisterEventHandler( "CaVESWindow", SystemData.Events.PLAYER_STATS_UPDATED, "CaVESWindow.StatChangeEventStats" )
		WindowUnregisterEventHandler( "CaVESWindow", SystemData.Events.PLAYER_EFFECTS_UPDATED, "CaVESWindow.StatChangeEventEffects" ) 
		WindowUnregisterEventHandler( "CaVESWindow", SystemData.Events.PLAYER_TROPHY_SLOT_UPDATED, "CaVESWindow.StatChangeEventTrophy" )

		return hookShutdownPlayInterface( ... )
end

function CaVESWindow.CharacterWindow_UpdateMode( mode )
		oldCharacterWindowUpdate ( mode )
		
		local showing = WindowGetShowing( windowName )
		
		-- Get updated data on the enabled/disabled mods in case the user has made changes 
		-- since the initialization of the mod data.
		CaVES.GetSupportedModData()
		
		local w, h = WindowGetDimensions( "CharacterWindow" )
		
		if (mode == CharacterWindow.MODE_NORMAL) then
				if ( showing == true ) then
					WindowSetDimensions( "CharacterWindow", w + width_CaVESWindow, h )
				end
		elseif (mode == CharacterWindow.MODE_DYE_MERCHANT) then
				if ( showing == true ) then
					-- First fix dimensions for the Character Window with Dye Merchant
					WindowSetDimensions( "CharacterWindow", w + width_CaVESWindow, h )
					-- Just toggle the CaVES Window (off in his case) as normal if the user has selected hiding CaVES when 
					-- using the Dye Merchant
					if  ((CaVES.Settings.OpenWithCV == true) and (CaVES.Settings.HideWithDyeMerchant == true))  then
							CaVESWindow.ToggleShowing()
					end
				end
		-- Support/Fix for RvRStats-based mods (there are currently 2, RvRStatsUpdated and RvR Stats Tab)
		elseif (mode == CharacterWindow.MODE_RVR) then
				if ( showing == true ) then -- keep individual hooks for each in for now
						if bRvRStatsTab == true then
								WindowSetDimensions( "CharacterWindow", w + width_CaVESWindow, h )
						elseif bRvRStats == true then
								WindowSetDimensions( "CharacterWindow", w + width_CaVESWindow, h )
						end
				end
		end
end

function CaVES.GetSupportedModData()
		local supportedModData = {}
		supportedModData = ModulesGetData()

		local bAnywhereTrainerExists = false
    
    for modIndex, modData in ipairs( supportedModData ) do
        if (modData.name == "AnywhereTrainer") then
        		bAnywhereTrainerExists = true
        		if (modData.isEnabled == true) then
        				bAnywhereTrainer = true
        				CharacterWindow_TotalWidth_CharacterMode = width_CharacterWindow_NORMAL + width_CaVESWindow + width_AnywhereTrainer
								CharacterWindow_TotalWidth_DyeMode = width_CharacterWindow_DYE + width_CaVESWindow + width_AnywhereTrainer
        		else
        				bAnywhereTrainer = false
        				CharacterWindow_TotalWidth_CharacterMode = width_CharacterWindow_NORMAL + width_CaVESWindow
								CharacterWindow_TotalWidth_DyeMode = width_CharacterWindow_DYE + width_CaVESWindow
        		end
        elseif (modData.name == "RvRStats") then
        		if (modData.isEnabled == true) then
        				bRvRStats = true 
        		else
        				bRvRStats = false
        		end 
        elseif (modData.name == "RvRStatsTab") then
        		if (modData.isEnabled == true) then
        				bRvRStatsTab = true 
        		else
        				bRvRStatsTab = false
        		end
				end
    end
    
    -- Set the default window dimensions if AnywhereTrainer is not installed.
    if bAnywhereTrainerExists == false then
				CharacterWindow_TotalWidth_CharacterMode = width_CharacterWindow_NORMAL + width_CaVESWindow
				CharacterWindow_TotalWidth_DyeMode = width_CharacterWindow_DYE + width_CaVESWindow
		end
    
end

-- Function taken from function "AnywhereTrainer.ReadjustWindowAnchors" found 
-- in AnywhereTrainer by DarthVon (thedpui02@sneakemail.com).
-- This function helps in the support of AnywhereTrainer alongside CaVES and helps
-- proper UI placement of other UI windows with respect to CaVES or CaVES 
-- with AnywhereTrainer.  The function name has been changed so as to avoid any conflict
-- with any future changes or modifications that may be required between the two mods.
function CaVESWindow.ReadjustWindowAnchors( wndName, point, xOffset, yOffset )
		local anchors = {}
		local anchorCount = WindowGetAnchorCount( wndName )
		for i = 1, anchorCount do
				table.insert( anchors, { WindowGetAnchor( wndName, i ) } )
				if( anchors[i][1] == point ) then
						anchors[i][4] = anchors[i][4] + xOffset
						anchors[i][5] = anchors[i][5] + yOffset
				end
		end
		WindowClearAnchors( wndName )
		for i = 1, anchorCount do
				WindowAddAnchor( wndName, anchors[i][1], anchors[i][3], anchors[i][2], anchors[i][4], anchors[i][5] )
		end
		CaVES.anchors = anchors
end

------------------------------------------------------------------
---- Nub Button Functions
------------------------------------------------------------------
function CaVESWindow_CharacterWindow_Nub.ToggleWindow()
    CaVESWindow.ToggleShowing()
end

function CaVESWindow_CharacterWindow_Nub.OnMouseoverBtn()
    Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil ) 
    
    local row = 1
    local column = 1
    Tooltips.SetTooltipText( row, column, LOC_TEXT.MOUSEOVER_NUB )
    
    Tooltips.Finalize()
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT)
end

------------------------------------------------------------------
---- Options Window
------------------------------------------------------------------
function CaVESWindow.ToggleOptions()
    local showing = WindowGetShowing( windowName.."OptionsWindow" )
    WindowSetShowing( windowName.."OptionsWindow", showing == false )
end

function CaVESWindowOptions.Initialize()
	
		-- Options Window Labels
		LabelSetText( windowName.."OptionsWindowTitleBarText", L"CaVES "..GetString( StringTables.Default.LABEL_CHAT_OPTIONS ))   
		LabelSetText( windowName.."OptionsWindowGeneralTitle", LOC_TEXT.TITLE_OPTIONSWINDOW )
	  LabelSetText( windowName.."OptionsWindowGeneralOption1Label", LOC_TEXT.OPTIONS_OPENWITHCV )
	  LabelSetText( windowName.."OptionsWindowGeneralOption2Label", LOC_TEXT.OPTIONS_HIDEWITHDYEMERCHANT )
	  LabelSetText( windowName.."OptionsWindowReferenceStatsTitle", LOC_TEXT.OPTIONS_REFSTATS )
	  LabelSetText( windowName.."OptionsWindowRefStatsOptionsComboLabel", LOC_TEXT.OPTIONS_VALUES )
	  ComboBoxAddMenuItem( windowName.."OptionsWindowRefStatsOptionsCombo", LOC_TEXT.OPTIONS_VALUES01 )
	  ComboBoxAddMenuItem( windowName.."OptionsWindowRefStatsOptionsCombo", LOC_TEXT.OPTIONS_VALUES02 )
	  ComboBoxAddMenuItem( windowName.."OptionsWindowRefStatsOptionsCombo", LOC_TEXT.OPTIONS_VALUES03 )
	  LabelSetText( windowName.."OptionsWindowToolTipTitle", LOC_TEXT.OPTIONS_TOOLTIPS )
	  LabelSetText( windowName.."OptionsWindowToolTipOption1Label", LOC_TEXT.OPTIONS_TOOLTIPS01 )
	  LabelSetText( windowName.."OptionsWindowToolTipOption2Label", LOC_TEXT.OPTIONS_TOOLTIPS02 )
	  ButtonSetText( windowName.."OptionsWindowSaveButton", GetString( StringTables.Default.LABEL_SAVE ))
	  ButtonSetText( windowName.."OptionsWindowCancelButton", GetString( StringTables.Default.LABEL_CLOSE ))
	
		CaVESWindowOptions.UpdateOptions()
end

function CaVESWindowOptions.Cancel()
  	WindowSetShowing( windowName.."OptionsWindow", false)
end

function CaVESWindowOptions.Save()
		CaVES.Settings.OpenWithCV = ButtonGetPressedFlag( windowName.."OptionsWindowGeneralOption1Button" )
		CaVES.Settings.HideWithDyeMerchant = ButtonGetPressedFlag( windowName.."OptionsWindowGeneralOption2Button" )
		CaVES.Settings.RefStatsValueSetting = ComboBoxGetSelectedMenuItem( windowName.."OptionsWindowRefStatsOptionsCombo" )
		CaVES.Settings.ShowStatToolTips = ButtonGetPressedFlag( windowName.."OptionsWindowToolTipOption1Button" )
		CaVES.Settings.ShowStatToolTipsOnStatIconOnly = ButtonGetPressedFlag( windowName.."OptionsWindowToolTipOption2Button" )
		
		-- Handle case where CaVES is set to show when using Dye Window, Dye Window is being used, and the user opens
		-- the Options window and sets CaVES to not show when Dye Window is shown and hits Save for the options
		if CaVES.Settings.HideWithDyeMerchant == true then
			if ((WindowGetShowing( windowName )) and (CaVES.Settings.HideWithDyeMerchant == true) and (CharacterWindow.mode == CharacterWindow.MODE_DYE_MERCHANT)) then
				CaVESWindow.ToggleShowing()
			end
		end
		
		if prevRefStatsValueSetting ~= CaVES.Settings.RefStatsValueSetting then
				-- Set a new value for the previous Reference Value options setting.
				prevRefStatsValueSetting = CaVES.Settings.RefStatsValueSetting

				-- If the persistence reference stats option was selected, these values need to be 
				-- saved immediately so that these will get saved upon logout or reload of the UI.
				if CaVES.Settings.RefStatsValueSetting == 2 then
						CaVESWindow.SaveRefValues()
				end

				-- Update the Reference Stats values to reflect the way that Reference Stats are to now be handled.
				CaVESWindow.UpdateStatsLabels()
		end
end

function CaVESWindowOptions.OnShown()
  CaVESWindowOptions.UpdateOptions()
end

function CaVESWindowOptions.OnToolTipOption1Changed()
		EA_LabelCheckButton.Toggle()
		if (ButtonGetPressedFlag( windowName.."OptionsWindowToolTipOption1Button" ) == false ) then
				ButtonSetDisabledFlag( windowName.."OptionsWindowToolTipOption2Button", true )
				LabelSetTextColor( windowName.."OptionsWindowToolTipOption2Label", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b ) 
		elseif (ButtonGetPressedFlag( windowName.."OptionsWindowToolTipOption1Button" ) == true ) then
				ButtonSetDisabledFlag( windowName.."OptionsWindowToolTipOption2Button", false )
				LabelSetTextColor( windowName.."OptionsWindowToolTipOption2Label", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b ) 
		end
end

function CaVESWindowOptions.UpdateOptions()
    ButtonSetPressedFlag( windowName.."OptionsWindowGeneralOption1Button", CaVES.Settings.OpenWithCV )
    ButtonSetPressedFlag( windowName.."OptionsWindowGeneralOption2Button", CaVES.Settings.HideWithDyeMerchant )
    ComboBoxSetSelectedMenuItem( windowName.."OptionsWindowRefStatsOptionsCombo", CaVES.Settings.RefStatsValueSetting )
  	ButtonSetPressedFlag( windowName.."OptionsWindowToolTipOption1Button", CaVES.Settings.ShowStatToolTips )
  	ButtonSetPressedFlag( windowName.."OptionsWindowToolTipOption2Button", CaVES.Settings.ShowStatToolTipsOnStatIconOnly )
end

------------------------------------------------------------------
---- Mouseovers/Tooltips
------------------------------------------------------------------
function CaVESWindow.OnMouseoverStatHeaderLabelIcon()
    Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, nil ) 
    
    local row = 1
    local column = 1
    Tooltips.SetTooltipText( row, column, LOC_TEXT.MOUSEOVER_STATICON )
    
    Tooltips.Finalize()

    local anchor = { Point="topright", RelativeTo=SystemData.MouseOverWindow.name, RelativePoint="bottomleft", XOffset=-24, YOffset=-12 }
    Tooltips.AnchorTooltip ( anchor )
end

function CaVESWindow.OnMouseoverStatHeaderLabelStat()
    Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, nil ) 
    
    local row = 1
    local column = 1
    Tooltips.SetTooltipText( row, column, LOC_TEXT.MOUSEOVER_STATNAME )

    Tooltips.Finalize()

    local anchor = { Point="topleft", RelativeTo=SystemData.MouseOverWindow.name, RelativePoint="bottomleft", XOffset=-4, YOffset=-12}
    Tooltips.AnchorTooltip ( anchor )
end

function CaVESWindow.OnMouseoverStatHeaderLabelReference()
    Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, nil ) 
    
    local row = 1
    local column = 1
    if CaVES.Settings.RefStatsValueSetting == 1 then
    		Tooltips.SetTooltipText( row, column, LOC_TEXT.MOUSEOVER_REFSTAT01 )
    elseif CaVES.Settings.RefStatsValueSetting == 2 then
    		Tooltips.SetTooltipText( row, column, LOC_TEXT.MOUSEOVER_REFSTAT02 )
    elseif CaVES.Settings.RefStatsValueSetting == 3 then
    		Tooltips.SetTooltipText( row, column, LOC_TEXT.MOUSEOVER_REFSTAT03 )
    end
    
    Tooltips.Finalize()
    
    local anchor = { Point="topleft", RelativeTo=SystemData.MouseOverWindow.name, RelativePoint="bottomright", XOffset=240, YOffset=-12 }
    Tooltips.AnchorTooltip ( anchor )
end

function CaVESWindow.OnMouseoverStatHeaderLabelRecent()
    Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, nil ) 
    
    local row = 1
    local column = 1
    Tooltips.SetTooltipText( row, column, LOC_TEXT.MOUSEOVER_RECENTSTAT )
    
    Tooltips.Finalize()
    
    local anchor = { Point="topleft", RelativeTo=SystemData.MouseOverWindow.name, RelativePoint="bottomright", XOffset=200, YOffset=-12 }
    Tooltips.AnchorTooltip ( anchor )
end

-- Modified EA/Mythic function (CharacterWindow.CreateTooltip) from characterwindow.lua, allows a anchor point 
-- to be passed instead of always using default of Tooltips.ANCHOR_WINDOW_RIGHT.
function CaVESWindow.CreateTooltip( wndName, line1, line2, anchorpoint )
    if (line1 == nil) then
        return
    end
    Tooltips.CreateTextOnlyTooltip( wndName )
    Tooltips.SetTooltipText( 1, 1, line1 )
    Tooltips.SetTooltipColorDef( 1, 1, Tooltips.COLOR_HEADING )
    if (line2 ~= nil) then
        Tooltips.SetTooltipText( 2, 1, line2 )  
    end
    Tooltips.Finalize()
    
    if anchorpoint then
   		Tooltips.AnchorTooltip( anchorpoint )
   	else -- If no anchorpoint parameter is passed, act like the original EA/Mythic function.
   		Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )
   	end
end

function CaVESWindow.DisplayStatsTooltip( curWindowName, statsStatWindow, statsIconWindow, labelsTooltipFunction1, labelsTooltipFunction2, index )

		local tooltipDisplayWindowName

		if CaVES.Settings.ShowStatToolTipsOnStatIconOnly == true then
				tooltipDisplayWindowName = statsIconWindow
				CaVESWindow.CreateTooltip( tooltipDisplayWindowName, labelsTooltipFunction1(index), labelsTooltipFunction2(index), Tooltips.ANCHOR_WINDOW_RIGHT )
		elseif CaVES.Settings.ShowStatToolTipsOnStatIconOnly == false then
				if curWindowName == statsStatWindow then
						tooltipDisplayWindowName = statsStatWindow
						CaVESWindow.CreateTooltip( tooltipDisplayWindowName, labelsTooltipFunction1(index), labelsTooltipFunction2(index), Tooltips.ANCHOR_WINDOW_RIGHT )
				elseif curWindowName == statsIconWindow then
						tooltipDisplayWindowName = statsIconWindow
						CaVESWindow.CreateTooltip( tooltipDisplayWindowName, labelsTooltipFunction1(index), labelsTooltipFunction2(index), CaVES.Tooltips.ANCHOR_ICON_WINDOW_STATS_WINDOW_OFFSET_RIGHT )
				end
		end
end

function CaVESWindow.StatsMouseOver()

	if CaVES.Settings.ShowStatToolTips == true then
	
			local currentWindowName = SystemData.ActiveWindow.name

		-- CaVES uses a different display setup for windows and naming convention, but it is still possible to use
		-- the majority of the Character View functions to display tooltips.  This removes similar or redundant script, 
		-- decreasing script memory footprint and allowing any changes to the game script to be automatically
		-- inherited by the CaVES mod.

			-- Stats
			if ( currentWindowName == windowName.."StatsStrengthWindow" ) or ( currentWindowName == windowName.."StatsStrengthIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsStrengthWindow", windowName.."StatsStrengthIconWindow", CharacterWindow.GetStatsLabelsTooltip, CharacterWindow.GetStatsLabelsTooltipLine2, 1 )
			elseif ( currentWindowName == windowName.."StatsBallisticSkillWindow" ) or ( currentWindowName == windowName.."StatsBallisticSkillIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsBallisticSkillWindow", windowName.."StatsBallisticSkillIconWindow", CharacterWindow.GetStatsLabelsTooltip, CharacterWindow.GetStatsLabelsTooltipLine2, 2 )
			elseif ( currentWindowName == windowName.."StatsIntelligenceWindow" ) or ( currentWindowName == windowName.."StatsIntelligenceIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsIntelligenceWindow", windowName.."StatsIntelligenceIconWindow", CharacterWindow.GetStatsLabelsTooltip, CharacterWindow.GetStatsLabelsTooltipLine2, 3 )
			elseif ( currentWindowName == windowName.."StatsToughnessWindow" ) or ( currentWindowName == windowName.."StatsToughnessIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsToughnessWindow", windowName.."StatsToughnessIconWindow", CharacterWindow.GetStatsLabelsTooltip, CharacterWindow.GetStatsLabelsTooltipLine2, 4 )
			elseif ( currentWindowName == windowName.."StatsFortitudeWindow" ) or ( currentWindowName == windowName.."StatsFortitudeIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsFortitudeWindow", windowName.."StatsFortitudeIconWindow", CaVESWindow.GetPowerBonusLabelsTooltip, CaVESWindow.GetPowerBonusLabelsTooltip2, 0 )
			elseif ( currentWindowName == windowName.."StatsWeaponSkillWindow" ) or ( currentWindowName == windowName.."StatsWeaponSkillIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsWeaponSkillWindow", windowName.."StatsWeaponSkillIconWindow", CharacterWindow.GetStatsLabelsTooltip, CharacterWindow.GetStatsLabelsTooltipLine2, 5 )
			elseif ( currentWindowName == windowName.."StatsInitiativeWindow" ) or ( currentWindowName == windowName.."StatsInitiativeIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsInitiativeWindow", windowName.."StatsInitiativeIconWindow", CharacterWindow.GetStatsLabelsTooltip, CharacterWindow.GetStatsLabelsTooltipLine2, 6 )
			elseif ( currentWindowName == windowName.."StatsWillpowerWindow" ) or ( currentWindowName == windowName.."StatsWillpowerIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsWillpowerWindow", windowName.."StatsWillpowerIconWindow", CharacterWindow.GetStatsLabelsTooltip, CharacterWindow.GetStatsLabelsTooltipLine2, 7 )
			elseif ( currentWindowName == windowName.."StatsWoundsWindow" ) or ( currentWindowName == windowName.."StatsWoundsIconWindow" )then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."StatsWoundsWindow", windowName.."StatsWoundsIconWindow", CharacterWindow.GetStatsLabelsTooltip, CharacterWindow.GetStatsLabelsTooltipLine2, 8 )
	
			-- Defense
			elseif ( currentWindowName == windowName.."DefenseArmorWindow" ) or ( currentWindowName == windowName.."DefenseArmorIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."DefenseArmorWindow", windowName.."DefenseArmorIconWindow", CharacterWindow.GetDefenseLabelsTooltip, CharacterWindow.GetDefenseLabelsTooltipLine2, 1 )
			elseif ( currentWindowName == windowName.."DefenseSpiritualResistanceWindow" ) or ( currentWindowName == windowName.."DefenseSpiritualResistanceIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."DefenseSpiritualResistanceWindow", windowName.."DefenseSpiritualResistanceIconWindow", CharacterWindow.GetDefenseLabelsTooltip, CharacterWindow.GetDefenseLabelsTooltipLine2, 2 )
			elseif ( currentWindowName == windowName.."DefenseCorporealResistanceWindow" ) or ( currentWindowName == windowName.."DefenseCorporealResistanceIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."DefenseCorporealResistanceWindow", windowName.."DefenseCorporealResistanceIconWindow", CharacterWindow.GetDefenseLabelsTooltip, CharacterWindow.GetDefenseLabelsTooltipLine2, 3 )
			elseif ( currentWindowName == windowName.."DefenseElementalResistanceWindow" ) or ( currentWindowName == windowName.."DefenseElementalResistanceIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."DefenseElementalResistanceWindow", windowName.."DefenseElementalResistanceIconWindow", CharacterWindow.GetDefenseLabelsTooltip, CharacterWindow.GetDefenseLabelsTooltipLine2, 4 )
			elseif ( currentWindowName == windowName.."DefenseBlockWindow" ) or ( currentWindowName == windowName.."DefenseBlockIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."DefenseBlockWindow", windowName.."DefenseBlockIconWindow", CharacterWindow.GetDefenseLabelsTooltip, CharacterWindow.GetDefenseLabelsTooltipLine2, 5 )
			elseif ( currentWindowName == windowName.."DefenseParryWindow" ) or ( currentWindowName == windowName.."DefenseParryIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."DefenseParryWindow", windowName.."DefenseParryIconWindow", CharacterWindow.GetDefenseLabelsTooltip, CharacterWindow.GetDefenseLabelsTooltipLine2, 6 )
			elseif ( currentWindowName == windowName.."DefenseDodgeWindow" ) or ( currentWindowName == windowName.."DefenseDodgeIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."DefenseDodgeWindow", windowName.."DefenseDodgeIconWindow", CharacterWindow.GetDefenseLabelsTooltip, CharacterWindow.GetDefenseLabelsTooltipLine2, 7 )
			elseif ( currentWindowName == windowName.."DefenseDisruptWindow" ) or ( currentWindowName == windowName.."DefenseDisruptIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."DefenseDisruptWindow", windowName.."DefenseDisruptIconWindow", CharacterWindow.GetDefenseLabelsTooltip, CharacterWindow.GetDefenseLabelsTooltipLine2, 8 )

			-- Melee
			-- NOTE: Since CaVES expands the Melee stats for Right and Left handed values, custom functions for
			-- the display of the tooltip information were created.
			elseif ( currentWindowName == windowName.."MeleeWeaponDPSRHWindow" ) or ( currentWindowName == windowName.."MeleeWeaponDPSRHIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MeleeWeaponDPSRHWindow", windowName.."MeleeWeaponDPSRHIconWindow", CaVESWindow.GetMeleeWeaponDPSLabelsTooltip, CaVESWindow.GetMeleeWeaponDPSLabelsTooltipLine2, 0 )
			elseif ( currentWindowName == windowName.."MeleeSpeedRHWindow" ) or ( currentWindowName == windowName.."MeleeSpeedRHIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MeleeSpeedRHWindow", windowName.."MeleeSpeedRHIconWindow", CaVESWindow.GetMeleeWeaponSpeedLabelsTooltip, CaVESWindow.GetMeleeWeaponSpeedLabelsTooltipLine2, 0 )
			elseif ( currentWindowName == windowName.."MeleeWeaponDPSLHWindow" ) or ( currentWindowName == windowName.."MeleeWeaponDPSLHIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MeleeWeaponDPSLHWindow", windowName.."MeleeWeaponDPSLHIconWindow", CaVESWindow.GetMeleeWeaponDPSLabelsTooltip, CaVESWindow.GetMeleeWeaponDPSLabelsTooltipLine2, 1 )
			elseif ( currentWindowName == windowName.."MeleeSpeedLHWindow" ) or ( currentWindowName == windowName.."MeleeSpeedLHIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MeleeSpeedLHWindow", windowName.."MeleeSpeedLHIconWindow", CaVESWindow.GetMeleeWeaponSpeedLabelsTooltip, CaVESWindow.GetMeleeWeaponSpeedLabelsTooltipLine2, 1 )
			elseif ( currentWindowName == windowName.."MeleePowerBonusWindow" ) or ( currentWindowName == windowName.."MeleePowerBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MeleePowerBonusWindow", windowName.."MeleeDamageBonusIconWindow", CaVESWindow.GetPowerBonusLabelsTooltip, CaVESWindow.GetPowerBonusLabelsTooltip2, 1 )
			elseif ( currentWindowName == windowName.."MeleeDamageBonusWindow" ) or ( currentWindowName == windowName.."MeleeDamageBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MeleeDamageBonusWindow", windowName.."MeleeDamageBonusIconWindow", CharacterWindow.GetMeleeLabelsTooltip, CharacterWindow.GetMeleeLabelsTooltipLine2, 2 )
			elseif ( currentWindowName == windowName.."MeleeArmorPenetrationWindow" ) or ( currentWindowName == windowName.."MeleeArmorPenetrationIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MeleeArmorPenetrationWindow", windowName.."MeleeArmorPenetrationIconWindow", CharacterWindow.GetMeleeLabelsTooltip, CharacterWindow.GetMeleeLabelsTooltipLine2, 3 )
			elseif ( currentWindowName == windowName.."MeleeCriticalHitBonusWindow" ) or ( currentWindowName == windowName.."MeleeCriticalHitBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MeleeCriticalHitBonusWindow", windowName.."MeleeCriticalHitBonusIconWindow", CharacterWindow.GetMeleeLabelsTooltip, CharacterWindow.GetMeleeLabelsTooltipLine2, 6 )

			-- Ranged
			elseif ( currentWindowName == windowName.."RangedWeaponDPSWindow" ) or ( currentWindowName == windowName.."RangedWeaponDPSIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."RangedWeaponDPSWindow", windowName.."RangedWeaponDPSIconWindow", CharacterWindow.GetRangedLabelsTooltip, CharacterWindow.GetRangedLabelsTooltipLine2, 1 )
			elseif ( currentWindowName == windowName.."RangedSpeedWindow" ) or ( currentWindowName == windowName.."RangedSpeedIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."RangedSpeedWindow", windowName.."RangedSpeedIconWindow", CharacterWindow.GetRangedLabelsTooltip, CharacterWindow.GetRangedLabelsTooltipLine2, 2 )
			elseif ( currentWindowName == windowName.."RangedPowerBonusWindow" ) or ( currentWindowName == windowName.."RangedPowerBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."RangedPowerBonusWindow", windowName.."RangedPowerBonusIconWindow", CaVESWindow.GetPowerBonusLabelsTooltip, CaVESWindow.GetPowerBonusLabelsTooltip2, 2 )
			elseif ( currentWindowName == windowName.."RangedDamageBonusWindow" ) or ( currentWindowName == windowName.."RangedDamageBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."RangedDamageBonusWindow", windowName.."RangedDamageBonusIconWindow", CharacterWindow.GetRangedLabelsTooltip, CharacterWindow.GetRangedLabelsTooltipLine2, 3 )
			elseif ( currentWindowName == windowName.."RangedCriticalHitBonusWindow" ) or ( currentWindowName == windowName.."RangedCriticalHitBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."RangedCriticalHitBonusWindow", windowName.."RangedCriticalHitBonusIconWindow", CharacterWindow.GetRangedLabelsTooltip, CharacterWindow.GetRangedLabelsTooltipLine2, 4 )

			-- Magic
			elseif ( currentWindowName == windowName.."MagicPowerBonusWindow" ) or ( currentWindowName == windowName.."MagicPowerBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MagicPowerBonusWindow", windowName.."MagicPowerBonusIconWindow", CaVESWindow.GetPowerBonusLabelsTooltip, CaVESWindow.GetPowerBonusLabelsTooltip2, 3 )
			elseif ( currentWindowName == windowName.."MagicDamageBonusWindow" ) or ( currentWindowName == windowName.."MagicDamageBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MagicDamageBonusWindow", windowName.."MagicDamageBonusIconWindow", CharacterWindow.GetMagicLabelsTooltip, CharacterWindow.GetMagicLabelsTooltipLine2, 1 )
			elseif ( currentWindowName == windowName.."MagicCriticalHitBonusAttackWindow" ) or ( currentWindowName == windowName.."MagicCriticalHitBonusAttackIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MagicCriticalHitBonusAttackWindow", windowName.."MagicCriticalHitBonusAttackIconWindow", CharacterWindow.GetMagicLabelsTooltip, CharacterWindow.GetMagicLabelsTooltipLine2, 2 )
			elseif ( currentWindowName == windowName.."MagicHealingPowerBonusWindow" ) or ( currentWindowName == windowName.."MagicHealingPowerBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MagicHealingPowerBonusWindow", windowName.."MagicHealingPowerBonusIconWindow", CaVESWindow.GetPowerBonusLabelsTooltip, CaVESWindow.GetPowerBonusLabelsTooltip2, 4 )
			elseif ( currentWindowName == windowName.."MagicHealingBonusWindow" ) or ( currentWindowName == windowName.."MagicHealingBonusIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MagicHealingBonusWindow", windowName.."MagicHealingBonusIconWindow", CharacterWindow.GetMagicLabelsTooltip, CharacterWindow.GetMagicLabelsTooltipLine2, 3 )
			elseif ( currentWindowName == windowName.."MagicCriticalHitBonusHealWindow" ) or ( currentWindowName == windowName.."MagicCriticalHitBonusHealIconWindow" ) then
					CaVESWindow.DisplayStatsTooltip( currentWindowName, windowName.."MagicCriticalHitBonusHealWindow", windowName.."MagicCriticalHitBonusHealIconWindow", CharacterWindow.GetMagicLabelsTooltip, CharacterWindow.GetMagicLabelsTooltipLine2, 4 )
			end

		end

end

function CaVESWindow.GetMeleeWeaponDPSLabelsTooltip(hand)
	local retString = L""
	if ( hand == 0 ) then
	    if ( CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps > 0 ) then
	    		retString = GetString( StringTables.Default.LABEL_WEAPON_DPS )..L": "
	        retString = retString..wstring.format(L"%.01f",CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps)
	    else
	        retString = GetString( StringTables.Default.LABEL_WEAPON_DPS )..L": "
	        retString = retString..L"0"
	    end
	elseif ( hand == 1 ) then
	    if ( CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps > 0 ) then
	        retString = GetString( StringTables.Default.LABEL_WEAPON_DPS )..L": "
	        retString = retString..wstring.format( L"%.01f",CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps )
	    else
	        retString = GetString( StringTables.Default.LABEL_WEAPON_DPS )..L": "
	        retString = retString..L"0"
	    end	
	end

	return retString
end

function CaVESWindow.GetMeleeWeaponDPSLabelsTooltipLine2 (hand)
	local retString = L""
	retString = LOC_TEXT.TOOLTIP_MELEEWEAPDPS
	
	if ( hand == 0 ) then
			retString = retString..LOC_TEXT.TOOLTIP_MELEEWEAPDPSRH
			retString = retString..wstring.format( L"%.01f",CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps )
	elseif ( hand == 1 ) then
			retString = retString..LOC_TEXT.TOOLTIP_MELEEWEAPDPSLH
			retString = retString..wstring.format( L"%.01f",CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps )
	end

	return retString
end

function CaVESWindow.GetMeleeWeaponSpeedLabelsTooltip(hand)
	local retString = L""
	retString = GetString( StringTables.Default.LABEL_SPEED )..L": "
	
	if ( hand == 0 ) then
	    retString = retString..wstring.format( L"%.01f", currentMeleeSpeedRHVal )

	elseif ( hand == 1 ) then
			retString = retString..wstring.format( L"%.01f", currentMeleeSpeedLHVal )
	end

	return retString
end

function CaVESWindow.GetMeleeWeaponSpeedLabelsTooltipLine2 (hand)
	local retString = L""
	if ( hand == 0 ) then
			retString = LOC_TEXT.TOOLTIP_MELEEWEAPDPSDESC
			retString = retString..LOC_TEXT.TOOLTIP_MELEEWEAPSPEEDRH
			retString = retString..wstring.format( L"%.01f", currentMeleeSpeedRHVal )
	elseif ( hand == 1 ) then
			retString = retString..LOC_TEXT.TOOLTIP_MELEEWEAPSPEEDDESC
			retString = retString..LOC_TEXT.TOOLTIP_MELEEWEAPSPEEDLH
			retString = retString..wstring.format( L"%.01f", currentMeleeSpeedLHVal )
	end

	return retString
end


-- Tool tip function to display "power" bonus information (Fortitude, Melee, Ranged, Magic, and Healing Power)
function CaVESWindow.GetPowerBonusLabelsTooltip(bonustype)
	local retString = L""
	if ( bonustype == 0 ) then -- Fortitude
			retString = GetString( StringTables.Default.LABEL_FORTITUDE )..L": "
			retString = retString..currentStatsFortitudeVal
	elseif ( bonustype == 1 ) then -- Melee Power
	    retString = GetString( StringTables.Default.LABEL_BONUS_DAMAGE_MELEE )..L": "
			retString = retString..currentMeleePowerBonusVal
	elseif ( bonustype == 2 ) then -- Ranged Power
	    retString = GetString( StringTables.Default.LABEL_BONUS_DAMAGE_RANGED )..L": "
			retString = retString..currentRangedPowerBonusVal
	elseif ( bonustype == 3 ) then -- Magic Power
	    retString = GetString( StringTables.Default.LABEL_BONUS_DAMAGE_MAGIC )..L": "
			retString = retString..currentMagicPowerBonusVal
	elseif ( bonustype == 4 ) then -- Healing Power
	    retString = GetString( StringTables.Default.LABEL_BONUS_HEALING_POWER )..L": "
			retString = retString..currentMagicHealingPowerBonusVal
	end

	return retString
end

-- Tool tip function to display "power" bonus information (Fortitude, Melee, Ranged, Magic, and Healing Power)
function CaVESWindow.GetPowerBonusLabelsTooltip2(bonustype)
	local retString = L""
	if ( bonustype == 0 ) then -- Fortitude
			retString = LOC_TEXT.TOOLTIP_FORTITUDEDESC
			retString = retString..GetString( StringTables.Default.LABEL_FORTITUDE )..L": "
			retString = retString..currentStatsFortitudeVal..L"<BR>"
			retString = retString..GetString( StringTables.Default.LABEL_FORTITUDE )..L"("..GetString( StringTables.Default.LABEL_BONUS_INC_DMG)..L")"..L": "
			retString = retString..L"-"..(CaVESWindow.GetTotalBonusPower(0, GameData.BonusTypes.EBONUS_FORTITUDE))..L""..GetString( StringTables.Default.LABEL_DPS )
	elseif ( bonustype == 1 ) then -- Melee Power
			retString = retString..LOC_TEXT.TOOLTIP_MELEEPOWERDESC
			retString = retString..GetString( StringTables.Default.LABEL_BONUS_DAMAGE_MELEE )..L": "
			retString = retString..currentMeleePowerBonusVal..L"<BR>"
			retString = retString..GetString( StringTables.Default.LABEL_BONUS_DAMAGE_MELEE )..L"("..GetString( StringTables.Default.LABEL_DAMAGE)..L")"..L": "
			retString = retString..L"+"..(CaVESWindow.GetTotalBonusPower(0, GameData.BonusTypes.EBONUS_DAMAGE_MELEE))..L""..GetString( StringTables.Default.LABEL_DPS )
	elseif ( bonustype == 2 ) then -- Ranged Power
			retString = retString..LOC_TEXT.TOOLTIP_RANGEDPOWERDESC
			retString = retString..GetString( StringTables.Default.LABEL_BONUS_DAMAGE_RANGED )..L": "
			retString = retString..currentRangedPowerBonusVal..L"<BR>"
			retString = retString..GetString( StringTables.Default.LABEL_BONUS_DAMAGE_RANGED )..L"("..GetString( StringTables.Default.LABEL_DAMAGE)..L")"..L": "
			retString = retString..L"+"..(CaVESWindow.GetTotalBonusPower(0, GameData.BonusTypes.EBONUS_DAMAGE_RANGED))..L""..GetString( StringTables.Default.LABEL_DPS )
	elseif ( bonustype == 3 ) then -- Magic Power
			retString = retString..LOC_TEXT.TOOLTIP_MAGICPOWERDESC
			retString = retString..GetString( StringTables.Default.LABEL_BONUS_DAMAGE_MAGIC )..L": "
			retString = retString..currentMagicPowerBonusVal..L"<BR>"
			retString = retString..GetString( StringTables.Default.LABEL_BONUS_DAMAGE_MAGIC )..L"("..GetString( StringTables.Default.LABEL_DAMAGE)..L")"..L": "
			retString = retString..L"+"..(CaVESWindow.GetTotalBonusPower(0, GameData.BonusTypes.EBONUS_DAMAGE_MAGIC))..L""..GetString( StringTables.Default.LABEL_DPS )
	elseif ( bonustype == 4 ) then -- Healing Power
			retString = retString..LOC_TEXT.TOOLTIP_HEALINGPOWERDESC
			retString = retString..GetString( StringTables.Default.LABEL_BONUS_HEALING_POWER )..L": "
			retString = retString..currentMagicHealingPowerBonusVal..L"<BR>"
			retString = retString..GetString( StringTables.Default.LABEL_BONUS_HEALING_POWER )..L"("..GetString( StringTables.Default.LABEL_HEALING)..L")"..L": "
			retString = retString..L"+"..(CaVESWindow.GetTotalBonusPower(0, GameData.BonusTypes.EBONUS_HEALING_POWER))..L""..GetString( StringTables.Default.LABEL_HPS )
	end

	return retString
end


------------------------------------------------------------------
---- Stat Update Functions
------------------------------------------------------------------

-- For each event that can cause a stat change, enable the timer
-- and set a flag denoting which event(s) occurred.
function CaVESWindow.StatChangeEventEquipment()
		bPlayerEquipmentSlotUpdated = true
		bTimerEnable = true
end

function CaVESWindow.StatChangeEventStats()
		bPlayerStatsUpdated = true
		bTimerEnable = true
end

function CaVESWindow.StatChangeEventTrophy()
		bPlayerTrophySlotUpdated = true
		bTimerEnable = true
end

function CaVESWindow.StatChangeEventEffects()
		bPlayerEffectsUpdated = true
		bTimerEnable = true
end

-- After a brief pause created by the timer, all the events that
-- can fire due to an item/stat/trophy/effect change should now
-- be done (have caught up) and have set a boolean flag.  Only 
-- one event is chosen to update the stat labels, multiple stat 
-- label updates cause the recent value differences to be zero 
-- after the first one occurs, resulting in what appears to the
-- end user as an error in the value displays.
function CaVESWindow.UpdateStatsLabelsQueue()
		-- Equipment slot changes are special and take priority, this
		-- especially holds true for main hand(right hand) weapons
		-- and related stats.
		if (bPlayerEquipmentSlotUpdated == true) then
				bPlayerEquipmentSlotUpdated = false
				bPlayerStatsUpdated = false
				bPlayerTrophySlotUpdated = false
				bPlayerEffectsUpdated = false
				CaVESWindow.UpdateStatsLabels()
		elseif (bPlayerStatsUpdated == true) then
				bPlayerEquipmentSlotUpdated = false
				bPlayerStatsUpdated = false
				bPlayerTrophySlotUpdated = false
				bPlayerEffectsUpdated = false
				CaVESWindow.UpdateStatsLabels()
		elseif (bPlayerTrophySlotUpdated == true) then
				bPlayerEquipmentSlotUpdated = false
				bPlayerStatsUpdated = false
				bPlayerTrophySlotUpdated = false
				bPlayerEffectsUpdated = false
				CaVESWindow.UpdateStatsLabels()
		elseif (bPlayerEffectsUpdated == true) then
				bPlayerEquipmentSlotUpdated = false
				bPlayerStatsUpdated = false
				bPlayerTrophySlotUpdated = false
				bPlayerEffectsUpdated = false
				CaVESWindow.UpdateStatsLabels()
		end
end

function CaVESWindow.UpdateStatsLabels()
    CaVESWindow.UpdateStrengthLabel( windowName.."StatsStrengthWindow" )
    CaVESWindow.UpdateBallisticSkillLabel( windowName.."StatsBallisticSkillWindow" )
    CaVESWindow.UpdateIntelligenceLabel( windowName.."StatsIntelligenceWindow" )
    CaVESWindow.UpdateToughnessLabel( windowName.."StatsToughnessWindow" )
    CaVESWindow.UpdateFortitudeLabel( windowName.."StatsFortitudeWindow" )
    CaVESWindow.UpdateWeaponSkillLabel( windowName.."StatsWeaponSkillWindow" )
    CaVESWindow.UpdateInitiativeLabel( windowName.."StatsInitiativeWindow" )
    CaVESWindow.UpdateWillpowerLabel( windowName.."StatsWillpowerWindow" )
    CaVESWindow.UpdateWoundsLabel( windowName.."StatsWoundsWindow" )
    
    -- The following function in the Character View window now needs called due to global variables that are now being set
    -- (as of patch 1.2.1) in this function that are needed for mouseover tooltips.  The function is not called until 
    -- the pull down stats window in the Character View window is set to the Melee stats and therefore these variables 
    -- do not get initialized for CaVES to use.
    CharacterWindow.UpdateDefenseLabels()
		-- Quick check to prevent unnecessary stat calculations
    if CharacterWindow.currentStatSelection ~= 2 then	
    	CharacterWindow.UpdateStatsNew() -- Make sure Stats Window in Character Viewer displays proper stat set and information.
    end
    CaVESWindow.UpdateArmorLabel( windowName.."DefenseArmorWindow" )
    CaVESWindow.UpdateSpiritualResistanceLabel( windowName.."DefenseSpiritualResistanceWindow" )
    CaVESWindow.UpdateCorporealResistanceLabel( windowName.."DefenseCorporealResistanceWindow" )
    CaVESWindow.UpdateElementalResistanceLabel( windowName.."DefenseElementalResistanceWindow" )
    CaVESWindow.UpdateBlockLabel( windowName.."DefenseBlockWindow" )
    CaVESWindow.UpdateParryLabel( windowName.."DefenseParryWindow" )
    CaVESWindow.UpdateDodgeLabel( windowName.."DefenseDodgeWindow" )
    CaVESWindow.UpdateDisruptLabel( windowName.."DefenseDisruptWindow" )

	  CaVESWindow.UpdateMeleeWeaponDPSRHLabel( windowName.."MeleeWeaponDPSRHWindow" )
    CaVESWindow.UpdateMeleeSpeedRHLabel( windowName.."MeleeSpeedRHWindow" )
	  CaVESWindow.UpdateMeleeWeaponDPSLHLabel( windowName.."MeleeWeaponDPSLHWindow" )
    CaVESWindow.UpdateMeleeSpeedLHLabel( windowName.."MeleeSpeedLHWindow" )
    CaVESWindow.UpdateMeleePowerBonusLabel( windowName.."MeleePowerBonusWindow" )
    CaVESWindow.UpdateMeleeDamageBonusLabel( windowName.."MeleeDamageBonusWindow" )
    CaVESWindow.UpdateArmorPenetrationLabel( windowName.."MeleeArmorPenetrationWindow" )
    CaVESWindow.UpdateMeleeCriticalHitBonusLabel( windowName.."MeleeCriticalHitBonusWindow" )

    CaVESWindow.UpdateRangedWeaponDPSLabel( windowName.."RangedWeaponDPSWindow" )
    CaVESWindow.UpdateRangedSpeedLabel( windowName.."RangedSpeedWindow" )
    CaVESWindow.UpdateRangedPowerBonusLabel( windowName.."RangedPowerBonusWindow" )
    CaVESWindow.UpdateRangedDamageBonusLabel( windowName.."RangedDamageBonusWindow" )
    CaVESWindow.UpdateRangedCriticalHitBonusLabel( windowName.."RangedCriticalHitBonusWindow" )

		CaVESWindow.UpdateMagicPowerBonusLabel( windowName.."MagicPowerBonusWindow" )
    CaVESWindow.UpdateMagicDamageBonusLabel( windowName.."MagicDamageBonusWindow" )
    CaVESWindow.UpdateMagicCriticalHitBonusAttackLabel( windowName.."MagicCriticalHitBonusAttackWindow" )
    CaVESWindow.UpdateMagicHealingPowerBonusLabel( windowName.."MagicHealingPowerBonusWindow" )
		CaVESWindow.UpdateMagicHealingBonusLabel( windowName.."MagicHealingBonusWindow" )
    CaVESWindow.UpdateMagicCriticalHitBonusHealLabel( windowName.."MagicCriticalHitBonusHealWindow" )
   
	  -- Check if CaVES Window has now been initialized along with the associated stats. This is a backup check
	  -- that exists primarily to ensure that item sets have had a chance to update so that initial start or
	  -- reference stat values are correct.
	  if bRegisteredForItemSetData == true then
	  		-- Item set data has not been updated yet, so allow CaVES initialization to continue
	  		bCaVESInitialization = true
	  elseif bRegisteredForItemSetData == false then
	  		-- Item set data has been updated, so set the CaVES initialization to false.
	  		bCaVESInitialization = false
	  end
   
   	-- Check if the Reference Values should now be updated/saved.
   	if bSaveRefValues == true then
   		CaVESWindow.SaveRefValues()
   		bSaveRefValues = false
   	end
end

function CaVESWindow.ResetRefValues()
		bResetReferenceValues = true
		CaVESWindow.UpdateStatsLabels()
		bResetReferenceValues = false
		
		-- If Reference Values are set to option 2, which is persistent 
		-- between game sessions, then save the reset values as the new
		-- Reference Values.
		if CaVES.Settings.RefStatsValueSetting == 2 then
				CaVESWindow.SaveRefValues()
		end
end

function CaVESWindow.SaveRefValues()
		-- Stats Reference Values
		CaVESWindow.RefValues[serverName][characterName].StatsStrengthVal 							= startStatsStrengthVal
		CaVESWindow.RefValues[serverName][characterName].StatsBallisticSkillVal 				= startStatsBallisticSkillVal
		CaVESWindow.RefValues[serverName][characterName].StatsIntelligenceVal          	=	startStatsIntelligenceVal
		CaVESWindow.RefValues[serverName][characterName].StatsToughnessVal             	=	startStatsToughnessVal
		CaVESWindow.RefValues[serverName][characterName].StatsFortitudeVal             	=	startStatsFortitudeVal
		CaVESWindow.RefValues[serverName][characterName].StatsWeaponSkillVal           	= startStatsWeaponSkillVal
		CaVESWindow.RefValues[serverName][characterName].StatsInitiativeVal            	=	startStatsInitiativeVal
		CaVESWindow.RefValues[serverName][characterName].StatsWillpowerVal             	=	startStatsWillpowerVal
		CaVESWindow.RefValues[serverName][characterName].StatsWoundsVal                	=	startStatsWoundsVal
		-- Defense Reference Values      
		CaVESWindow.RefValues[serverName][characterName].DefenseArmorVal               	= startDefenseArmorVal
		CaVESWindow.RefValues[serverName][characterName].DefenseSpiritualResistanceVal 	=	startDefenseSpiritualResistanceVal
		CaVESWindow.RefValues[serverName][characterName].DefenseCorporealResistanceVal 	=	startDefenseCorporealResistanceVal
		CaVESWindow.RefValues[serverName][characterName].DefenseElementalResistanceVal 	=	startDefenseElementalResistanceVal
		CaVESWindow.RefValues[serverName][characterName].DefenseBlockVal               	=	startDefenseBlockVal
		CaVESWindow.RefValues[serverName][characterName].DefenseParryVal               	=	startDefenseParryVal
		CaVESWindow.RefValues[serverName][characterName].DefenseDodgeVal               	=	startDefenseDodgeVal
		CaVESWindow.RefValues[serverName][characterName].DefenseDisruptVal             	=	startDefenseDisruptVal
		-- Melee Reference Values             
		CaVESWindow.RefValues[serverName][characterName].MeleeWeaponDPSRHVal           	=	startMeleeWeaponDPSRHVal
		CaVESWindow.RefValues[serverName][characterName].MeleeSpeedRHVal               	=	startMeleeSpeedRHVal
		CaVESWindow.RefValues[serverName][characterName].MeleeWeaponDPSLHVal           	=	startMeleeWeaponDPSLHVal
		CaVESWindow.RefValues[serverName][characterName].MeleeSpeedLHVal               	=	startMeleeSpeedLHVal
		CaVESWindow.RefValues[serverName][characterName].MeleePowerBonusVal           	=	startMeleePowerBonusVal
		CaVESWindow.RefValues[serverName][characterName].MeleeDamageBonusVal           	=	startMeleeDamageBonusVal
		CaVESWindow.RefValues[serverName][characterName].MeleeArmorPenetrationVal      	=	startMeleeArmorPenetrationVal
		CaVESWindow.RefValues[serverName][characterName].MeleeCriticalHitBonusVal      	=	startMeleeCriticalHitBonusVal
		-- Ranged Reference Values                   
		CaVESWindow.RefValues[serverName][characterName].RangedWeaponDPSVal            	=	startRangedWeaponDPSVal
		CaVESWindow.RefValues[serverName][characterName].RangedSpeedVal                	=	startRangedSpeedVal
		CaVESWindow.RefValues[serverName][characterName].RangedPowerBonusVal          	=	startRangedPowerBonusVal
		CaVESWindow.RefValues[serverName][characterName].RangedDamageBonusVal          	=	startRangedDamageBonusVal
		CaVESWindow.RefValues[serverName][characterName].RangedCriticalHitBonusVal     	=	startRangedCriticalHitBonusVal
	  -- Magic Reference Values                      
		CaVESWindow.RefValues[serverName][characterName].MagicPowerBonusVal           	=	startMagicPowerBonusVal
		CaVESWindow.RefValues[serverName][characterName].MagicDamageBonusVal           	=	startMagicDamageBonusVal
		CaVESWindow.RefValues[serverName][characterName].MagicCriticalHitBonusAttackVal	=	startMagicCriticalHitBonusAttackVal
		CaVESWindow.RefValues[serverName][characterName].MagicHealingPowerBonusVal		 	=	startMagicHealingPowerBonusVal
		CaVESWindow.RefValues[serverName][characterName].MagicHealingBonusVal						=	startMagicHealingBonusVal
		CaVESWindow.RefValues[serverName][characterName].MagicCriticalHitBonusHealVal  	=	startMagicCriticalHitBonusHealVal
end

function CaVESWindow.ResetRecentValues()
	CaVESWindow.UpdateStatsLabels()
end

function CaVESWindow.ToggleIgnoreTalismans()
		EA_LabelCheckButton.Toggle()
		if (ButtonGetPressedFlag( windowName.."ToggleTalismansButton" ) == false ) then
			bIgnoreTalismans = false
			CaVES.Settings.IgnoreTalismans = false
			CaVESWindow.UpdateStatsLabels()
		elseif (ButtonGetPressedFlag( windowName.."ToggleTalismansButton" ) == true ) then
			bIgnoreTalismans = true
			CaVES.Settings.IgnoreTalismans = true
			CaVESWindow.UpdateStatsLabels()
		end
end

function CaVESWindow.ToggleIgnoreEventSlot()
		EA_LabelCheckButton.Toggle()
		if (ButtonGetPressedFlag( windowName.."ToggleEventSlotButton" ) == false ) then
			bIgnoreEventSlotItem = false
			CaVES.Settings.IgnoreEventSlotItem = false
			CaVESWindow.UpdateStatsLabels()
		elseif (ButtonGetPressedFlag( windowName.."ToggleEventSlotButton" ) == true ) then
			bIgnoreEventSlotItem = true
			CaVES.Settings.IgnoreEventSlotItem = true
			CaVESWindow.UpdateStatsLabels()
		end
end

function CaVESWindow.UpdateLabel( wndName, startStatVal, startStatValText, diffStartCurVal, diffStartCurValText, 
																 prevStatVal, curStatVal, curStatValText, diffPrevCurVal, diffPrevCurValText, baseStatVal, 
																 bCurStatDiminished, bRefStatDiminished, bFlipColorCodes, coreStartStatVal, coreBaseStatVal, 
																 coreCurrentStatVal )
    seperatorText = L"/"
    LabelSetText( wndName.."StatsRefLabelValue", startStatValText )
    LabelSetText( wndName.."StatsRefLabelSeperator", seperatorText )
    LabelSetText( wndName.."StatsRecentLabelValue", curStatValText )
    LabelSetText( wndName.."StatsRecentLabelSeperator", seperatorText )
    
    -- Define the color codes to use for positive and negative stat changes
		local posStatChangeColor = CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN		
		local negStatChangeColor = CaVESWindow.COLOR_CODE_STAT_CHANGE_RED
		
		-- If the boolean for flipping the color code is true, then flip the color codes. This is primarily
		-- for Weapon Speed values, which should be color coded GREEN for a DECREASE in speed since this is
		-- a desired stat change and RED for an INCREASE in speed since this is considered less desirable since
		-- the player will typically want a faster Weapon Speed and Weapon Speeds decrease as weapon class or
		-- level increases for character and item/equipment progression.
		if bFlipColorCodes == true then
			posStatChangeColor = CaVESWindow.COLOR_CODE_STAT_CHANGE_RED	
			negStatChangeColor = CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN
		end

		-- Reference value display and color coding
		if (coreStartStatVal ~= nil) and (coreBaseStatVal ~= nil) and (coreCurrentStatVal ~= nil) then
			if coreStartStatVal > coreBaseStatVal then
	        LabelSetTextColor( wndName.."StatsRefLabelValue", CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.r, CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.g, CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.b )
	    elseif coreCurrentStatVal < coreBaseStatVal then
	        LabelSetTextColor( wndName.."StatsRefLabelValue", CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.r, CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.g, CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.b )
	    else
	        LabelSetTextColor( wndName.."StatsRefLabelValue", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
	    end		
		else
			if bRefStatDiminished == true then
					LabelSetTextColor( wndName.."StatsRefLabelValue", DefaultColor.ChatTextColors[23].r, DefaultColor.ChatTextColors[23].g, DefaultColor.ChatTextColors[23].b )
			else
			    if (startStatVal > baseStatVal) and ((startStatVal - baseStatVal) >= 0.1) then -- additional check compensates for floating point numbers truncated 
			        																																					 -- and saved for CaVES.Settings.RefStatsValueSetting = 2 
			        LabelSetTextColor( wndName.."StatsRefLabelValue", CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.r, CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.g, CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.b )
			    elseif curStatVal < baseStatVal then
			        LabelSetTextColor( wndName.."StatsRefLabelValue", CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.r, CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.g, CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.b )
			    else
			        LabelSetTextColor( wndName.."StatsRefLabelValue", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
			    end
			end
	  end

		-- Reference value difference display and color coding
    if diffStartCurVal > 0 then
        LabelSetText( wndName.."StatsRefLabelDiff", L"+"..diffStartCurValText )
        LabelSetTextColor( wndName.."StatsRefLabelDiff", posStatChangeColor.r, posStatChangeColor.g, posStatChangeColor.b )
    elseif diffStartCurVal < 0 and (math.abs(diffStartCurVal) >= 0.1) then -- additional check compensates for floating point numbers truncated and saved 
    		LabelSetText( wndName.."StatsRefLabelDiff", L""..diffStartCurValText )
        LabelSetTextColor( wndName.."StatsRefLabelDiff", negStatChangeColor.r, negStatChangeColor.g, negStatChangeColor.b )
    else
    		LabelSetText( wndName.."StatsRefLabelDiff", L"+"..diffStartCurValText )
        LabelSetTextColor( wndName.."StatsRefLabelDiff", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
    end

		-- Recent (current) value display and color coding
		if (coreStartStatVal ~= nil) and (coreBaseStatVal ~= nil) and (coreCurrentStatVal ~= nil) then
	    if coreCurrentStatVal > coreBaseStatVal then
	        LabelSetTextColor( wndName.."StatsRecentLabelValue", CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.r, CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.g, CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.b )
	    elseif coreCurrentStatVal < coreBaseStatVal then
	        LabelSetTextColor( wndName.."StatsRecentLabelValue", CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.r, CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.g, CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.b )
	    else
	        LabelSetTextColor( wndName.."StatsRecentLabelValue", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
	    end
		else
			if bCurStatDiminished == true then
					LabelSetTextColor( wndName.."StatsRecentLabelValue", DefaultColor.ChatTextColors[23].r, DefaultColor.ChatTextColors[23].g, DefaultColor.ChatTextColors[23].b )
			else
			    if curStatVal > baseStatVal then
			        LabelSetTextColor( wndName.."StatsRecentLabelValue", CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.r, CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.g, CaVESWindow.COLOR_CODE_STAT_CHANGE_GREEN.b )
			    elseif curStatVal < baseStatVal then
			        LabelSetTextColor( wndName.."StatsRecentLabelValue", CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.r, CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.g, CaVESWindow.COLOR_CODE_STAT_CHANGE_RED.b )
			    else
			        LabelSetTextColor( wndName.."StatsRecentLabelValue", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
			    end
			end
	  end

		-- Recent (current) value difference display and color coding
    if diffPrevCurVal > 0 then
        LabelSetText( wndName.."StatsRecentLabelDiff", L"+"..diffPrevCurValText )
        LabelSetTextColor( wndName.."StatsRecentLabelDiff", posStatChangeColor.r, posStatChangeColor.g, posStatChangeColor.b )
    elseif diffPrevCurVal < 0 then
    		LabelSetText( wndName.."StatsRecentLabelDiff", L""..diffPrevCurValText )
        LabelSetTextColor( wndName.."StatsRecentLabelDiff", negStatChangeColor.r, negStatChangeColor.g, negStatChangeColor.b )
    else
    		LabelSetText( wndName.."StatsRecentLabelDiff", L"+"..diffPrevCurValText )
        LabelSetTextColor( wndName.."StatsRecentLabelDiff", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b )
    end
end

---- Stat and Stat Bonus Calculation Functions ----

-- Calculate the value of the stat after diminishing returns has been applied to the current value,
-- this function tries to mimic the servers behavior.
-- NOTE: This function had to be copied over from characterwindow.lua because it was made local in that script
-- and was not able to be accessed by CaVES.
local function GetEffectiveStatValue( baseValue, currentValue )
    local thresholdMult = 25
    local threshold = GameData.Player.battleLevelWithRenown * thresholdMult + 50
    local capMult = 40
    local cap = GameData.Player.battleLevelWithRenown * capMult + 50

    -- After the dimishing returns kick in, we only get half the effectiveness from stats
    if( currentValue > threshold )
    then
        currentValue = ( currentValue - threshold ) / 2 + threshold
    end
    
    -- An absolute stat cap.... terrible design, but theoretically only happens if items are being created with bad parameters
    if( currentValue > cap )
    then
        currentValue = cap
    end
    
    if( baseValue > currentValue )
    then
        baseValue = currentValue
    end
    
    -- Truncate for nicer output
    baseValue = math.floor( baseValue )
    currentValue = math.floor( currentValue ) 
    
    return baseValue, currentValue
end

function CaVESWindow.CalculateValueWithBonus(bonusType, baseValue)
		local bonus = GetBonus(bonusType, baseValue)

		-- If Ignore Talismans is true, then subtract Talisman values in equipment enhancement slots from stat values
		if bIgnoreTalismans == true then
			bonus = bonus - CaVESWindow.CalculateBonusValueFromTalismans(bonusType)
		end

		-- If Ignore Event Item is true, then subtract Event Item stat values in from stat values
		if bIgnoreEventSlotItem == true then
			bonus = bonus - CaVESWindow.CalculateBonusValueFromEventItem(bonusType)
		end		
		
		return bonus
end

-- Calculate the total bonus value for the stat
-- NOTE: This function had to be copied over from characterwindow.lua because it was made local in that script
-- and was not able to be accessed by CaVES.
function CaVESWindow.GetTotalBonusPower( statValue, bonusType )
    local statAddition = statValue / 5

    local bonusAddition
    
    -- If Ignore Talismans is true, then subtract Talisman values in equipment enhancement slots from bonus calculation
    if bIgnoreTalismans == true then
    	bonusAddition = (CaVESWindow.CalculateValueWithBonus( bonusType, 0 ) - (CaVESWindow.CalculateBonusValueFromTalismans(bonusType))) / 5
    else
    	bonusAddition = CaVESWindow.CalculateValueWithBonus( bonusType, 0 ) / 5
    end
    
    local total = statAddition + bonusAddition
    return wstring.format( L"%.01f", total )
end

-- Function that calculates the total stat bonus value contributed by Talismans.
function CaVESWindow.CalculateBonusValueFromTalismans(bonusType)
    local talisman_bonus = 0
    for slot, item in ipairs( CharacterWindow.equipmentData )
    do
				if (item.enhSlot ~= nil) then
				    for enhSlotIndex = 1, item.numEnhancementSlots
				    do
				    		if (item.enhSlot[enhSlotIndex] ~= nil) then
				        		if (item.enhSlot[enhSlotIndex].bonus ~= nil) then
				            		for bonusSlot, bonusSlotData in ipairs( item.enhSlot[enhSlotIndex].bonus )
				                do
				                		if bonusSlotData.reference == bonusType then
				                    	talisman_bonus = talisman_bonus + bonusSlotData.value
				                    end
				                end
				            end
				        end
				    end
				end
		end
    return talisman_bonus
end

-- Function that calculates the total stat bonus value contributed by an item equipped in the Event slot.
function CaVESWindow.CalculateBonusValueFromEventItem(bonusType)
    local event_item_bonus = 0
		for slot, item in ipairs( CharacterWindow.equipmentData )
    do
    	if slot == 15 then
				if (item.bonus ~= nil) then
					for bonus, bonusSlotData in ipairs (item.bonus)
					do
						if bonusSlotData.reference == bonusType then
							event_item_bonus = event_item_bonus + bonusSlotData.value
						end
					end
				end
			end
		end
		return event_item_bonus
end                                             


---- Stats Update Functions ----

function CaVESWindow.UpdateStrengthLabel( wndName )
		local baseStatsStrengthVal = GameData.Player.Stats[GameData.Stats.STRENGTH].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curStatsStrengthVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_STRENGTH, baseStatsStrengthVal )

		-- Calculate effective stat values.
    baseStatsStrengthVal, curStatsStrengthVal = GetEffectiveStatValue( baseStatsStrengthVal, curStatsStrengthVal )

		if  prevStatsStrengthVal == nil and currentStatsStrengthVal == nil then
				currentStatsStrengthVal = curStatsStrengthVal
				prevStatsStrengthVal = currentStatsStrengthVal
		else 
				if bResetReferenceValues == false then
						prevStatsStrengthVal = currentStatsStrengthVal
				end
		end

		currentStatsStrengthVal = curStatsStrengthVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat..
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsStrengthVal = currentStatsStrengthVal
						startStatsStrengthValText = L""..startStatsStrengthVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it..
						if CaVESWindow.RefValues[serverName][characterName].StatsStrengthVal == nil then
								startStatsStrengthVal = currentStatsStrengthVal
								startStatsStrengthValText = L""..startStatsStrengthVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file..
								startStatsStrengthVal = CaVESWindow.RefValues[serverName][characterName].StatsStrengthVal
								startStatsStrengthValText = L""..startStatsStrengthVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsStrengthVal = currentStatsStrengthVal
				startStatsStrengthValText = L""..startStatsStrengthVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function..
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsStrengthVal = currentStatsStrengthVal
				startStatsStrengthValText = L""..startStatsStrengthVal
		end
		
		local currentStatsStrengthValText = L""..currentStatsStrengthVal

		local diffStartCurVal
		local diffStartCurValText
		
		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsStrengthVal - startStatsStrengthVal)
				diffStartCurValText = L""..diffStartCurVal
		end
		
		local diffPrevCurVal = (currentStatsStrengthVal - prevStatsStrengthVal)
		local diffPrevCurValText = L""..diffPrevCurVal
		
		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsStrengthVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsStrengthVal )
		
		-- Set flip color code boolean
		local bFlipColorCodes = false
		
		CaVESWindow.UpdateLabel( wndName, startStatsStrengthVal, startStatsStrengthValText, diffStartCurVal, diffStartCurValText, 
														prevStatsStrengthVal, currentStatsStrengthVal, currentStatsStrengthValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsStrengthVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateBallisticSkillLabel( wndName )
		local baseStatsBallisticSkillVal = GameData.Player.Stats[GameData.Stats.BALLISTICSKILL].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curStatsBallisticSkillVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_BALLISTICSKILL, baseStatsBallisticSkillVal )
		
		-- Calculate effective stat values..
    baseStatsBallisticSkillVal, curStatsBallisticSkillVal = GetEffectiveStatValue( baseStatsBallisticSkillVal, curStatsBallisticSkillVal )
		
		if  prevStatsBallisticSkillVal == nil and currentStatsBallisticSkillVal == nil then
				currentStatsBallisticSkillVal = curStatsBallisticSkillVal
				prevStatsBallisticSkillVal = currentStatsBallisticSkillVal
		else 
				if bResetReferenceValues == false then
						prevStatsBallisticSkillVal = currentStatsBallisticSkillVal
				end
		end
		
		currentStatsBallisticSkillVal = curStatsBallisticSkillVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat..
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsBallisticSkillVal = currentStatsBallisticSkillVal
						startStatsBallisticSkillValText = L""..startStatsBallisticSkillVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it..
						if CaVESWindow.RefValues[serverName][characterName].StatsBallisticSkillVal == nil then
								startStatsBallisticSkillVal = currentStatsBallisticSkillVal
								startStatsBallisticSkillValText = L""..startStatsBallisticSkillVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file..
								startStatsBallisticSkillVal = CaVESWindow.RefValues[serverName][characterName].StatsBallisticSkillVal
								startStatsBallisticSkillValText = L""..startStatsBallisticSkillVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsBallisticSkillVal = currentStatsBallisticSkillVal
				startStatsBallisticSkillValText = L""..startStatsBallisticSkillVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function..
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsBallisticSkillVal = currentStatsBallisticSkillVal
				startStatsBallisticSkillValText = L""..startStatsBallisticSkillVal
		end
		
		local currentStatsBallisticSkillValText = L""..currentStatsBallisticSkillVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsBallisticSkillVal - startStatsBallisticSkillVal)
				diffStartCurValText = L""..diffStartCurVal
		end
		
		local diffPrevCurVal = (currentStatsBallisticSkillVal - prevStatsBallisticSkillVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsBallisticSkillVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsBallisticSkillVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startStatsBallisticSkillVal, startStatsBallisticSkillValText, diffStartCurVal, diffStartCurValText, 
														prevStatsBallisticSkillVal, currentStatsBallisticSkillVal, currentStatsBallisticSkillValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsBallisticSkillVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateIntelligenceLabel( wndName )
		local baseStatsIntelligenceVal = GameData.Player.Stats[GameData.Stats.INTELLIGENCE].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curStatsIntelligenceVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_INTELLIGENCE, baseStatsIntelligenceVal )

		-- Calculate effective stat values.
    baseStatsIntelligenceVal, curStatsIntelligenceVal = GetEffectiveStatValue( baseStatsIntelligenceVal, curStatsIntelligenceVal )

		if  prevStatsIntelligenceVal == nil and currentStatsIntelligenceVal == nil then
				currentStatsIntelligenceVal = curStatsIntelligenceVal
				prevStatsIntelligenceVal = currentStatsIntelligenceVal
		else 
				if bResetReferenceValues == false then
						prevStatsIntelligenceVal = currentStatsIntelligenceVal
				end
		end

		currentStatsIntelligenceVal = curStatsIntelligenceVal
		
		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsIntelligenceVal = currentStatsIntelligenceVal
						startStatsIntelligenceValText = L""..startStatsIntelligenceVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].StatsIntelligenceVal == nil then
								startStatsIntelligenceVal = currentStatsIntelligenceVal
								startStatsIntelligenceValText = L""..startStatsIntelligenceVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startStatsIntelligenceVal = CaVESWindow.RefValues[serverName][characterName].StatsIntelligenceVal
								startStatsIntelligenceValText = L""..startStatsIntelligenceVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsIntelligenceVal = currentStatsIntelligenceVal
				startStatsIntelligenceValText = L""..startStatsIntelligenceVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsIntelligenceVal = currentStatsIntelligenceVal
				startStatsIntelligenceValText = L""..startStatsIntelligenceVal
		end
		
		local currentStatsIntelligenceValText = L""..currentStatsIntelligenceVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsIntelligenceVal - startStatsIntelligenceVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentStatsIntelligenceVal - prevStatsIntelligenceVal)
		local diffPrevCurValText = L""..diffPrevCurVal
		
		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsIntelligenceVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsIntelligenceVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false
		
		CaVESWindow.UpdateLabel( wndName, startStatsIntelligenceVal, startStatsIntelligenceValText, diffStartCurVal, diffStartCurValText, 
														prevStatsIntelligenceVal, currentStatsIntelligenceVal, currentStatsIntelligenceValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsIntelligenceVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateToughnessLabel( wndName )
		local baseStatsToughnessVal = GameData.Player.Stats[GameData.Stats.TOUGHNESS].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curStatsToughnessVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_TOUGHNESS, baseStatsToughnessVal )
	
		-- Calculate effective stat values.
    baseStatsToughnessVal, curStatsToughnessVal = GetEffectiveStatValue( baseStatsToughnessVal, curStatsToughnessVal )
		
		if  prevStatsToughnessVal == nil and currentStatsToughnessVal == nil then
				currentStatsToughnessVal = curStatsToughnessVal
				prevStatsToughnessVal = currentStatsToughnessVal
		else 
				if bResetReferenceValues == false then
						prevStatsToughnessVal = currentStatsToughnessVal
				end
		end
		
		currentStatsToughnessVal = curStatsToughnessVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsToughnessVal = currentStatsToughnessVal
						startStatsToughnessValText = L""..startStatsToughnessVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].StatsToughnessVal == nil then
								startStatsToughnessVal = currentStatsToughnessVal
								startStatsToughnessValText = L""..startStatsToughnessVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startStatsToughnessVal = CaVESWindow.RefValues[serverName][characterName].StatsToughnessVal
								startStatsToughnessValText = L""..startStatsToughnessVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsToughnessVal = currentStatsToughnessVal
				startStatsToughnessValText = L""..startStatsToughnessVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsToughnessVal = currentStatsToughnessVal
				startStatsToughnessValText = L""..startStatsToughnessVal
		end
		
		local currentStatsToughnessValText = L""..currentStatsToughnessVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsToughnessVal - startStatsToughnessVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentStatsToughnessVal - prevStatsToughnessVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsToughnessVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsToughnessVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false
		
		CaVESWindow.UpdateLabel( wndName, startStatsToughnessVal, startStatsToughnessValText, diffStartCurVal, diffStartCurValText, 
														prevStatsToughnessVal, currentStatsToughnessVal, currentStatsToughnessValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsToughnessVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateFortitudeLabel( wndName )
		local baseStatsFortitudeVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
  	local curStatsFortitudeVal = GetBonus( GameData.BonusTypes.EBONUS_FORTITUDE, baseStatsFortitudeVal)
	
		-- Calculate effective stat values.
    baseStatsFortitudeVal, curStatsFortitudeVal = GetEffectiveStatValue( baseStatsFortitudeVal, curStatsFortitudeVal )
		
		if  prevStatsFortitudeVal == nil and currentStatsFortitudeVal == nil then
				currentStatsFortitudeVal = curStatsFortitudeVal
				prevStatsFortitudeVal = currentStatsFortitudeVal
		else 
				if bResetReferenceValues == false then
						prevStatsFortitudeVal = currentStatsFortitudeVal
				end
		end
		
		currentStatsFortitudeVal = curStatsFortitudeVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsFortitudeVal = currentStatsFortitudeVal
						startStatsFortitudeValText = L""..startStatsFortitudeVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].StatsFortitudeVal == nil then
								startStatsFortitudeVal = currentStatsFortitudeVal
								startStatsFortitudeValText = L""..startStatsFortitudeVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startStatsFortitudeVal = CaVESWindow.RefValues[serverName][characterName].StatsFortitudeVal
								startStatsFortitudeValText = L""..startStatsFortitudeVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsFortitudeVal = currentStatsFortitudeVal
				startStatsFortitudeValText = L""..startStatsFortitudeVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsFortitudeVal = currentStatsFortitudeVal
				startStatsFortitudeValText = L""..startStatsFortitudeVal
		end
		
		local currentStatsFortitudeValText = L""..currentStatsFortitudeVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsFortitudeVal - startStatsFortitudeVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentStatsFortitudeVal - prevStatsFortitudeVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsFortitudeVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsFortitudeVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false
		
		CaVESWindow.UpdateLabel( wndName, startStatsFortitudeVal, startStatsFortitudeValText, diffStartCurVal, diffStartCurValText, 
														prevStatsFortitudeVal, currentStatsFortitudeVal, currentStatsFortitudeValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsFortitudeVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateWeaponSkillLabel( wndName )
		local baseStatsWeaponSkillVal = GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curStatsWeaponSkillVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_WEAPONSKILL, baseStatsWeaponSkillVal ) 
	
		-- Calculate effective stat values.
    baseStatsWeaponSkillVal, curStatsWeaponSkillVal = GetEffectiveStatValue( baseStatsWeaponSkillVal, curStatsWeaponSkillVal )
		
		if  prevStatsWeaponSkillVal == nil and currentStatsWeaponSkillVal == nil then
				currentStatsWeaponSkillVal = curStatsWeaponSkillVal
				prevStatsWeaponSkillVal = currentStatsWeaponSkillVal
		else 
				if bResetReferenceValues == false then
						prevStatsWeaponSkillVal = currentStatsWeaponSkillVal
				end
		end
		
				currentStatsWeaponSkillVal = curStatsWeaponSkillVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsWeaponSkillVal = currentStatsWeaponSkillVal
						startStatsWeaponSkillValText = L""..startStatsWeaponSkillVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].StatsWeaponSkillVal == nil then
								startStatsWeaponSkillVal = currentStatsWeaponSkillVal
								startStatsWeaponSkillValText = L""..startStatsWeaponSkillVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startStatsWeaponSkillVal = CaVESWindow.RefValues[serverName][characterName].StatsWeaponSkillVal
								startStatsWeaponSkillValText = L""..startStatsWeaponSkillVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsWeaponSkillVal = currentStatsWeaponSkillVal
				startStatsWeaponSkillValText = L""..startStatsWeaponSkillVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsWeaponSkillVal = currentStatsWeaponSkillVal
				startStatsWeaponSkillValText = L""..startStatsWeaponSkillVal
		end
				
		local currentStatsWeaponSkillValText = L""..currentStatsWeaponSkillVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsWeaponSkillVal - startStatsWeaponSkillVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentStatsWeaponSkillVal - prevStatsWeaponSkillVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsWeaponSkillVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsWeaponSkillVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false
	
		CaVESWindow.UpdateLabel( wndName, startStatsWeaponSkillVal, startStatsWeaponSkillValText, diffStartCurVal, diffStartCurValText, 
														prevStatsWeaponSkillVal, currentStatsWeaponSkillVal, currentStatsWeaponSkillValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsWeaponSkillVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateInitiativeLabel( wndName )
		local baseStatsInitiativeVal = GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curStatsInitiativeVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_INITIATIVE, baseStatsInitiativeVal ) 
	
		-- Calculate effective stat values.
    baseStatsInitiativeVal, curStatsInitiativeVal = GetEffectiveStatValue( baseStatsInitiativeVal, curStatsInitiativeVal )
		
		if  prevStatsInitiativeVal == nil and currentStatsInitiativeVal == nil then
				currentStatsInitiativeVal = curStatsInitiativeVal
				prevStatsInitiativeVal = currentStatsInitiativeVal
		else 
				if bResetReferenceValues == false then
						prevStatsInitiativeVal = currentStatsInitiativeVal
				end
		end
		
		currentStatsInitiativeVal = curStatsInitiativeVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsInitiativeVal = currentStatsInitiativeVal
						startStatsInitiativeValText = L""..startStatsInitiativeVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].StatsInitiativeVal == nil then
								startStatsInitiativeVal = currentStatsInitiativeVal
								startStatsInitiativeValText = L""..startStatsInitiativeVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startStatsInitiativeVal = CaVESWindow.RefValues[serverName][characterName].StatsInitiativeVal
								startStatsInitiativeValText = L""..startStatsInitiativeVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsInitiativeVal = currentStatsInitiativeVal
				startStatsInitiativeValText = L""..startStatsInitiativeVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsInitiativeVal = currentStatsInitiativeVal
				startStatsInitiativeValText = L""..startStatsInitiativeVal
		end
		
		local currentStatsInitiativeValText = L""..currentStatsInitiativeVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsInitiativeVal - startStatsInitiativeVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentStatsInitiativeVal - prevStatsInitiativeVal)
		local diffPrevCurValText = L""..diffPrevCurVal
		
		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsInitiativeVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsInitiativeVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false
	
		CaVESWindow.UpdateLabel( wndName, startStatsInitiativeVal, startStatsInitiativeValText, diffStartCurVal, diffStartCurValText, 
														prevStatsInitiativeVal, currentStatsInitiativeVal, currentStatsInitiativeValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsInitiativeVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateWillpowerLabel( wndName )
		local baseStatsWillpowerVal = GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curStatsWillpowerVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_WILLPOWER, baseStatsWillpowerVal )
	
		-- Calculate effective stat values.
    baseStatsWillpowerVal, curStatsWillpowerVal = GetEffectiveStatValue( baseStatsWillpowerVal, curStatsWillpowerVal )
		
		if  prevStatsWillpowerVal == nil and currentStatsWillpowerVal == nil then
				currentStatsWillpowerVal = curStatsWillpowerVal
				prevStatsWillpowerVal = currentStatsWillpowerVal
		else 
				if bResetReferenceValues == false then
						prevStatsWillpowerVal = currentStatsWillpowerVal
				end
		end
		
		currentStatsWillpowerVal = curStatsWillpowerVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsWillpowerVal = currentStatsWillpowerVal
						startStatsWillpowerValText = L""..startStatsWillpowerVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].StatsWillpowerVal == nil then
								startStatsWillpowerVal = currentStatsWillpowerVal
								startStatsWillpowerValText = L""..startStatsWillpowerVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startStatsWillpowerVal = CaVESWindow.RefValues[serverName][characterName].StatsWillpowerVal
								startStatsWillpowerValText = L""..startStatsWillpowerVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsWillpowerVal = currentStatsWillpowerVal
				startStatsWillpowerValText = L""..startStatsWillpowerVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsWillpowerVal = currentStatsWillpowerVal
				startStatsWillpowerValText = L""..startStatsWillpowerVal
		end
		
		local currentStatsWillpowerValText = L""..currentStatsWillpowerVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsWillpowerVal - startStatsWillpowerVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentStatsWillpowerVal - prevStatsWillpowerVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsWillpowerVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsWillpowerVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startStatsWillpowerVal, startStatsWillpowerValText, diffStartCurVal, diffStartCurValText, 
														prevStatsWillpowerVal, currentStatsWillpowerVal, currentStatsWillpowerValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsWillpowerVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateWoundsLabel( wndName )
		local baseStatsWoundsVal = GameData.Player.Stats[GameData.Stats.WOUNDS].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curStatsWoundsVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_WOUNDS, baseStatsWoundsVal )
	
		-- Calculate effective stat values.
    baseStatsWoundsVal, curStatsWoundsVal = GetEffectiveStatValue( baseStatsWoundsVal, curStatsWoundsVal )
		
		if  prevStatsWoundsVal == nil and currentStatsWoundsVal == nil then
				currentStatsWoundsVal = curStatsWoundsVal
				prevStatsWoundsVal = currentStatsWoundsVal
		else 
				if bResetReferenceValues == false then
						prevStatsWoundsVal = currentStatsWoundsVal
				end
		end
		
		currentStatsWoundsVal = curStatsWoundsVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startStatsWoundsVal = currentStatsWoundsVal
						startStatsWoundsValText = L""..startStatsWoundsVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].StatsWoundsVal == nil then
								startStatsWoundsVal = currentStatsWoundsVal
								startStatsWoundsValText = L""..startStatsWoundsVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startStatsWoundsVal = currentStatsWoundsVal
								startStatsWoundsValText = L""..startStatsWoundsVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startStatsWoundsVal = currentStatsWoundsVal
				startStatsWoundsValText = L""..startStatsWoundsVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startStatsWoundsVal = currentStatsWoundsVal
				startStatsWoundsValText = L""..startStatsWoundsVal
		end
		
		local currentStatsWoundsValText = L""..currentStatsWoundsVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentStatsWoundsVal - startStatsWoundsVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentStatsWoundsVal - prevStatsWoundsVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Check if stat is diminished and pass this to the updatelabel function.
		local bIsCurStatDiminished = CharacterWindow.IsStatDiminished ( currentStatsWoundsVal )
		local bIsRefStatDiminished = CharacterWindow.IsStatDiminished ( startStatsWoundsVal )			

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startStatsWoundsVal, startStatsWoundsValText, diffStartCurVal, diffStartCurValText, 
														prevStatsWoundsVal, currentStatsWoundsVal, currentStatsWoundsValText, diffPrevCurVal, 
														diffPrevCurValText, baseStatsWoundsVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end


---- Defense Update Functions ----

function CaVESWindow.UpdateArmorLabel( wndName )
		local baseDefenseArmorVal = 0
		
		-- Special handling for armor bonuses from Talismans in equipment enhancement slots.
		if bIgnoreTalismans == true then
			baseDefenseArmorVal = (GameData.Player.armorValue) - CaVESWindow.CalculateBonusValueFromTalismans(GameData.BonusTypes.EBONUS_ARMOR)
		else
			baseDefenseArmorVal = (GameData.Player.armorValue)
		end

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curDefenseArmorVal = baseDefenseArmorVal
    
		if  prevDefenseArmorVal == nil and currentDefenseArmorVal == nil then
				currentDefenseArmorVal = curDefenseArmorVal
				prevDefenseArmorVal = currentDefenseArmorVal
		else 
				if bResetReferenceValues == false then
						prevDefenseArmorVal = currentDefenseArmorVal
				end
		end
		
		currentDefenseArmorVal = curDefenseArmorVal
		
		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startDefenseArmorVal = currentDefenseArmorVal
						startDefenseArmorValText = L""..startDefenseArmorVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].DefenseArmorVal == nil then
								startDefenseArmorVal = currentDefenseArmorVal
								startDefenseArmorValText = L""..startDefenseArmorVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startDefenseArmorVal = CaVESWindow.RefValues[serverName][characterName].DefenseArmorVal
								startDefenseArmorValText = L""..startDefenseArmorVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startDefenseArmorVal = currentDefenseArmorVal
				startDefenseArmorValText = L""..startDefenseArmorVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startDefenseArmorVal = currentDefenseArmorVal
				startDefenseArmorValText = L""..startDefenseArmorVal
		end
		
		local currentDefenseArmorValText = L""..currentDefenseArmorVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentDefenseArmorVal - startDefenseArmorVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentDefenseArmorVal - prevDefenseArmorVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false		

		-- Set flip color code boolean
		local bFlipColorCodes = false
		
		-- NOTE: Set startDefenseArmor value to baseDefenseArmorVal	so the display values are not color coded either way.
		CaVESWindow.UpdateLabel( wndName, baseDefenseArmorVal, startDefenseArmorValText, diffStartCurVal, diffStartCurValText, 
														prevDefenseArmorVal, currentDefenseArmorVal, currentDefenseArmorValText, diffPrevCurVal, 
														diffPrevCurValText, baseDefenseArmorVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateSpiritualResistanceLabel( wndName )
		local baseDefenseSpiritualResistanceVal = GameData.Player.Stats[GameData.Stats.SPIRITRESIST].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curDefenseSpiritualResistanceVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_SPIRIT_RESIST, baseDefenseSpiritualResistanceVal )
	
		if  prevDefenseSpiritualResistanceVal == nil and currentDefenseSpiritualResistanceVal == nil then
				currentDefenseSpiritualResistanceVal = curDefenseSpiritualResistanceVal
				prevDefenseSpiritualResistanceVal = currentDefenseSpiritualResistanceVal
		else 
				if bResetReferenceValues == false then
						prevDefenseSpiritualResistanceVal = currentDefenseSpiritualResistanceVal
				end
		end
		
		currentDefenseSpiritualResistanceVal = curDefenseSpiritualResistanceVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startDefenseSpiritualResistanceVal = currentDefenseSpiritualResistanceVal
						startDefenseSpiritualResistanceValText = wstring.format(L"%.0f", startDefenseSpiritualResistanceVal) 
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].DefenseSpiritualResistanceVal == nil then
								startDefenseSpiritualResistanceVal = currentDefenseSpiritualResistanceVal
								startDefenseSpiritualResistanceValText = wstring.format(L"%.0f", startDefenseSpiritualResistanceVal)
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startDefenseSpiritualResistanceVal = CaVESWindow.RefValues[serverName][characterName].DefenseSpiritualResistanceVal
								startDefenseSpiritualResistanceValText = wstring.format(L"%.0f", startDefenseSpiritualResistanceVal) 
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startDefenseSpiritualResistanceVal = currentDefenseSpiritualResistanceVal
				startDefenseSpiritualResistanceValText = wstring.format(L"%.0f", startDefenseSpiritualResistanceVal) 
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startDefenseSpiritualResistanceVal = currentDefenseSpiritualResistanceVal
				startDefenseSpiritualResistanceValText = wstring.format(L"%.0f", startDefenseSpiritualResistanceVal) 
		end
		
		local currentDefenseSpiritualResistanceValText = wstring.format(L"%.0f", currentDefenseSpiritualResistanceVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentDefenseSpiritualResistanceVal - startDefenseSpiritualResistanceVal)
				diffStartCurValText = wstring.format(L"%.0f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentDefenseSpiritualResistanceVal - prevDefenseSpiritualResistanceVal)
		local diffPrevCurValText = wstring.format(L"%.0f", diffPrevCurVal)
		
		-- Check if resistance is diminished and pass this to the updatelabel function.
		local bIsCurResistDiminished = CharacterWindow.IsResistDiminished( currentDefenseSpiritualResistanceVal )
		local bIsRefResistDiminished = CharacterWindow.IsResistDiminished( startDefenseSpiritualResistanceVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false
		
		CaVESWindow.UpdateLabel( wndName, startDefenseSpiritualResistanceVal, startDefenseSpiritualResistanceValText, diffStartCurVal, diffStartCurValText, 
														prevDefenseSpiritualResistanceVal, currentDefenseSpiritualResistanceVal, currentDefenseSpiritualResistanceValText, diffPrevCurVal, 
														diffPrevCurValText, baseDefenseSpiritualResistanceVal, bIsCurResistDiminished, bIsRefResistDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateCorporealResistanceLabel( wndName )
		local baseDefenseCorporealResistanceVal = GameData.Player.Stats[GameData.Stats.CORPOREALRESIST].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curDefenseCorporealResistanceVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_CORPOREAL_RESIST, baseDefenseCorporealResistanceVal )	

		if  prevDefenseCorporealResistanceVal == nil and currentDefenseCorporealResistanceVal == nil then
				currentDefenseCorporealResistanceVal = curDefenseCorporealResistanceVal
				prevDefenseCorporealResistanceVal = currentDefenseCorporealResistanceVal
		else 
				if bResetReferenceValues == false then
						prevDefenseCorporealResistanceVal = currentDefenseCorporealResistanceVal
				end
		end
		
		currentDefenseCorporealResistanceVal = curDefenseCorporealResistanceVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startDefenseCorporealResistanceVal = currentDefenseCorporealResistanceVal
						startDefenseCorporealResistanceValText = wstring.format(L"%.0f", startDefenseCorporealResistanceVal)
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].DefenseCorporealResistanceVal == nil then
								startDefenseCorporealResistanceVal = currentDefenseCorporealResistanceVal
								startDefenseCorporealResistanceValText = wstring.format(L"%.0f", startDefenseCorporealResistanceVal)
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startDefenseCorporealResistanceVal = CaVESWindow.RefValues[serverName][characterName].DefenseCorporealResistanceVal
								startDefenseCorporealResistanceValText = wstring.format(L"%.0f", startDefenseCorporealResistanceVal)
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startDefenseCorporealResistanceVal = currentDefenseCorporealResistanceVal
				startDefenseCorporealResistanceValText = wstring.format(L"%.0f", startDefenseCorporealResistanceVal)
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startDefenseCorporealResistanceVal = currentDefenseCorporealResistanceVal
				startDefenseCorporealResistanceValText = wstring.format(L"%.0f", startDefenseCorporealResistanceVal)
		end

		local currentDefenseCorporealResistanceValText = wstring.format(L"%.0f", currentDefenseCorporealResistanceVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentDefenseCorporealResistanceVal - startDefenseCorporealResistanceVal)
				diffStartCurValText = wstring.format(L"%.0f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentDefenseCorporealResistanceVal - prevDefenseCorporealResistanceVal)
		local diffPrevCurValText = wstring.format(L"%.0f", diffPrevCurVal)

		-- Check if resistance is diminished and pass this to the updatelabel function.
		local bIsCurResistDiminished = CharacterWindow.IsResistDiminished( currentDefenseCorporealResistanceVal )
		local bIsRefResistDiminished = CharacterWindow.IsResistDiminished( startDefenseCorporealResistanceVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startDefenseCorporealResistanceVal, startDefenseCorporealResistanceValText, diffStartCurVal, diffStartCurValText, 
														prevDefenseCorporealResistanceVal, currentDefenseCorporealResistanceVal, currentDefenseCorporealResistanceValText, diffPrevCurVal, 
														diffPrevCurValText, baseDefenseCorporealResistanceVal, bIsCurResistDiminished, bIsRefResistDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateElementalResistanceLabel( wndName )
		local baseDefenseElementalResistanceVal = GameData.Player.Stats[GameData.Stats.ELEMENTALRESIST].baseValue

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curDefenseElementalResistanceVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_ELEMENTAL_RESIST, baseDefenseElementalResistanceVal )

		if  prevDefenseElementalResistanceVal == nil and currentDefenseElementalResistanceVal == nil then
				currentDefenseElementalResistanceVal = curDefenseElementalResistanceVal
				prevDefenseElementalResistanceVal = currentDefenseElementalResistanceVal
		else 
				if bResetReferenceValues == false then
						prevDefenseElementalResistanceVal = currentDefenseElementalResistanceVal
				end
		end

		currentDefenseElementalResistanceVal = curDefenseElementalResistanceVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startDefenseElementalResistanceVal = currentDefenseElementalResistanceVal
						startDefenseElementalResistanceValText = wstring.format(L"%.0f", startDefenseElementalResistanceVal)
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].DefenseElementalResistanceVal == nil then
								startDefenseElementalResistanceVal = currentDefenseElementalResistanceVal
								startDefenseElementalResistanceValText = wstring.format(L"%.0f", startDefenseElementalResistanceVal)
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startDefenseElementalResistanceVal = CaVESWindow.RefValues[serverName][characterName].DefenseElementalResistanceVal
								startDefenseElementalResistanceValText = wstring.format(L"%.0f", startDefenseElementalResistanceVal)
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startDefenseElementalResistanceVal = currentDefenseElementalResistanceVal
				startDefenseElementalResistanceValText = wstring.format(L"%.0f", startDefenseElementalResistanceVal)
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startDefenseElementalResistanceVal = currentDefenseElementalResistanceVal
				startDefenseElementalResistanceValText = wstring.format(L"%.0f", startDefenseElementalResistanceVal)
		end

		local currentDefenseElementalResistanceValText = wstring.format(L"%.0f", currentDefenseElementalResistanceVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentDefenseElementalResistanceVal - startDefenseElementalResistanceVal)
				diffStartCurValText = wstring.format(L"%.0f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentDefenseElementalResistanceVal - prevDefenseElementalResistanceVal)
		local diffPrevCurValText = wstring.format(L"%.0f", diffPrevCurVal)

		-- Check if resistance is diminished and pass this to the updatelabel function.
		local bIsCurResistDiminished = CharacterWindow.IsResistDiminished( currentDefenseElementalResistanceVal )
		local bIsRefResistDiminished = CharacterWindow.IsResistDiminished( startDefenseElementalResistanceVal )

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startDefenseElementalResistanceVal, startDefenseElementalResistanceValText, diffStartCurVal, diffStartCurValText, 
														prevDefenseElementalResistanceVal, currentDefenseElementalResistanceVal, currentDefenseElementalResistanceValText, diffPrevCurVal, 
														diffPrevCurValText, baseDefenseElementalResistanceVal, bIsCurResistDiminished, bIsRefResistDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateBlockLabel( wndName )
		local baseDefenseBlockVal = GameData.Player.Stats[GameData.Stats.BLOCKSKILL].baseValue / 100

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curDefenseBlockVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_BLOCK, baseDefenseBlockVal ) 

    if( curDefenseBlockVal > 100 ) then curDefenseBlockVal = 100 end

		if  prevDefenseBlockVal == nil and currentDefenseBlockVal == nil then
				currentDefenseBlockVal = curDefenseBlockVal
				prevDefenseBlockVal = currentDefenseBlockVal
		else 
				if bResetReferenceValues == false then
						prevDefenseBlockVal = currentDefenseBlockVal
				end
		end

		currentDefenseBlockVal = curDefenseBlockVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startDefenseBlockVal = currentDefenseBlockVal
						startDefenseBlockValText = wstring.format(L"%.01f", startDefenseBlockVal)..L"%"           
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].DefenseBlockVal == nil then
								startDefenseBlockVal = currentDefenseBlockVal
								startDefenseBlockValText = wstring.format(L"%.01f", startDefenseBlockVal)..L"%"           
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startDefenseBlockVal = CaVESWindow.RefValues[serverName][characterName].DefenseBlockVal
								startDefenseBlockValText = wstring.format(L"%.01f", startDefenseBlockVal)..L"%"
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startDefenseBlockVal = currentDefenseBlockVal
				startDefenseBlockValText = wstring.format(L"%.01f", startDefenseBlockVal)..L"%"           
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startDefenseBlockVal = currentDefenseBlockVal
				startDefenseBlockValText = wstring.format(L"%.01f", startDefenseBlockVal)..L"%"           
		end

		local currentDefenseBlockValText = wstring.format(L"%.01f", currentDefenseBlockVal)..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentDefenseBlockVal - startDefenseBlockVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)..L"%"
		end

		local diffPrevCurVal = (currentDefenseBlockVal - prevDefenseBlockVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)..L"%"

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startDefenseBlockVal, startDefenseBlockValText, diffStartCurVal, diffStartCurValText, 
														prevDefenseBlockVal, currentDefenseBlockVal, currentDefenseBlockValText, diffPrevCurVal, 
														diffPrevCurValText, baseDefenseBlockVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateParryLabel( wndName )
		local baseDefenseParryVal = GameData.Player.Stats[GameData.Stats.PARRYSKILL].baseValue / 100

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curDefenseParryVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_PARRY, baseDefenseParryVal ) 

    if( curDefenseParryVal > 100 ) then curDefenseParryVal = 100 end

		if  prevDefenseParryVal == nil and currentDefenseParryVal == nil then
				currentDefenseParryVal = curDefenseParryVal
				prevDefenseParryVal = currentDefenseParryVal
		else 
				if bResetReferenceValues == false then
						prevDefenseParryVal = currentDefenseParryVal
				end
		end

		currentDefenseParryVal = curDefenseParryVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startDefenseParryVal = currentDefenseParryVal
						startDefenseParryValText = wstring.format(L"%.01f", startDefenseParryVal)..L"%"           
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].DefenseParryVal == nil then
								startDefenseParryVal = currentDefenseParryVal
								startDefenseParryValText = wstring.format(L"%.01f", startDefenseParryVal)..L"%"           
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startDefenseParryVal = CaVESWindow.RefValues[serverName][characterName].DefenseParryVal
								startDefenseParryValText = wstring.format(L"%.01f", startDefenseParryVal)..L"%" 
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startDefenseParryVal = currentDefenseParryVal
				startDefenseParryValText = wstring.format(L"%.01f", startDefenseParryVal)..L"%"           
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startDefenseParryVal = currentDefenseParryVal
				startDefenseParryValText = wstring.format(L"%.01f", startDefenseParryVal)..L"%"           
		end

		local currentDefenseParryValText = wstring.format(L"%.01f", currentDefenseParryVal)..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentDefenseParryVal - startDefenseParryVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)..L"%"
		end

		local diffPrevCurVal = (currentDefenseParryVal - prevDefenseParryVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)..L"%"

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startDefenseParryVal, startDefenseParryValText, diffStartCurVal, diffStartCurValText, 
														prevDefenseParryVal, currentDefenseParryVal, currentDefenseParryValText, diffPrevCurVal, 
														diffPrevCurValText, baseDefenseParryVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateDodgeLabel( wndName )
		local baseDefenseDodgeVal = GameData.Player.Stats[GameData.Stats.EVADESKILL].baseValue / 100

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curDefenseDodgeVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_EVADE, baseDefenseDodgeVal )

    if( curDefenseDodgeVal > 100 ) then curDefenseDodgeVal = 100 end

		if  prevDefenseDodgeVal == nil and currentDefenseDodgeVal == nil then
				currentDefenseDodgeVal = curDefenseDodgeVal
				prevDefenseDodgeVal = currentDefenseDodgeVal
		else 
				if bResetReferenceValues == false then
						prevDefenseDodgeVal = currentDefenseDodgeVal
				end
		end

		currentDefenseDodgeVal = curDefenseDodgeVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startDefenseDodgeVal = currentDefenseDodgeVal
						startDefenseDodgeValText = wstring.format(L"%.01f", startDefenseDodgeVal)..L"%"           
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].DefenseDodgeVal == nil then
								startDefenseDodgeVal = currentDefenseDodgeVal
								startDefenseDodgeValText = wstring.format(L"%.01f", startDefenseDodgeVal)..L"%"           
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startDefenseDodgeVal = CaVESWindow.RefValues[serverName][characterName].DefenseDodgeVal
								startDefenseDodgeValText = wstring.format(L"%.01f", startDefenseDodgeVal)..L"%"
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startDefenseDodgeVal = currentDefenseDodgeVal
				startDefenseDodgeValText = wstring.format(L"%.01f", startDefenseDodgeVal)..L"%"           
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startDefenseDodgeVal = currentDefenseDodgeVal
				startDefenseDodgeValText = wstring.format(L"%.01f", startDefenseDodgeVal)..L"%"           
		end
		
		local currentDefenseDodgeValText = wstring.format(L"%.01f", currentDefenseDodgeVal)..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentDefenseDodgeVal - startDefenseDodgeVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)..L"%"
		end

		local diffPrevCurVal = (currentDefenseDodgeVal - prevDefenseDodgeVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)..L"%"

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startDefenseDodgeVal, startDefenseDodgeValText, diffStartCurVal, diffStartCurValText, 
														prevDefenseDodgeVal, currentDefenseDodgeVal, currentDefenseDodgeValText, diffPrevCurVal, 
														diffPrevCurValText, baseDefenseDodgeVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateDisruptLabel( wndName )
		local baseDefenseDisruptVal = GameData.Player.Stats[GameData.Stats.DISRUPTSKILL].baseValue / 100

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curDefenseDisruptVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_DISRUPT, baseDefenseDisruptVal )

    if( curDefenseDisruptVal > 100 ) then curDefenseDisruptVal = 100 end

		if  prevDefenseDisruptVal == nil and currentDefenseDisruptVal == nil then
				currentDefenseDisruptVal = curDefenseDisruptVal
				prevDefenseDisruptVal = currentDefenseDisruptVal
		else 
				if bResetReferenceValues == false then
						prevDefenseDisruptVal = currentDefenseDisruptVal
				end
		end

		currentDefenseDisruptVal = curDefenseDisruptVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startDefenseDisruptVal = currentDefenseDisruptVal
						startDefenseDisruptValText = wstring.format(L"%.01f", startDefenseDisruptVal)..L"%"           
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].DefenseDisruptVal == nil then
								startDefenseDisruptVal = currentDefenseDisruptVal
								startDefenseDisruptValText = wstring.format(L"%.01f", startDefenseDisruptVal)..L"%"           
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startDefenseDisruptVal = CaVESWindow.RefValues[serverName][characterName].DefenseDisruptVal
								startDefenseDisruptValText = wstring.format(L"%.01f", startDefenseDisruptVal)..L"%" 
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startDefenseDisruptVal = currentDefenseDisruptVal
				startDefenseDisruptValText = wstring.format(L"%.01f", startDefenseDisruptVal)..L"%"           
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startDefenseDisruptVal = currentDefenseDisruptVal
				startDefenseDisruptValText = wstring.format(L"%.01f", startDefenseDisruptVal)..L"%"          
		end
		
		local currentDefenseDisruptValText = wstring.format(L"%.01f", currentDefenseDisruptVal)..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentDefenseDisruptVal - startDefenseDisruptVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)..L"%"
		end

		local diffPrevCurVal = (currentDefenseDisruptVal - prevDefenseDisruptVal)
		local diffPrevCurValText =  wstring.format(L"%.01f", diffPrevCurVal)..L"%"

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startDefenseDisruptVal, startDefenseDisruptValText, diffStartCurVal, diffStartCurValText, 
														prevDefenseDisruptVal, currentDefenseDisruptVal, currentDefenseDisruptValText, diffPrevCurVal, 
														diffPrevCurValText, baseDefenseDisruptVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end


---- Melee Update Functions ----

function CaVESWindow.UpdateMeleeWeaponDPSRHLabel( wndName )
		local baseMeleeWeaponDPSRHVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMeleeWeaponDPSRHVal = tonumber(wstring.format(L"%.01f",CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps))

		if prevMeleeWeaponDPSRHVal == nil and currentMeleeWeaponDPSRHVal == nil then
				if (curMeleeWeaponDPSRHVal > 0) then
						currentMeleeWeaponDPSRHVal = curMeleeWeaponDPSRHVal
				else
						currentMeleeWeaponDPSRHVal = 0
				end
				prevMeleeWeaponDPSRHVal = currentMeleeWeaponDPSRHVal
		else 
				if bResetReferenceValues == false then
						prevMeleeWeaponDPSRHVal = currentMeleeWeaponDPSRHVal
				end
		end

		currentMeleeWeaponDPSRHVal = curMeleeWeaponDPSRHVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
			  		if (currentMeleeWeaponDPSRHVal > 0) then                                                     
			      		startMeleeWeaponDPSRHVal = currentMeleeWeaponDPSRHVal
			    			startMeleeWeaponDPSRHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSRHVal)                                         
			    	else                                                                                                                               
			      		startMeleeWeaponDPSRHVal = 0                                                                                                   
			      		startMeleeWeaponDPSRHValText = L"0.0"                                                                                          
			      end                                                                                                                                
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MeleeWeaponDPSRHVal == nil then
					  		if (currentMeleeWeaponDPSRHVal > 0) then                                                     
					      		startMeleeWeaponDPSRHVal = currentMeleeWeaponDPSRHVal
					    			startMeleeWeaponDPSRHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSRHVal)                                         
					    	else                                                                                                                               
					      		startMeleeWeaponDPSRHVal = 0                                                                                                   
					      		startMeleeWeaponDPSRHValText = L"0.0"                                                                                          
					      end                                                                                                                                
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMeleeWeaponDPSRHVal = CaVESWindow.RefValues[serverName][characterName].MeleeWeaponDPSRHVal  
								startMeleeWeaponDPSRHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSRHVal)    
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				if (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps > 0) then                                                     
						startMeleeWeaponDPSRHVal = currentMeleeWeaponDPSRHVal
						startMeleeWeaponDPSRHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSRHVal)                                         
				else                                                                                                                               
						startMeleeWeaponDPSRHVal = 0                                                                                              
						startMeleeWeaponDPSRHValText = L"0.0"                                                                                          
				end                                                                                                                                
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
	  		if (currentMeleeWeaponDPSRHVal > 0) then                                                     
	      		startMeleeWeaponDPSRHVal = currentMeleeWeaponDPSRHVal
	    			startMeleeWeaponDPSRHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSRHVal)                                         
	    	else                                                                                                                               
	      		startMeleeWeaponDPSRHVal = 0                                                                                                   
	      		startMeleeWeaponDPSRHValText = L"0.0"                                                                                          
	      end                                                                                                                                
		end

		local currentMeleeWeaponDPSRHValText = L""..wstring.format(L"%.01f", currentMeleeWeaponDPSRHVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentMeleeWeaponDPSRHVal - startMeleeWeaponDPSRHVal)
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentMeleeWeaponDPSRHVal - prevMeleeWeaponDPSRHVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMeleeWeaponDPSRHVal, startMeleeWeaponDPSRHValText, diffStartCurVal, diffStartCurValText, 
														prevMeleeWeaponDPSRHVal, currentMeleeWeaponDPSRHVal, currentMeleeWeaponDPSRHValText, diffPrevCurVal, 
														diffPrevCurValText, baseMeleeWeaponDPSRHVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )

end

function CaVESWindow.UpdateMeleeSpeedRHLabel( wndName )
		local baseMeleeSpeedRHVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local weaponSpeedRH = CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].speed
		local curMeleeSpeedRHVal = tonumber(wstring.format(L"%.01f",CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_AUTO_ATTACK_SPEED, weaponSpeedRH )))

		if  prevMeleeSpeedRHVal == nil and currentMeleeSpeedRHVal == nil then
				currentMeleeSpeedRHVal = curMeleeSpeedRHVal
				prevMeleeSpeedRHVal = currentMeleeSpeedRHVal
		else 
				if bResetReferenceValues == false then
						prevMeleeSpeedRHVal = currentMeleeSpeedRHVal
				end
		end

		currentMeleeSpeedRHVal = curMeleeSpeedRHVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMeleeSpeedRHVal = currentMeleeSpeedRHVal
						startMeleeSpeedRHValText = L""..wstring.format(L"%.01f", startMeleeSpeedRHVal)                                              
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MeleeSpeedRHVal == nil then
								startMeleeSpeedRHVal = currentMeleeSpeedRHVal
								startMeleeSpeedRHValText = L""..wstring.format(L"%.01f", startMeleeSpeedRHVal)                                              
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMeleeSpeedRHVal = CaVESWindow.RefValues[serverName][characterName].MeleeSpeedRHVal  
								startMeleeSpeedRHValText = L""..wstring.format(L"%.01f", startMeleeSpeedRHVal)    
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMeleeSpeedRHVal = currentMeleeSpeedRHVal
				startMeleeSpeedRHValText = L""..wstring.format(L"%.01f", startMeleeSpeedRHVal)                                              
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMeleeSpeedRHVal = currentMeleeSpeedRHVal
				startMeleeSpeedRHValText = L""..wstring.format(L"%.01f", startMeleeSpeedRHVal)                                              
		end

		local currentMeleeSpeedRHValText = L""..wstring.format(L"%.01f", currentMeleeSpeedRHVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentMeleeSpeedRHVal - startMeleeSpeedRHVal)
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentMeleeSpeedRHVal - prevMeleeSpeedRHVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = true

		CaVESWindow.UpdateLabel( wndName, startMeleeSpeedRHVal, startMeleeSpeedRHValText, diffStartCurVal, diffStartCurValText, 
														prevMeleeSpeedRHVal, currentMeleeSpeedRHVal, currentMeleeSpeedRHValText, diffPrevCurVal, 
														diffPrevCurValText, baseMeleeSpeedRHVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateMeleeWeaponDPSLHLabel( wndName )
		local baseMeleeWeaponDPSLHVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMeleeWeaponDPSLHVal = tonumber(wstring.format(L"%.01f",CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps))

		if prevMeleeWeaponDPSLHVal == nil and currentMeleeWeaponDPSLHVal == nil then
				if (curMeleeWeaponDPSLHVal > 0) then
						currentMeleeWeaponDPSLHVal = curMeleeWeaponDPSLHVal
				else
						currentMeleeWeaponDPSLHVal = 0
				end
				prevMeleeWeaponDPSLHVal = currentMeleeWeaponDPSLHVal
		else 
				if bResetReferenceValues == false then
						prevMeleeWeaponDPSLHVal = currentMeleeWeaponDPSLHVal
				end
		end

		currentMeleeWeaponDPSLHVal = curMeleeWeaponDPSLHVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
			  		if (currentMeleeWeaponDPSLHVal > 0) then                                                     
			      		startMeleeWeaponDPSLHVal = currentMeleeWeaponDPSLHVal
			    			startMeleeWeaponDPSLHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSLHVal)                                         
			    	else                                                                                                                               
			      		startMeleeWeaponDPSLHVal = 0                                                                                                   
			      		startMeleeWeaponDPSLHValText = L"0.0"                                                                                          
			      end                                                                                                                                
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MeleeWeaponDPSLHVal == nil then
					  		if (currentMeleeWeaponDPSLHVal > 0) then                                                     
					      		startMeleeWeaponDPSLHVal = currentMeleeWeaponDPSLHVal
					    			startMeleeWeaponDPSLHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSLHVal)                                         
					    	else                                                                                                                               
					      		startMeleeWeaponDPSLHVal = 0                                                                                                   
					      		startMeleeWeaponDPSLHValText = L"0.0"                                                                                          
					      end                                                                                                                                
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMeleeWeaponDPSLHVal = CaVESWindow.RefValues[serverName][characterName].MeleeWeaponDPSLHVal  
								startMeleeWeaponDPSLHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSLHVal)    
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				if (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps > 0) then                                                     
						startMeleeWeaponDPSLHVal = currentMeleeWeaponDPSLHVal
						startMeleeWeaponDPSLHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSLHVal)                                         
				else                                                                                                                               
						startMeleeWeaponDPSLHVal = 0                                                                                              
						startMeleeWeaponDPSLHValText = L"0.0"                                                                                          
				end                                                                                                                                
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
	  		if (currentMeleeWeaponDPSLHVal > 0) then                                                     
	      		startMeleeWeaponDPSLHVal = currentMeleeWeaponDPSLHVal
	    			startMeleeWeaponDPSLHValText = L""..wstring.format(L"%.01f", startMeleeWeaponDPSLHVal)                                         
	    	else                                                                                                                               
	      		startMeleeWeaponDPSLHVal = 0                                                                                                   
	      		startMeleeWeaponDPSLHValText = L"0.0"                                                                                          
	      end                                                                                                                                
		end

		local currentMeleeWeaponDPSLHValText = L""..wstring.format(L"%.01f", currentMeleeWeaponDPSLHVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentMeleeWeaponDPSLHVal - startMeleeWeaponDPSLHVal)
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentMeleeWeaponDPSLHVal - prevMeleeWeaponDPSLHVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMeleeWeaponDPSLHVal, startMeleeWeaponDPSLHValText, diffStartCurVal, diffStartCurValText, 
														prevMeleeWeaponDPSLHVal, currentMeleeWeaponDPSLHVal, currentMeleeWeaponDPSLHValText, diffPrevCurVal, 
														diffPrevCurValText, baseMeleeWeaponDPSLHVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )

end

function CaVESWindow.UpdateMeleeSpeedLHLabel( wndName )
		local baseMeleeSpeedLHVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local weaponSpeedLH = CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].speed
		local curMeleeSpeedLHVal = tonumber(wstring.format(L"%.01f",CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_AUTO_ATTACK_SPEED, weaponSpeedLH )))

		if  prevMeleeSpeedLHVal == nil and currentMeleeSpeedLHVal == nil then
				currentMeleeSpeedLHVal = curMeleeSpeedLHVal
				prevMeleeSpeedLHVal = currentMeleeSpeedLHVal
		else 
				if bResetReferenceValues == false then
						prevMeleeSpeedLHVal = currentMeleeSpeedLHVal
				end
		end

		currentMeleeSpeedLHVal = curMeleeSpeedLHVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMeleeSpeedLHVal = currentMeleeSpeedLHVal
						startMeleeSpeedLHValText = L""..wstring.format(L"%.01f", startMeleeSpeedLHVal)                                              
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MeleeSpeedLHVal == nil then
								startMeleeSpeedLHVal = currentMeleeSpeedLHVal
								startMeleeSpeedLHValText = L""..wstring.format(L"%.01f", startMeleeSpeedLHVal)                                              
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMeleeSpeedLHVal = CaVESWindow.RefValues[serverName][characterName].MeleeSpeedLHVal  
								startMeleeSpeedLHValText = L""..wstring.format(L"%.01f", startMeleeSpeedLHVal)    
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMeleeSpeedLHVal = currentMeleeSpeedLHVal
				startMeleeSpeedLHValText = L""..wstring.format(L"%.01f", startMeleeSpeedLHVal)                                              
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMeleeSpeedLHVal = currentMeleeSpeedLHVal
				startMeleeSpeedLHValText = L""..wstring.format(L"%.01f", startMeleeSpeedLHVal)                                              
		end

		local currentMeleeSpeedLHValText = L""..wstring.format(L"%.01f", currentMeleeSpeedLHVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentMeleeSpeedLHVal - startMeleeSpeedLHVal)
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentMeleeSpeedLHVal - prevMeleeSpeedLHVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = true

		CaVESWindow.UpdateLabel( wndName, startMeleeSpeedLHVal, startMeleeSpeedLHValText, diffStartCurVal, diffStartCurValText, 
														prevMeleeSpeedLHVal, currentMeleeSpeedLHVal, currentMeleeSpeedLHValText, diffPrevCurVal, 
														diffPrevCurValText, baseMeleeSpeedLHVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateMeleePowerBonusLabel( wndName )
		local baseMeleePowerBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMeleePowerBonusVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_DAMAGE_MELEE, baseMeleePowerBonusVal)
		
		if  prevMeleePowerBonusVal == nil and currentMeleePowerBonusVal == nil then
				currentMeleePowerBonusVal = curMeleePowerBonusVal
				prevMeleePowerBonusVal = currentMeleePowerBonusVal
		else 
				if bResetReferenceValues == false then
						prevMeleePowerBonusVal = currentMeleePowerBonusVal
				end
		end

		currentMeleePowerBonusVal = curMeleePowerBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMeleePowerBonusVal = currentMeleePowerBonusVal
						startMeleePowerBonusValText = L""..startMeleePowerBonusVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MeleePowerBonusVal == nil then
								startMeleePowerBonusVal = currentMeleePowerBonusVal
								startMeleePowerBonusValText = L""..startMeleePowerBonusVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMeleePowerBonusVal = CaVESWindow.RefValues[serverName][characterName].MeleePowerBonusVal  
								startMeleePowerBonusValText = L""..startMeleePowerBonusVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMeleePowerBonusVal = currentMeleePowerBonusVal
				startMeleePowerBonusValText = L""..startMeleePowerBonusVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMeleePowerBonusVal = currentMeleePowerBonusVal
				startMeleePowerBonusValText = L""..startMeleePowerBonusVal                                         
		end

		local currentMeleePowerBonusValText = L""..currentMeleePowerBonusVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentMeleePowerBonusVal - startMeleePowerBonusVal)
				diffStartCurValText = diffStartCurVal
		end

		local diffPrevCurVal = (currentMeleePowerBonusVal - prevMeleePowerBonusVal)
		local diffPrevCurValText = diffPrevCurVal

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMeleePowerBonusVal, startMeleePowerBonusValText, diffStartCurVal, diffStartCurValText, 
														prevMeleePowerBonusVal, currentMeleePowerBonusVal, currentMeleePowerBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseMeleePowerBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateMeleeDamageBonusLabel( wndName )
		local baseMeleeDamageBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMeleeDamageBonusVal = tonumber(CaVESWindow.GetTotalBonusPower( currentStatsStrengthVal, GameData.BonusTypes.EBONUS_DAMAGE_MELEE ))

		if  prevMeleeDamageBonusVal == nil and currentMeleeDamageBonusVal == nil then
				currentMeleeDamageBonusVal = curMeleeDamageBonusVal
				prevMeleeDamageBonusVal = currentMeleeDamageBonusVal
		else 
				if bResetReferenceValues == false then
						prevMeleeDamageBonusVal = currentMeleeDamageBonusVal
				end
		end

		currentMeleeDamageBonusVal = curMeleeDamageBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMeleeDamageBonusVal = currentMeleeDamageBonusVal
						startMeleeDamageBonusValText = L""..wstring.format(L"%.01f", startMeleeDamageBonusVal)
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MeleeDamageBonusVal == nil then
								startMeleeDamageBonusVal = currentMeleeDamageBonusVal
								startMeleeDamageBonusValText = L""..wstring.format(L"%.01f", startMeleeDamageBonusVal)
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMeleeDamageBonusVal = CaVESWindow.RefValues[serverName][characterName].MeleeDamageBonusVal  
								startMeleeDamageBonusValText = L""..wstring.format(L"%.01f", startMeleeDamageBonusVal)
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMeleeDamageBonusVal = currentMeleeDamageBonusVal
				startMeleeDamageBonusValText = L""..wstring.format(L"%.01f", startMeleeDamageBonusVal)
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMeleeDamageBonusVal = currentMeleeDamageBonusVal
				startMeleeDamageBonusValText = L""..wstring.format(L"%.01f", startMeleeDamageBonusVal)                                             
		end

		local currentMeleeDamageBonusValText = L""..wstring.format(L"%.01f", currentMeleeDamageBonusVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentMeleeDamageBonusVal - startMeleeDamageBonusVal)
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentMeleeDamageBonusVal - prevMeleeDamageBonusVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMeleeDamageBonusVal, startMeleeDamageBonusValText, diffStartCurVal, diffStartCurValText, 
														prevMeleeDamageBonusVal, currentMeleeDamageBonusVal, currentMeleeDamageBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseMeleeDamageBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateArmorPenetrationLabel( wndName ) --TODO: Change this over eventually to "new way"?
		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMeleeArmorPenetrationVal = tonumber(CharacterWindow.CalcArmorPenetration())                                 
						startMeleeArmorPenetrationValText = L""..startMeleeArmorPenetrationVal..L"%"    
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MeleeArmorPenetrationVal == nil then
								startMeleeArmorPenetrationVal = tonumber(CharacterWindow.CalcArmorPenetration())     
								startMeleeArmorPenetrationValText = L""..startMeleeArmorPenetrationVal..L"%"
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMeleeArmorPenetrationVal = CaVESWindow.RefValues[serverName][characterName].MeleeArmorPenetrationVal   
								startMeleeArmorPenetrationValText = L""..startMeleeArmorPenetrationVal..L"%"    
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMeleeArmorPenetrationVal = tonumber(CharacterWindow.CalcArmorPenetration())         
				startMeleeArmorPenetrationValText = L""..startMeleeArmorPenetrationVal..L"%"    
		end 
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMeleeArmorPenetrationVal = tonumber(CharacterWindow.CalcArmorPenetration())         
				startMeleeArmorPenetrationValText = L""..startMeleeArmorPenetrationVal..L"%"    
		end
		
		if  prevMeleeArmorPenetrationVal == nil and currentMeleeArmorPenetrationVal == nil then
				currentMeleeArmorPenetrationVal = tonumber(CharacterWindow.CalcArmorPenetration())
				prevMeleeArmorPenetrationVal = currentMeleeArmorPenetrationVal
		else 
				if bResetReferenceValues == false then
						prevMeleeArmorPenetrationVal = currentMeleeArmorPenetrationVal
				end
		end
		
		currentMeleeArmorPenetrationVal = tonumber(CharacterWindow.CalcArmorPenetration())
		local currentMeleeArmorPenetrationValText = L""..currentMeleeArmorPenetrationVal..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentMeleeArmorPenetrationVal - startMeleeArmorPenetrationVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = (wstring.format(L"%.01f",diffStartCurVal))..L"%"
		end

		local diffPrevCurVal = (currentMeleeArmorPenetrationVal - prevMeleeArmorPenetrationVal)
		local diffPrevCurValText = (wstring.format(L"%.01f",diffPrevCurVal))..L"%"
		
		local baseMeleeArmorPenetrationVal = tonumber(CaVESWindow.CalcBaseArmorPenetration())
		
		-- Determine "core" values, these are used in the stat value calculation and other associated
		-- procedures such as the coloring of stat text.  These values are calculated and used so as 
		-- to keep consistency between how the CaVES mod and actual game calculate the same values.
		-- The following "core" stat is Weapon Skill, since Armor Penetration is primarily dependent upon
		-- that "core" stat value.
		local startBaseWeaponSkillVal = currentStatsWeaponSkillVal -- formality, makes following existing function setup easier
		local coreBaseWeaponSkillVal = GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue
		local coreCurrentWeaponSkillVal = currentStatsWeaponSkillVal -- pull this directly from the global variable

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false
		
		CaVESWindow.UpdateLabel( wndName, startMeleeArmorPenetrationVal, startMeleeArmorPenetrationValText, diffStartCurVal, diffStartCurValText, 
														prevMeleeArmorPenetrationVal, currentMeleeArmorPenetrationVal, currentMeleeArmorPenetrationValText, diffPrevCurVal, 
														diffPrevCurValText, baseMeleeArmorPenetrationVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes,
														startBaseWeaponSkillVal, coreBaseWeaponSkillVal, coreCurrentWeaponSkillVal )
end

-- Similar to the function found in characterwindow.lua, but calculates base value.
function CaVESWindow.CalcBaseArmorPenetration()
    return wstring.format(L"%.01f",((GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue/(GameData.Player.battleLevel*7.5+50)*.25)*100.0))
end

function CaVESWindow.UpdateMeleeCriticalHitBonusLabel( wndName )
		local baseMeleeCriticalHitBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMeleeCriticalHitBonusVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_MELEE, baseMeleeCriticalHitBonusVal)
    curMeleeCriticalHitBonusVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE, curMeleeCriticalHitBonusVal)
		
		if  prevMeleeCriticalHitBonusVal == nil and currentMeleeCriticalHitBonusLabelVal == nil then
				currentMeleeCriticalHitBonusVal = curMeleeCriticalHitBonusVal
				prevMeleeCriticalHitBonusVal = currentMeleeCriticalHitBonusVal
		else 
				if bResetReferenceValues == false then
						prevMeleeCriticalHitBonusVal = currentMeleeCriticalHitBonusVal
				end
		end
		
		currentMeleeCriticalHitBonusVal = curMeleeCriticalHitBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMeleeCriticalHitBonusVal = currentMeleeCriticalHitBonusVal
						startMeleeCriticalHitBonusValText = (wstring.format(L"%.01f", startMeleeCriticalHitBonusVal))..L"%"
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MeleeCriticalHitBonusVal == nil then
								startMeleeCriticalHitBonusVal = currentMeleeCriticalHitBonusVal
								startMeleeCriticalHitBonusValText = (wstring.format(L"%.01f", startMeleeCriticalHitBonusVal))..L"%"                    
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMeleeCriticalHitBonusVal = CaVESWindow.RefValues[serverName][characterName].MeleeCriticalHitBonusVal   
								startMeleeCriticalHitBonusValText = (wstring.format(L"%.01f", startMeleeCriticalHitBonusVal))..L"%"   
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMeleeCriticalHitBonusVal = currentMeleeCriticalHitBonusVal
				startMeleeCriticalHitBonusValText = (wstring.format(L"%.01f", startMeleeCriticalHitBonusVal))..L"%"                    
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMeleeCriticalHitBonusVal = currentMeleeCriticalHitBonusVal
				startMeleeCriticalHitBonusValText = (wstring.format(L"%.01f", startMeleeCriticalHitBonusVal))..L"%"                    
		end
		
		local currentMeleeCriticalHitBonusValText = (wstring.format(L"%.01f", currentMeleeCriticalHitBonusVal))..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentMeleeCriticalHitBonusVal - startMeleeCriticalHitBonusVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = (wstring.format(L"%.01f",diffStartCurVal))..L"%"
		end
		
		local diffPrevCurVal = (currentMeleeCriticalHitBonusVal - prevMeleeCriticalHitBonusVal)
		local diffPrevCurValText = (wstring.format(L"%.01f",diffPrevCurVal))..L"%"

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMeleeCriticalHitBonusVal, startMeleeCriticalHitBonusValText, diffStartCurVal, diffStartCurValText, 
														prevMeleeCriticalHitBonusVal, currentMeleeCriticalHitBonusVal, currentMeleeCriticalHitBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseMeleeCriticalHitBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end


---- Ranged Update Functions ----

function CaVESWindow.UpdateRangedWeaponDPSLabel( wndName )
		local baseRangedWeaponDPSVal = 0
		
		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curRangedWeaponDPSVal = tonumber(wstring.format(L"%.01f",CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].dps))

		if prevRangedWeaponDPSVal == nil and currentRangedWeaponDPSVal == nil then
    	if (curRangedWeaponDPSVal > 0) then
        	currentRangedWeaponDPSVal = curRangedWeaponDPSVal
    	else
        	currentRangedWeaponDPSVal = 0
      end
			prevRangedWeaponDPSVal = currentRangedWeaponDPSVal
		else 
				if bResetReferenceValues == false then
						prevRangedWeaponDPSVal = currentRangedWeaponDPSVal
				end
		end

		currentRangedWeaponDPSVal = curRangedWeaponDPSVal
		
		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
			  		if (currentRangedWeaponDPSVal > 0) then
			        	startRangedWeaponDPSVal = currentRangedWeaponDPSVal
			    			startRangedWeaponDPSValText = L""..wstring.format(L"%.01f", startRangedWeaponDPSVal)
			    	else
			        	startRangedWeaponDPSVal = 0
			        	startRangedWeaponDPSValText = L"0.0"
			      end 
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].RangedWeaponDPSVal == nil then
					  		if (CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].dps > 0) then
					        	startRangedWeaponDPSVal = currentRangedWeaponDPSVal
					    			startRangedWeaponDPSValText = L""..wstring.format(L"%.01f", startRangedWeaponDPSVal)
					    	else
					        	startRangedWeaponDPSVal = 0
					        	startRangedWeaponDPSValText = L"0.0"
					      end 
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startRangedWeaponDPSVal = CaVESWindow.RefValues[serverName][characterName].RangedWeaponDPSVal  
								startRangedWeaponDPSValText = L""..wstring.format(L"%.01f", startRangedWeaponDPSVal)    
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
	  		if (currentRangedWeaponDPSVal > 0) then
	        	startRangedWeaponDPSVal = currentRangedWeaponDPSVal
	    			startRangedWeaponDPSValText = L""..wstring.format(L"%.01f", startRangedWeaponDPSVal)
	    	else
	        	startRangedWeaponDPSVal = 0
	        	startRangedWeaponDPSValText = L"0.0"
	      end 
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
	  		if (currentRangedWeaponDPSVal > 0) then
	        	startRangedWeaponDPSVal = currentRangedWeaponDPSVal
	    			startRangedWeaponDPSValText = L""..wstring.format(L"%.01f", startRangedWeaponDPSVal)
	    	else
	        	startRangedWeaponDPSVal = 0
	        	startRangedWeaponDPSValText = L"0.0"
	      end 
		end

		local currentRangedWeaponDPSValText =  wstring.format(L"%.01f", currentRangedWeaponDPSVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentRangedWeaponDPSVal - startRangedWeaponDPSVal)
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)
		end
		
		local diffPrevCurVal = (currentRangedWeaponDPSVal - prevRangedWeaponDPSVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startRangedWeaponDPSVal, startRangedWeaponDPSValText, diffStartCurVal, diffStartCurValText, 
														prevRangedWeaponDPSVal, currentRangedWeaponDPSVal, currentRangedWeaponDPSValText, diffPrevCurVal, 
														diffPrevCurValText, baseRangedWeaponDPSVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateRangedSpeedLabel( wndName )
		local baseRangedSpeedVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local weaponSpeed = CharacterWindow.equipmentData[GameData.EquipSlots.RANGED].speed
		local curRangedSpeedVal = CaVESWindow.CalculateValueWithBonus( GameData.BonusTypes.EBONUS_AUTO_ATTACK_SPEED, weaponSpeed )

		if  prevRangedSpeedVal == nil and currentRangedSpeedVal == nil then
				currentRangedSpeedVal = curRangedSpeedVal
				prevRangedSpeedVal = currentRangedSpeedVal
		else 
				if bResetReferenceValues == false then
						prevRangedSpeedVal = currentRangedSpeedVal
				end
		end

		currentRangedSpeedVal = curRangedSpeedVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startRangedSpeedVal = currentRangedSpeedVal
						startRangedSpeedValText = L""..wstring.format(L"%.01f",startRangedSpeedVal) 
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].RangedSpeedVal == nil then
								startRangedSpeedVal = currentRangedSpeedVal
								startRangedSpeedValText = L""..wstring.format(L"%.01f",startRangedSpeedVal) 
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startRangedSpeedVal = CaVESWindow.RefValues[serverName][characterName].RangedSpeedVal  
								startRangedSpeedValText = L""..wstring.format(L"%.01f", startRangedSpeedVal)    
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startRangedSpeedVal = currentRangedSpeedVal
				startRangedSpeedValText = L""..wstring.format(L"%.01f",startRangedSpeedVal) 
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startRangedSpeedVal = currentRangedSpeedVal
				startRangedSpeedValText = L""..wstring.format(L"%.01f",startRangedSpeedVal) 
		end
		
		local currentRangedSpeedValText = L""..wstring.format(L"%.01f", currentRangedSpeedVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentRangedSpeedVal - startRangedSpeedVal)
				diffStartCurValText = wstring.format(L"%.01f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentRangedSpeedVal - prevRangedSpeedVal)
		local diffPrevCurValText = wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = true

		CaVESWindow.UpdateLabel( wndName, startRangedSpeedVal, startRangedSpeedValText, diffStartCurVal, diffStartCurValText, 
														prevRangedSpeedVal, currentRangedSpeedVal, currentRangedSpeedValText, diffPrevCurVal, 
														diffPrevCurValText, baseRangedSpeedVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateRangedPowerBonusLabel( wndName )
		local baseRangedPowerBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curRangedPowerBonusVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_DAMAGE_RANGED, baseRangedPowerBonusVal)

		if  prevRangedPowerBonusVal == nil and currentRangedPowerBonusVal == nil then
				currentRangedPowerBonusVal = curRangedPowerBonusVal
				prevRangedPowerBonusVal = currentRangedPowerBonusVal
		else 
				if bResetReferenceValues == false then
						prevRangedPowerBonusVal = currentRangedPowerBonusVal
				end
		end

		currentRangedPowerBonusVal = curRangedPowerBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startRangedPowerBonusVal = currentRangedPowerBonusVal                               
						startRangedPowerBonusValText = L""..startRangedPowerBonusVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].RangedPowerBonusVal == nil then
								startRangedPowerBonusVal = currentRangedPowerBonusVal
								startRangedPowerBonusValText = L""..startRangedPowerBonusVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startRangedPowerBonusVal = CaVESWindow.RefValues[serverName][characterName].RangedPowerBonusVal  
								startRangedPowerBonusValText = L""..startRangedPowerBonusVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startRangedPowerBonusVal = currentRangedPowerBonusVal       
				startRangedPowerBonusValText = L""..startRangedPowerBonusVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startRangedPowerBonusVal = currentRangedPowerBonusVal       
				startRangedPowerBonusValText = L""..startRangedPowerBonusVal
		end

		local currentRangedPowerBonusValText = L""..currentRangedPowerBonusVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentRangedPowerBonusVal - startRangedPowerBonusVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentRangedPowerBonusVal - prevRangedPowerBonusVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startRangedPowerBonusVal, startRangedPowerBonusValText, diffStartCurVal, diffStartCurValText, 
														prevRangedPowerBonusVal, currentRangedPowerBonusVal, currentRangedPowerBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseRangedPowerBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateRangedDamageBonusLabel( wndName )
		local baseRangedDamageBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curRangedDamageBonusVal = tonumber(CaVESWindow.GetTotalBonusPower(currentStatsBallisticSkillVal, GameData.BonusTypes.EBONUS_DAMAGE_RANGED))

		if  prevRangedDamageBonusVal == nil and currentRangedDamageBonusVal == nil then
				currentRangedDamageBonusVal = curRangedDamageBonusVal
				prevRangedDamageBonusVal = currentRangedDamageBonusVal
		else 
				if bResetReferenceValues == false then
						prevRangedDamageBonusVal = currentRangedDamageBonusVal
				end
		end

		currentRangedDamageBonusVal = curRangedDamageBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startRangedDamageBonusVal = currentRangedDamageBonusVal                               
						startRangedDamageBonusValText = L""..wstring.format(L"%.01f", startRangedDamageBonusVal)
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].RangedDamageBonusVal == nil then
								startRangedDamageBonusVal = currentRangedDamageBonusVal
								startRangedDamageBonusValText = L""..wstring.format(L"%.01f", startRangedDamageBonusVal)
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startRangedDamageBonusVal = CaVESWindow.RefValues[serverName][characterName].RangedDamageBonusVal  
								startRangedDamageBonusValText = L""..wstring.format(L"%.01f", startRangedDamageBonusVal)
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startRangedDamageBonusVal = currentRangedDamageBonusVal       
				startRangedDamageBonusValText = L""..wstring.format(L"%.01f", startRangedDamageBonusVal)
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startRangedDamageBonusVal = currentRangedDamageBonusVal       
				startRangedDamageBonusValText = L""..startRangedDamageBonusVal
		end

		local currentRangedDamageBonusValText = L""..wstring.format(L"%.01f", currentRangedDamageBonusVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentRangedDamageBonusVal - startRangedDamageBonusVal)
				diffStartCurValText = L""..wstring.format(L"%.01f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentRangedDamageBonusVal - prevRangedDamageBonusVal)
		local diffPrevCurValText = L""..wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startRangedDamageBonusVal, startRangedDamageBonusValText, diffStartCurVal, diffStartCurValText, 
														prevRangedDamageBonusVal, currentRangedDamageBonusVal, currentRangedDamageBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseRangedDamageBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateRangedCriticalHitBonusLabel( wndName )
		local baseRangedCriticalHitBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curRangedCriticalHitBonusVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_RANGED, baseRangedCriticalHitBonusVal)
    curRangedCriticalHitBonusVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE, curRangedCriticalHitBonusVal)

		if  prevRangedCriticalHitBonusVal == nil and currentRangedCriticalHitBonusVal == nil then
				currentRangedCriticalHitBonusVal = curRangedCriticalHitBonusVal
				prevRangedCriticalHitBonusVal = currentRangedCriticalHitBonusVal
		else 
				if bResetReferenceValues == false then
						prevRangedCriticalHitBonusVal = currentRangedCriticalHitBonusVal
				end
		end

		currentRangedCriticalHitBonusVal = curRangedCriticalHitBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startRangedCriticalHitBonusVal = currentRangedCriticalHitBonusVal
						startRangedCriticalHitBonusValText = (wstring.format(L"%.01f", startRangedCriticalHitBonusVal))..L"%"
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].RangedCriticalHitBonusVal == nil then
								startRangedCriticalHitBonusVal = currentRangedCriticalHitBonusVal
								startRangedCriticalHitBonusValText = (wstring.format(L"%.01f", startRangedCriticalHitBonusVal))..L"%"                    
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startRangedCriticalHitBonusVal = CaVESWindow.RefValues[serverName][characterName].RangedCriticalHitBonusVal   
								startRangedCriticalHitBonusValText = (wstring.format(L"%.01f", startRangedCriticalHitBonusVal))..L"%"   
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startRangedCriticalHitBonusVal = currentRangedCriticalHitBonusVal
				startRangedCriticalHitBonusValText = (wstring.format(L"%.01f", startRangedCriticalHitBonusVal))..L"%"                    
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startRangedCriticalHitBonusVal = currentRangedCriticalHitBonusVal
				startRangedCriticalHitBonusValText = (wstring.format(L"%.01f", startRangedCriticalHitBonusVal))..L"%"                    
		end
		
		local currentRangedCriticalHitBonusValText = (wstring.format(L"%.01f", currentRangedCriticalHitBonusVal))..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentRangedCriticalHitBonusVal - startRangedCriticalHitBonusVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = (wstring.format(L"%.01f",diffStartCurVal))..L"%"
		end

		local diffPrevCurVal = (currentRangedCriticalHitBonusVal - prevRangedCriticalHitBonusVal)
		local diffPrevCurValText = (wstring.format(L"%.01f",diffPrevCurVal))..L"%"

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startRangedCriticalHitBonusVal, startRangedCriticalHitBonusValText, diffStartCurVal, diffStartCurValText, 
														prevRangedCriticalHitBonusVal, currentRangedCriticalHitBonusVal, currentRangedCriticalHitBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseRangedCriticalHitBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end


---- Magic Update Functions ----

function CaVESWindow.UpdateMagicPowerBonusLabel( wndName )
		local baseMagicPowerBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMagicPowerBonusVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_DAMAGE_MAGIC, baseMagicPowerBonusVal)

		if  prevMagicPowerBonusVal == nil and currentMagicPowerBonusVal == nil then
				currentMagicPowerBonusVal = curMagicPowerBonusVal
				prevMagicPowerBonusVal = currentMagicPowerBonusVal
		else 
				if bResetReferenceValues == false then
						prevMagicPowerBonusVal = currentMagicPowerBonusVal
				end
		end

		currentMagicPowerBonusVal = curMagicPowerBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMagicPowerBonusVal = currentMagicPowerBonusVal
						startMagicPowerBonusValText = L""..startMagicPowerBonusVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MagicPowerBonusVal    == nil then
								startMagicPowerBonusVal = currentMagicPowerBonusVal
								startMagicPowerBonusValText = L""..startMagicPowerBonusVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMagicPowerBonusVal = CaVESWindow.RefValues[serverName][characterName].MagicPowerBonusVal   
								startMagicPowerBonusValText = L""..startMagicPowerBonusVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMagicPowerBonusVal = currentMagicPowerBonusVal
				startMagicPowerBonusValText = L""..startMagicPowerBonusVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMagicPowerBonusVal = currentMagicPowerBonusVal
				startMagicPowerBonusValText = L""..startMagicPowerBonusVal
		end
		
		local currentMagicPowerBonusValText = L""..currentMagicPowerBonusVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentMagicPowerBonusVal - startMagicPowerBonusVal)
				diffStartCurValText = L""..diffStartCurVal
		end

		local diffPrevCurVal = (currentMagicPowerBonusVal - prevMagicPowerBonusVal)
		local diffPrevCurValText = L""..diffPrevCurVal

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false				

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMagicPowerBonusVal, startMagicPowerBonusValText, diffStartCurVal, diffStartCurValText, 
														prevMagicPowerBonusVal, currentMagicPowerBonusVal, currentMagicPowerBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseMagicPowerBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateMagicDamageBonusLabel( wndName )
		local baseMagicDamageBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMagicDamageBonusVal = tonumber(CaVESWindow.GetTotalBonusPower(currentStatsIntelligenceVal, GameData.BonusTypes.EBONUS_DAMAGE_MAGIC))

		if  prevMagicDamageBonusVal == nil and currentMagicDamageBonusVal == nil then
				currentMagicDamageBonusVal = curMagicDamageBonusVal
				prevMagicDamageBonusVal = currentMagicDamageBonusVal
		else 
				if bResetReferenceValues == false then
						prevMagicDamageBonusVal = currentMagicDamageBonusVal
				end
		end

		currentMagicDamageBonusVal = curMagicDamageBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMagicDamageBonusVal = currentMagicDamageBonusVal
						startMagicDamageBonusValText = L""..wstring.format(L"%.01f", startMagicDamageBonusVal)
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MagicDamageBonusVal    == nil then
								startMagicDamageBonusVal = currentMagicDamageBonusVal
								startMagicDamageBonusValText = L""..wstring.format(L"%.01f", startMagicDamageBonusVal)
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMagicDamageBonusVal = CaVESWindow.RefValues[serverName][characterName].MagicDamageBonusVal   
								startMagicDamageBonusValText = L""..wstring.format(L"%.01f", startMagicDamageBonusVal)
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMagicDamageBonusVal = currentMagicDamageBonusVal
				startMagicDamageBonusValText = L""..wstring.format(L"%.01f", startMagicDamageBonusVal)
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMagicDamageBonusVal = currentMagicDamageBonusVal
				startMagicDamageBonusValText = L""..wstring.format(L"%.01f", startMagicDamageBonusVal)
		end
		
		local currentMagicDamageBonusValText = L""..wstring.format(L"%.01f", currentMagicDamageBonusVal)

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentMagicDamageBonusVal - startMagicDamageBonusVal)
				diffStartCurValText = L""..wstring.format(L"%.01f", diffStartCurVal)
		end

		local diffPrevCurVal = (currentMagicDamageBonusVal - prevMagicDamageBonusVal)
		local diffPrevCurValText = L""..wstring.format(L"%.01f", diffPrevCurVal)

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false				

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMagicDamageBonusVal, startMagicDamageBonusValText, diffStartCurVal, diffStartCurValText, 
														prevMagicDamageBonusVal, currentMagicDamageBonusVal, currentMagicDamageBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseMagicDamageBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateMagicCriticalHitBonusAttackLabel( wndName )
		local baseMagicCriticalHitBonusAttackVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curMagicCriticalHitBonusAttackVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_MAGIC, baseMagicCriticalHitBonusAttackVal)
    curMagicCriticalHitBonusAttackVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE, curMagicCriticalHitBonusAttackVal)

		if  prevMagicCriticalHitBonusAttackVal == nil and currentMagicCriticalHitBonusAttackVal == nil then
				currentMagicCriticalHitBonusAttackVal = curMagicCriticalHitBonusAttackVal
				prevMagicCriticalHitBonusAttackVal = currentMagicCriticalHitBonusAttackVal
		else 
				if bResetReferenceValues == false then
						prevMagicCriticalHitBonusAttackVal = currentMagicCriticalHitBonusAttackVal
				end
		end

		currentMagicCriticalHitBonusAttackVal = curMagicCriticalHitBonusAttackVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMagicCriticalHitBonusAttackVal = currentMagicCriticalHitBonusAttackVal
						startMagicCriticalHitBonusAttackValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusAttackVal))..L"%"              
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MagicCriticalHitBonusAttackVal == nil then
								startMagicCriticalHitBonusAttackVal = currentMagicCriticalHitBonusAttackVal
								startMagicCriticalHitBonusAttackValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusAttackVal))..L"%"              
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMagicCriticalHitBonusAttackVal = CaVESWindow.RefValues[serverName][characterName].MagicCriticalHitBonusAttackVal   
								startMagicCriticalHitBonusAttackValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusAttackVal))..L"%" 
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMagicCriticalHitBonusAttackVal = currentMagicCriticalHitBonusAttackVal
				startMagicCriticalHitBonusAttackValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusAttackVal))..L"%"              
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMagicCriticalHitBonusAttackVal = currentMagicCriticalHitBonusAttackVal
				startMagicCriticalHitBonusAttackValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusAttackVal))..L"%"              
		end

		local currentMagicCriticalHitBonusAttackValText = (wstring.format(L"%.01f", currentMagicCriticalHitBonusAttackVal))..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentMagicCriticalHitBonusAttackVal - startMagicCriticalHitBonusAttackVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = (wstring.format(L"%.01f", diffStartCurVal))..L"%"
		end

		local diffPrevCurVal = (currentMagicCriticalHitBonusAttackVal - prevMagicCriticalHitBonusAttackVal)
		local diffPrevCurValText = (wstring.format(L"%.01f", diffPrevCurVal))..L"%"

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMagicCriticalHitBonusAttackVal, startMagicCriticalHitBonusAttackValText, diffStartCurVal, diffStartCurValText, 
														prevMagicCriticalHitBonusAttackVal, currentMagicCriticalHitBonusAttackVal, currentMagicCriticalHitBonusAttackValText, diffPrevCurVal, 
														diffPrevCurValText, baseMagicCriticalHitBonusAttackVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateMagicHealingPowerBonusLabel( wndName )
		local baseMagicHealingPowerBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMagicHealingPowerBonusVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_HEALING_POWER, baseMagicHealingPowerBonusVal)
		
		if  prevMagicHealingPowerBonusVal == nil and currentMagicHealingPowerBonusVal == nil then
				currentMagicHealingPowerBonusVal = curMagicHealingPowerBonusVal
				prevMagicHealingPowerBonusVal = currentMagicHealingPowerBonusVal
		else 
				if bResetReferenceValues == false then
						prevMagicHealingPowerBonusVal = currentMagicHealingPowerBonusVal
				end
		end

		currentMagicHealingPowerBonusVal = curMagicHealingPowerBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMagicHealingPowerBonusVal = currentMagicHealingPowerBonusVal
						startMagicHealingPowerBonusValText = L""..startMagicHealingPowerBonusVal
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MagicHealingPowerBonusVal == nil then
								startMagicHealingPowerBonusVal = currentMagicHealingPowerBonusVal
								startMagicHealingPowerBonusValText = L""..startMagicHealingPowerBonusVal
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMagicHealingPowerBonusVal = CaVESWindow.RefValues[serverName][characterName].MagicHealingPowerBonusVal  
								startMagicHealingPowerBonusValText = L""..startMagicHealingPowerBonusVal
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMagicHealingPowerBonusVal = currentMagicHealingPowerBonusVal
				startMagicHealingPowerBonusValText = L""..startMagicHealingPowerBonusVal
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMagicHealingPowerBonusVal = currentMagicHealingPowerBonusVal
				startMagicHealingPowerBonusValText = L""..startMagicHealingPowerBonusVal                                         
		end

		local currentMagicHealingPowerBonusValText = L""..currentMagicHealingPowerBonusVal

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0"
		else
				diffStartCurVal = (currentMagicHealingPowerBonusVal - startMagicHealingPowerBonusVal)
				diffStartCurValText = diffStartCurVal
		end

		local diffPrevCurVal = (currentMagicHealingPowerBonusVal - prevMagicHealingPowerBonusVal)
		local diffPrevCurValText = diffPrevCurVal

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMagicHealingPowerBonusVal, startMagicHealingPowerBonusValText, diffStartCurVal, diffStartCurValText, 
														prevMagicHealingPowerBonusVal, currentMagicHealingPowerBonusVal, currentMagicHealingPowerBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseMagicHealingPowerBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateMagicHealingBonusLabel( wndName )
		local baseMagicHealingBonusVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
		local curMagicHealingBonusVal = tonumber(CaVESWindow.GetTotalBonusPower(currentStatsWillpowerVal, GameData.BonusTypes.EBONUS_HEALING_POWER))

		if  prevMagicHealingBonusVal == nil and currentMagicHealingBonusVal == nil then
				currentMagicHealingBonusVal = curMagicHealingBonusVal
				prevMagicHealingBonusVal = currentMagicHealingBonusVal
		else 
				if bResetReferenceValues == false then
						prevMagicHealingBonusVal = currentMagicHealingBonusVal
				end
		end

		currentMagicHealingBonusVal = curMagicHealingBonusVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMagicHealingBonusVal = currentMagicHealingBonusVal
						startMagicHealingBonusValText = (wstring.format(L"%.01f", startMagicHealingBonusVal))              
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MagicHealingBonusVal == nil then
								startMagicHealingBonusVal = currentMagicHealingBonusVal
								startMagicHealingBonusValText = (wstring.format(L"%.01f", startMagicHealingBonusVal))              
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMagicHealingBonusVal = CaVESWindow.RefValues[serverName][characterName].MagicHealingBonusVal
								startMagicHealingBonusValText = (wstring.format(L"%.01f", startMagicHealingBonusVal)) 
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMagicHealingBonusVal = currentMagicHealingBonusVal
				startMagicHealingBonusValText = (wstring.format(L"%.01f", startMagicHealingBonusVal))              
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMagicHealingBonusVal = currentMagicHealingBonusVal
				startMagicHealingBonusValText = (wstring.format(L"%.01f", startMagicHealingBonusVal))             
		end

		local currentMagicHealingBonusValText = (wstring.format(L"%.01f", currentMagicHealingBonusVal))

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0"
		else
				diffStartCurVal = (currentMagicHealingBonusVal - startMagicHealingBonusVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = (wstring.format(L"%.01f", diffStartCurVal))
		end
		
		local diffPrevCurVal = (currentMagicHealingBonusVal - prevMagicHealingBonusVal)
		local diffPrevCurValText = (wstring.format(L"%.01f",diffPrevCurVal))

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMagicHealingBonusVal, startMagicHealingBonusValText, diffStartCurVal, diffStartCurValText, 
														prevMagicHealingBonusVal, currentMagicHealingBonusVal, currentMagicHealingBonusValText, diffPrevCurVal, 
														diffPrevCurValText, baseMagicHealingBonusVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end

function CaVESWindow.UpdateMagicCriticalHitBonusHealLabel( wndName )
		local baseMagicCriticalHitBonusHealVal = 0

		-- Calculated value in a temporary variable due to having to account for previous current stat value.
    local curMagicCriticalHitBonusHealVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE_HEALING, baseMagicCriticalHitBonusHealVal)
    curMagicCriticalHitBonusHealVal = CaVESWindow.CalculateValueWithBonus(GameData.BonusTypes.EBONUS_CRITICAL_HIT_RATE, curMagicCriticalHitBonusHealVal)

		if  prevMagicCriticalHitBonusHealVal == nil and currentMagicCriticalHitBonusHealVal == nil then
				currentMagicCriticalHitBonusHealVal = curMagicCriticalHitBonusHealVal
				prevMagicCriticalHitBonusHealVal = currentMagicCriticalHitBonusHealVal
		else 
				if bResetReferenceValues == false then
						prevMagicCriticalHitBonusHealVal = currentMagicCriticalHitBonusHealVal
				end
		end

		currentMagicCriticalHitBonusHealVal = curMagicCriticalHitBonusHealVal

		-- If this is the first time CaVES has been shown, then set the start or reference
		-- values for the stat.
		if bCaVESInitialization == true then
				if CaVES.Settings.RefStatsValueSetting == 1 then
						-- Set start value to the initial current value.
						startMagicCriticalHitBonusHealVal = currentMagicCriticalHitBonusHealVal
						startMagicCriticalHitBonusHealValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusHealVal))..L"%"              
				elseif CaVES.Settings.RefStatsValueSetting == 2 then
						-- If stat values are to be persistent across sessions, check if no savedvariables value
						-- exists and if not set the start or reference value to the current value and save it.
						if CaVESWindow.RefValues[serverName][characterName].MagicCriticalHitBonusHealVal == nil then
								startMagicCriticalHitBonusHealVal = currentMagicCriticalHitBonusHealVal
								startMagicCriticalHitBonusHealValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusHealVal))..L"%"              
								bSaveRefValues = true
						else
								-- If a savedvariables value exists then set the start or reference value
								-- to the value found in the savedvariables.lua file.
								startMagicCriticalHitBonusHealVal = CaVESWindow.RefValues[serverName][characterName].MagicCriticalHitBonusHealVal   
								startMagicCriticalHitBonusHealValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusHealVal))..L"%" 
						end
				end
		end

		-- If the Reset button was pressed for the Reference values then update the
		-- stat value to the current stat value.
		if bResetReferenceValues == true then
				startMagicCriticalHitBonusHealVal = currentMagicCriticalHitBonusHealVal
				startMagicCriticalHitBonusHealValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusHealVal))..L"%"              
		end
		
		-- Only reset the Reference Stats values when the window is being shown after being hidden,
		-- do not always update it everytime update stats calls the individual stat update function.
		if CaVES.Settings.RefStatsValueSetting == 3 and bRefStatsValueSetting3Reset == true then
				startMagicCriticalHitBonusHealVal = currentMagicCriticalHitBonusHealVal
				startMagicCriticalHitBonusHealValText = (wstring.format(L"%.01f", startMagicCriticalHitBonusHealVal))..L"%"              
		end

		local currentMagicCriticalHitBonusHealValText = (wstring.format(L"%.01f", currentMagicCriticalHitBonusHealVal))..L"%"

		local diffStartCurVal
		local diffStartCurValText

		if bResetReferenceValues == true then 
				diffStartCurVal = 0
				diffStartCurValText = L"0.0%"
		else
				diffStartCurVal = (currentMagicCriticalHitBonusHealVal - startMagicCriticalHitBonusHealVal)
				if (math.abs(diffStartCurVal) <  0.1) then diffStartCurVal = 0.0 end  -- prevent floating point number error
				diffStartCurValText = (wstring.format(L"%.01f", diffStartCurVal))..L"%"
		end
		
		local diffPrevCurVal = (currentMagicCriticalHitBonusHealVal - prevMagicCriticalHitBonusHealVal)
		local diffPrevCurValText = (wstring.format(L"%.01f",diffPrevCurVal))..L"%"

		-- Set stat diminished booleans to false.
		local bIsCurStatDiminished = false
		local bIsRefStatDiminished = false

		-- Set flip color code boolean
		local bFlipColorCodes = false

		CaVESWindow.UpdateLabel( wndName, startMagicCriticalHitBonusHealVal, startMagicCriticalHitBonusHealValText, diffStartCurVal, diffStartCurValText, 
														prevMagicCriticalHitBonusHealVal, currentMagicCriticalHitBonusHealVal, currentMagicCriticalHitBonusHealValText, diffPrevCurVal, 
														diffPrevCurValText, baseMagicCriticalHitBonusHealVal, bIsCurStatDiminished, bIsRefStatDiminished, bFlipColorCodes )
end