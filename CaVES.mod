<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="CaVES" version="2.2" date="06/01/2011" >		 		
    <Author name="Alpha_Male" email="alpha_male@speakeasy.net" />		
    <Description text="Expands and displays player stats in the Character View and displays and tracks stat differences as a result of equipment and other stat related changes." />		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <WARInfo>			
      <Categories>				
        <Category name="ITEMS_INVENTORY" />				
        <Category name="BUFFS_DEBUFFS" />				
        <Category name="CAREER_SPECIFIC" />				
        <Category name="OTHER" />			
      </Categories>			
      <Careers>				
        <Career name="BLACKGUARD" />				
        <Career name="WITCH_ELF" />				
        <Career name="DISCIPLE" />				
        <Career name="SORCERER" />				
        <Career name="IRON_BREAKER" />				
        <Career name="SLAYER" />				
        <Career name="RUNE_PRIEST" />				
        <Career name="ENGINEER" />				
        <Career name="BLACK_ORC" />				
        <Career name="CHOPPA" />				
        <Career name="SHAMAN" />				
        <Career name="SQUIG_HERDER" />				
        <Career name="WITCH_HUNTER" />				
        <Career name="KNIGHT" />				
        <Career name="BRIGHT_WIZARD" />				
        <Career name="WARRIOR_PRIEST" />				
        <Career name="CHOSEN" />				
        <Career name="MARAUDER" />				
        <Career name="ZEALOT" />				
        <Career name="MAGUS" />				
        <Career name="SWORDMASTER" />				
        <Career name="SHADOW_WARRIOR" />				
        <Career name="WHITE_LION" />				
        <Career name="ARCHMAGE" />			
      </Careers>		
    </WARInfo>		
    <Dependencies>			
      <Dependency name="EA_CharacterWindow" />		
    </Dependencies>		
    <Files>			
      <File name="language/CaVESLocalization.lua" />			
      <File name="source/CaVES.lua" />			
      <File name="source/CaVES.xml" />		
    </Files>		
    <SavedVariables>			
      <SavedVariable name="CaVES.Settings" global="true"/>			
      <SavedVariable name="CaVESWindow.RefValues" global="true"/>		
    </SavedVariables>		 		
    <OnInitialize>			
      <CallFunction name="CaVES.Start" />			
      <CreateWindow name="CaVESWindow" show="false" />			
      <CreateWindow name="CaVESWindow_CharacterWindow_Nub" show="false" />		
    </OnInitialize>		
    <OnShutdown>			
      <CallFunction name="CaVES.Shutdown" />		
    </OnShutdown>		
  </UiMod>
</ModuleFile>