--	Title: Character View Expanded Stats (CaVES) v2.2
--
--	Author: Alpha_Male (alpha_male@speakeasy.net)
--
--	Description: Localization File
--
--	Files: \language\CaVESLocalization.lua
--				 \manual\CaVES_Manual.pdf
--				 \source\CaVES.lua
--				 \source\CaVES.xml
--				 \CaVES.mod
--				 \CaVES_Install.txt
--				 \CaVES_Readme.txt
--
--	Version History: 1.0 - Initial Release
--									 1.1 - Fixes for stat background transparency issues caused by WAR 1.2 patch
--											 - Created new tintable background .xml template with preset color and alpha
--											 - New tintable background template allowed removal of lua script that manually 
--												 set color and alpha
--											 - Removed deprecated .xml template for stat category labels
--									 1.2 - Localization updates
--											 - Added German localization
--											 - Added Italian localization
--											 - Added multiple user support when using the Persistent Reference Stats 
--												 option. Values are saved on a unique name per server basis so players
--												 can use the same character name across different servers and the values
--												 will not be overwritten.
--											 - Reworked how initial or starting Reference Stat Values were being handled,
--												 it was possible for persistent or saved values to not get saved upon choosing
--												 this option especially if the game or interface were shutdown abruptly.
--											 - Added additional hooks to handle interface shutdown better, hitting
--												 the close window button (X) when running the game in windowed mode 
--												 is handled now.
--											 - Fixed a global variable that should have been declared locally 
--												 (was not causing any issues but is proper way to set it up).
--											 - Added version to SavedVariables.lua
--											 - Removed some commented out debug lines
--											 - Reorganized variable organization	
--											 - Updated and added additional script comments	
--									 1.3 - Implemented new VersionSettings support for .mod file
--											 - Implemented new "global" optional attribute for SavedVariables used by CaVES. CaVES still 
--												 maintains its multiple characters across multiple servers saved stats and options support
--												 and it is all contained under \Warhammer Online - Age of Reckoning\user\settings\GLOBAL\CaVES. 
--												 Other instances of the SavedVariables.lua file for CaVES and its associated save directory 
--												 can be removed.
--											 - Reworked and updated stat calculations, mirroring changes made to the Character View window 
--												 in the 1.2.1 patch.
--											 - Updated resistances stats color coding introduced in 1.2.1 for diminished resistances stats. 
--												 These values appear as orange when a resistance value has hit the diminished value cap.
--											 - Fixed Defensive stats mouseover tooltips that were broken as a result of changes
--												 made to the Character View window in the 1.2.1 patch.
--											 - Added the ability to filter out or ignore Talismans that have been installed on equipment. 
--												 The option appears on the main CaVES window and its state is saved between sessions.
--											 - Added localization text for currently supported languages for new Ignore Talisman interface strings.
--											 - Created a robust and dedicated supported/compatible mods system for CaVES to allow easier
--												 mod support/compatibility in the future if necessary.
--											 - Fixed function hooks into Character View window to prevent function call chain being possibly 
--												 broken, this is more robust support for CaVES and other mods.	
--											 - Brought over AnywhereTrainer support into new mod compatiblility system
--											 - Added mod support/compatibility for RvRStatsTab by Ermite Chevelu. This should fix any issues
--												 other players were experiencing when having CaVES and this mod installed and enabled.
--											 - Added mod support/compatibility for RvRStatsUpdated. This should fix any issues
--												 other players were experiencing when having CaVES and this mod installed and enabled.
--											 - Changes and additions to declaration of global variables for stats.
--											 - General cleanup and additional script comments.
--											 - Updated CaVES_Manual.pdf and associated images.
--									 1.4 - Fix for Character Viewer window dimension bugs found in v1.3
--											 - Tightened up the bottom frame for the main CaVES window
--											 - Fix for RvRStatsTab support that adjusts Character Viewer window width properly depending on if
--												 AnywhereTrainer is installed/enabled or not installed/not enabled									 
--									 1.5 - Updated stat calculations, mirroring changes made to the Character Viewer in WAR v1.3 update
--											 - Reworked Ignore Talisman functionality due to stat calculation changes in WAR v1.3 update
--											 - Removed deprecated functions, overall due to WAR v1.3 update and CaVES changes, CaVES stat
--												 calculations and display should be faster
--									 1.6 - Floating point display fix for resists stats
--											 - Fixed critical hit bonuses for ranged, magic damage, and magic heal showing as twice their values
--									 1.7 - Added support for 1.3.1 WARInfo Categories and Careers (CaVES.mod file)
--											 - Damage Bonus tooltips working again for both Character View window and CaVES (result of Mythic Fix)
--											 - Fixed bonus value calculations for Melee Damage Bonus
--											 - Fixed bonus value calculations for Ranged Damage Bonus
--											 - Fixed bonus value calculations for Magic Damage Bonus
--											 - Fixed stat value calculation and display for Magic Critical Hit Bonus (Heal)
--											 - Fixed tooltip display for Magic Critical Hit Bonus (Heal)
--											 - Added new Healing Bonus Stat and associated tooltip/mouseover to the Magic Stat section
--											 - Changed color coding for Weapon Speed stat changes (DECREASES are GREEN, INCREASES are RED)
--											 - Fixed Melee Weapon Speed stat calculation to be inline with recent changes (both Right and Left hands)
--											 - Fixed Ranged Weapon Speed stat calculation to be inline with recent changes
--									 1.8 - Updated version to 1.3.2
--											 - New feature, ignore equipped Event item in the stats calculation
--											 - Updated localization for new ignore Event item functionality
--									 1.9 - All stats should now display soft cap color coding properly
--											 - Healing Bonus stat has been updated to no longer be displayed as a percentage
--											 - Fixed Dye Window bottom panel misalignment when CaVES is displayed (result of
--												 latest patch changes to Character View window)
--											 - Fixed additional Dye Window bottom panel misalignment issues when CaVES is displayed and
--												 AnywhereTrainer is also installed (result of latest patch changes to Character View window)
--											 - Fixed tab misalignment for RvRStatsTab when CaVES showing and AnywhereTrainer installed
--									 2.0 - Updated version to 1.3.4
--											 - Changed string formatting for version number display
--									 2.1 - Maintenance Update and Fixes
--											 - Updated version to 1.4.0
--											 - CaVES window breaking Character View window (due to patch related Character View window dimension change)
--											 - Fix for Ignore Talismans bug where items with 2 or more Talisman slots were not having
--												 the Talisman stat values and differences for slots 2 and above properly calculated and displayed
--												 when Ignore Talismans was enabled or disabled
--											 - Fix for potential issue with displaying proper color coded value for Recent stat differences
--											 - Fix for Healing Bonus value not calculating and displaying proper stat and difference values
--											 - Fix for Magic Critical Hit Bonus value not calculating and displaying proper stat and difference values
--											 - 5 New Bonus stats and associated tooltips with new descriptions added for display:
--												 - Fortitude (new in game patch 1.4.0)
--												 - Melee Power
--												 - Ranged Power
--												 - Magic Power
--												 - Healing Power
--									 2.2 - New Features and Bug Fixes
--											 - Updated version to 1.4.3
--											 - Added Options menu item to hide CaVES window when using the Dye Merchant Window
--											 - Fixed effective stat value calculation based on battle level with renown, this fixed some stat calculations/display
--											 - Fixed Weapon Speed mouseover tooltip value due to autoattack haste contribution for both Left and Right Hands
--											 - Optimizations and consolidation of output lines
--											 - Fixes and changes to initialization of Options window and values
--											 - Updates to documentation and manual for new features
--
--	Supported Versions: Warhammer Online v1.4.3
--
--	Dependencies: None
--
--	Addon Compatability: Designed to be compatible with:
--											 - AnywhereTrainer by DarthVon (thedpui02@sneakemail.com)
--											 - RvRStatsTab by ErmiteChevelu (ermite_chevelu@hotmail.com)
--											 - RvRStatsUpdated maintained by Corwynn_Maelstrom, original author Felyza (felyza@gmail.com)
--
--	Future Features: Continued updates for multiple language support.
--
--	Known Issues: None with regards to the localization file.
--
--	Additional Credits: EA/Mythic - Several functions used/duplicated/modified in CaVES are from their core/default scripts,													
--											related functions are with regards to stat calculation and display. 
--											DarthVon (thedpui02@sneakemail.com) - Function used/duplicated from AnywhereTrainer, solved
--											UI window positioning issues and allowed AnywhereTrainer compatibility with CaVES.
--											Thurwell and the DuffTimer team - For simplicity and non-dependency on other mods or libraries,
--											CaVES uses the localization format and functions used by DuffTimer. The variable names have been
--											changed to be specific to the CaVES mod and for personal preference with regards to naming conventions.
--											AmonCarroburg for CaVES German localization
--											Ziomav for CaVES Italian localization
--
--	Special Thanks:	EA/Mythic for a great game and for releasing the API specs
--								  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
--									www.curse.com and www.curseforge.com for hosting WAR mod files and projects
--									Morituri Guild for testing and feedback
--									Ominous Latin Name guild on Iron Rock for testing and feedback
--									Trouble guild on Iron Rock for their support
--
--	License/Disclaimers: This addon and it's relevant parts and files are released under the 
--											 GNU General Public License version 3 (GPLv3).
--
--

------------------------------------------------------------------
----  Global Variables
------------------------------------------------------------------

if not CaVES then
	CaVES = {}
end

CaVES.wstrings = {}

------------------------------------------------------------------
----  ENGLISH
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.ENGLISH] = {
	MOUSEOVER_STATICON = L"Stat Icon",
	MOUSEOVER_STATNAME = L"Stat Name",
	MOUSEOVER_REFSTAT01 = L"Reference Stat changes, based on stat values from when the player character was logged into and entered the game.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",
	MOUSEOVER_REFSTAT02 = L"Reference Stat changes, persistent stat values based on Reference values saved upon choosing this option.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",
	MOUSEOVER_REFSTAT03 = L"Reference Stat changes, based on stat values from when the Character View was opened.  Values reset on open/close of Character View/Expanded Stats.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",	
	MOUSEOVER_RECENTSTAT = L"Recent Stat changes based on the most recent character modification.<BR>Format is: New Value/Value Change",
	TITLE_OPTIONSWINDOW = L"General Options:",
	OPTIONS_OPENWITHCV = L"Expanded Stats Opens with Character View",
	OPTIONS_HIDEWITHDYEMERCHANT = L"Hide Expanded Stats when using Dye Merchant",
	OPTIONS_REFSTATS = L"Reference Stats:",
	OPTIONS_VALUES = L"Values:",
	OPTIONS_VALUES01 = L"Persistent Only for Current Game Session",
	OPTIONS_VALUES02 = L"Persistent Between Game Sessions",     
	OPTIONS_VALUES03 = L"Reset on Character View Open/Close",
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:",
	OPTIONS_TOOLTIPS01 = L"Show Tooltips",
	OPTIONS_TOOLTIPS02 = L"Show Tooltips on Stat Icon Mouse Over Only", 
	TITLE_CAVESWINDOW = L"Expanded Stats",
	HEADER_STAT = L"Stat",
	HEADER_REFERENCE = L"Reference",
	HEADER_RECENT = L"Recent",
	MOUSEOVER_NUB = L"Toggle Character View Expanded Stats",
	TOOLTIP_MELEEWEAPDPS = L"Damage Per Second for the weapon. <BR>",
	TOOLTIP_MELEEWEAPDPSRH = L"Main Hand (Right Hand) Damage: ",
	TOOLTIP_MELEEWEAPDPSLH = L"Off Hand (Left Hand) Damage: ",
	TOOLTIP_MELEEWEAPDPSDESC = L"How many seconds it takes for each auto-attack weapon swing.<BR>",	
	TOOLTIP_MELEEWEAPSPEEDRH = L"Main Hand (Right Hand) Speed: ",
	TOOLTIP_MELEEWEAPSPEEDDESC = L"How many seconds it takes for each weapon attack swing.<BR>",
	TOOLTIP_MELEEWEAPSPEEDLH = L"Off Hand (Left Hand) Speed: ",
	TOOLTIP_FORTITUDEDESC = L"Fortitude adds damage reduction in addition to that provided by Toughness. Fortitude is unaffected by Toughness debuffs or the Toughness stat softcap.<BR>",
	TOOLTIP_MELEEPOWERDESC = L"Increases Melee Damage in addition to that provided by Strength, including Standard and Auto Attack damage. Unlike Strength, Melee Power does not help reduce opponents chance to block or parry attacks. Melee Power damage bonus contributions are unaffected by Strength debuffs or by the Strength stat value soft cap.<BR>",
	TOOLTIP_RANGEDPOWERDESC = L"Increases Ranged Damage in addition that to provided by Ballistic Skill. Unlike Ballistic Skill, Ranged Power does not help reduce opponents chance to block or evade attacks. Ranged Power damage bonus contributions are unaffected by Ballistic Skill debuffs or by the Ballistic Skill stat value soft cap.<BR>",
	TOOLTIP_MAGICPOWERDESC = L"Increases Magic Attack Damage in addition to that provided by Intelligence. Unlike Intelligence, Magic Power does not help reduce opponents chance to block or disrupt attacks. Magic Power damage bonus contributions are unaffected by Intelligence debuffs or by the Intelligence stat value soft cap.<BR>",
	TOOLTIP_HEALINGPOWERDESC = L"Increases Healing done to allies in addition to that provided by Willpower. Unlike Willpower, Healing Power does not help increase your chance to disrupt attacks. Healing Power bonus contributions are unaffected by Willpower debuffs or by the Willpower stat value soft cap.<BR>",
	TITLE_IGNORE = L"Ignore:",
	OPTIONS_IGNORETALISMANS = L"Talismans",
	OPTIONS_IGNOREEVENT = L"Event Item",
}

------------------------------------------------------------------
----  FRENCH
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.FRENCH] = {
	MOUSEOVER_STATICON = L"Stat Icon", -- Localization Required
	MOUSEOVER_STATNAME = L"Stat Name", -- Localization Required
	MOUSEOVER_REFSTAT01 = L"Reference Stat changes, based on stat values from when the player character was logged into and entered the game.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT02 = L"Reference Stat changes, persistent stat values based on Reference values saved upon choosing this option.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT03 = L"Reference Stat changes, based on stat values from when the Character View was opened.  Values reset on open/close of Character View/Expanded Stats.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",	-- Localization Required
	MOUSEOVER_RECENTSTAT = L"Recent Stat changes based on the most recent character modification.<BR>Format is: New Value/Value Change",
	TITLE_OPTIONSWINDOW = L"General Options:", -- Localization Required
	OPTIONS_OPENWITHCV = L"Expanded Stats Opens with Character View", -- Localization Required
	OPTIONS_HIDEWITHDYEMERCHANT = L"Hide Expanded Stats when using Dye Merchant",  -- Localization Required
	OPTIONS_REFSTATS = L"Reference Stats:", -- Localization Required
	OPTIONS_VALUES = L"Values:", -- Localization Required
	OPTIONS_VALUES01 = L"Persistent Only for Current Game Session", -- Localization Required
	OPTIONS_VALUES02 = L"Persistent Between Game Sessions", -- Localization Required     
	OPTIONS_VALUES03 = L"Reset on Character View Open/Close", -- Localization Required
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:", -- Localization Required
	OPTIONS_TOOLTIPS01 = L"Show Tooltips", -- Localization Required
	OPTIONS_TOOLTIPS02 = L"Show Tooltips on Stat Icon Mouse Over Only", -- Localization Required
	TITLE_CAVESWINDOW = L"Expanded Stats", -- Localization Required
	HEADER_STAT = L"Stat", -- Localization Required
	HEADER_REFERENCE = L"Reference", -- Localization Required
	HEADER_RECENT = L"Recent", -- Localization Required
	MOUSEOVER_NUB = L"Toggle Character View Expanded Stats", -- Localization Required
	TOOLTIP_MELEEWEAPDPS = L"Damage Per Second for the weapon. <BR>", -- Localization Required
	TOOLTIP_MELEEWEAPDPSRH = L"Main Hand (Right Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSLH = L"Off Hand (Left Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSDESC = L"How many seconds it takes for each auto-attack weapon swing.<BR>",	-- Localization Required
	TOOLTIP_MELEEWEAPSPEEDRH = L"Main Hand (Right Hand) Speed: ", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDDESC = L"How many seconds it takes for each weapon attack swing.<BR>", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDLH = L"Off Hand (Left Hand) Speed: ", -- Localization Required
	TOOLTIP_FORTITUDEDESC = L"Fortitude adds damage reduction in addition to that provided by Toughness. Fortitude is unaffected by Toughness debuffs or the Toughness stat softcap.<BR>", -- Localization Required
	TOOLTIP_MELEEPOWERDESC = L"Increases Melee Damage in addition to that provided by Strength, including Standard and Auto Attack damage. Unlike Strength, Melee Power does not help reduce opponents chance to block or parry attacks. Melee Power damage bonus contributions are unaffected by Strength debuffs or by the Strength stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_RANGEDPOWERDESC = L"Increases Ranged Damage in addition that to provided by Ballistic Skill. Unlike Ballistic Skill, Ranged Power does not help reduce opponents chance to block or evade attacks. Ranged Power damage bonus contributions are unaffected by Ballistic Skill debuffs or by the Ballistic Skill stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_MAGICPOWERDESC = L"Increases Magic Attack Damage in addition to that provided by Intelligence. Unlike Intelligence, Magic Power does not help reduce opponents chance to block or disrupt attacks. Magic Power damage bonus contributions are unaffected by Intelligence debuffs or by the Intelligence stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_HEALINGPOWERDESC = L"Increases Healing done to allies in addition to that provided by Willpower. Unlike Willpower, Healing Power does not help increase your chance to disrupt attacks. Healing Power bonus contributions are unaffected by Willpower debuffs or by the Willpower stat value soft cap.<BR>", -- Localization Required
	TITLE_IGNORE = L"Ignore:", -- Localization Required
	OPTIONS_IGNORETALISMANS = L"Talismans", -- Localization Required
	OPTIONS_IGNOREEVENT = L"Event Item", -- Localization Required
}

------------------------------------------------------------------
----  GERMAN
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.GERMAN] = {
	MOUSEOVER_STATICON = L"Stat-Symbol",
	MOUSEOVER_STATNAME = L"Stat-Name",
	MOUSEOVER_REFSTAT01 = L"Referenz-Stat �ndert sich auf den Wert, den der Charakter beim Einloggen und Betreten des Spiels hatte.<BR><BR>Das Format ist: Anfangswert/Aktuelle �nderung verglichen mit dem Anfangswert",
	MOUSEOVER_REFSTAT02 = L"Referenz-Stat �ndert sich auf den Wert, den der Charakter beim Aktivieren der Option hatte.<BR><BR>Das Format ist: Anfangswert/Aktuelle �nderung verglichen mit dem Anfangswert",
	MOUSEOVER_REFSTAT03 = L"Referenz-Stat �ndert sich auf den Wert, den der Charakter �ffnen der Charakteransicht hatte. Die Werte werden beim �ffnen/Schlie�en der Charakteransicht/Erweiterten Stats zur�ckgesetzt.<BR><BR>Das Format ist: Anfangswert/Aktuelle �nderung verglichen mit dem Anfangswert",
	MOUSEOVER_RECENTSTAT = L"Aktualwert �ndert sich auf die letzte Charaktermodifikation.<BR>Das Format ist: Neuer Wert/Wert�nderung",
	TITLE_OPTIONSWINDOW = L"Allgemeine Optionen:",
	OPTIONS_OPENWITHCV = L"Erweitere Stats �ffnen sich mit der Charakteransicht",
	OPTIONS_HIDEWITHDYEMERCHANT = L"Fell geerweiterter Notfall, wenn F�rbungs-Kaufmann verwendet wird",
	OPTIONS_REFSTATS = L"Referenz Stats:",
	OPTIONS_VALUES = L"Werte:",
	OPTIONS_VALUES01 = L"Dauerhaftigkeit auf die aktuelle Spielsitzung beschr�nkt",
	OPTIONS_VALUES02 = L"Dauerhaftigkeit �ber mehrere Spielsitzungen gew�hrt",
	OPTIONS_VALUES03 = L"R�cksetzen beim �ffnen/Schlie�en der Charakteransicht",
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:",
	OPTIONS_TOOLTIPS01 = L"Zeige Tooltips",
	OPTIONS_TOOLTIPS02 = L"Zeige Tooltips nur, wenn die Maus auf dem Stat-Symbol liegt",
	TITLE_CAVESWINDOW = L"Erweiterte Stats",
	HEADER_STAT = L"Stat",
	HEADER_REFERENCE = L"Referenz",
	HEADER_RECENT = L"Aktuell",
	MOUSEOVER_NUB = L"Benutze Charakteransicht mit erweiterten Stats",
	TOOLTIP_MELEEWEAPDPS = L"Schade pro Sekunde f�r die Waffe. <BR>",
	TOOLTIP_MELEEWEAPDPSRH = L"Schaden der Haupthand (rechte Hand):",
	TOOLTIP_MELEEWEAPDPSLH = L"Schaden der Nebenhand (linke Hand):",
	TOOLTIP_MELEEWEAPDPSDESC = L"Wieviele Sekunden zwischen den automatischen Schl�gen mit der Waffe vergehen.<BR>",
	TOOLTIP_MELEEWEAPSPEEDRH = L"Geschwindigkeit der Haupthand (rechte Hand): ",
	TOOLTIP_MELEEWEAPSPEEDDESC = L"Wieviele Sekunden zwischen den Schl�gen mit der Waffe vergehen.<BR>",
	TOOLTIP_MELEEWEAPSPEEDLH = L"Geschwindigkeit der Nebenhand (linke Hand):",
	TOOLTIP_FORTITUDEDESC = L"Kraft f�gt Schadenverkleinerung zus�tzlich zu der hinzu, die von Toughness zur Verf�gung gestellt wird. Kraft ist durch Toughness debuffs oder das H�rtenotfall softcap unber�hrt.<BR>",
	TOOLTIP_MELEEPOWERDESC = L"Erh�ht Handgemenge-Schaden zus�tzlich zu dem, der durch Strength, einschlie�lich Standard- und Selbstangriffsschaden bereitgestellt wird. Anders als St�rke hilft Handgemenge-Energie nicht, Konkurrenten chance zu verringern, um Angriffe zu blockieren oder abzuwehren. Handgemenge-Energienschaden-Pr�mienbeitr�ge sind durch Strength debuffs oder durch die weiche Kappe des St�rkennotfall-Wertes unber�hrt.<BR>",
	TOOLTIP_RANGEDPOWERDESC = L"Zunahmen erstreckten sich Schaden zus�tzlich das zur Verf�gung gestellt von der ballistischen F�higkeit. Anders als ballistische F�higkeit hilft erstreckte Energie nicht, Konkurrenten chance zu verringern, um Angriffe zu blockieren oder auszuweichen. Erstreckte Energienschaden-Pr�mienbeitr�ge sind durch ballistische F�higkeit debuffs oder durch die weiche Kappe des ballistischen F�higkeitsnotfall-Wertes unber�hrt.<BR>",
	TOOLTIP_MAGICPOWERDESC = L"Zunahme-stellte magischer Angriffs-Schaden zus�tzlich zu dem von Intelligence zur Verf�gung. Anders als Intelligenz hilft magische Energie nicht, Konkurrenten chance zu verringern, um Angriffe zu blockieren oder zu st�ren. Schadenpr�mienbeitr�ge der magischen Energie sind durch Intelligence debuffs oder durch die weiche Kappe des Intelligenznotfall-Wertes unber�hrt.<BR>",
	TOOLTIP_HEALINGPOWERDESC = L"Erh�ht das Heilen erfolgt auf Verb�ndete zus�tzlich zu dem, das durch Willpower bereitgestellt wird. Anders als Willenskraft hilft heilende Energie nicht, Ihre Wahrscheinlichkeit zu erh�hen, Angriffe zu st�ren. Heilende Energienpr�mienbeitr�ge sind durch Willpower debuffs oder durch die weiche Kappe des Willenskraftnotfall-Wertes unber�hrt.<BR>",
	TITLE_IGNORE = L"Ignorieren Sie:",
	OPTIONS_IGNORETALISMANS = L"Talismans",
	OPTIONS_IGNOREEVENT = L"Ereignis-Einzelteil",
}

------------------------------------------------------------------
----  ITALIAN
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.ITALIAN] = {
	MOUSEOVER_STATICON = L"Icona statistica",
	MOUSEOVER_STATNAME = L"Nome statistica",
	MOUSEOVER_REFSTAT01 = L"Cambiamenti alle statistiche di riferimento, basati sui valori del personaggio all'accesso nel gioco.<BR><BR>Formato: valore iniziale/cambiamento recente rispetto al valore iniziale",
	MOUSEOVER_REFSTAT02 = L"Cambiamenti alle statistiche di riferimento, valori persistenti basati sui valori di riferimento salvati attraverso quest'opzione.<BR><BR>Formato: valore iniziale/cambiamento recente rispetto al valore iniziale",
	MOUSEOVER_REFSTAT03 = L"Cambiamenti alle statistiche di riferimento, basati sui valori del personaggio all'apertura della scheda personaggio. I valori si azzerano con l'apertura/chiusura della scheda personaggio/statistiche complete.<BR><BR>Formato: valore iniziale/cambiamento recente rispetto al valore iniziale",
	MOUSEOVER_RECENTSTAT = L"Cambiamenti alle statistiche recenti basati sulle ultime modifiche al personaggio..<BR>Formato: nuovo valore/cambiamento del valore",
	TITLE_OPTIONSWINDOW = L"Opzioni generali:",
	OPTIONS_OPENWITHCV = L"Stat. complete aperte con scheda personaggio",
	OPTIONS_HIDEWITHDYEMERCHANT = L"Stats ampliato pellame quando usando il commerciante della tintura",
	OPTIONS_REFSTATS = L"Statistiche di riferimento:",
	OPTIONS_VALUES = L"Valori:",
	OPTIONS_VALUES01 = L"Persistente solo per questa sessione",
	OPTIONS_VALUES02 = L"Persistente tra le sessioni di gioco",
	OPTIONS_VALUES03 = L"Azzera all'apertura/chiusura scheda PG",
	OPTIONS_TOOLTIPS = L"Suggerimenti sulle info delle statistiche:",
	OPTIONS_TOOLTIPS01 = L"Mostra suggerimenti",
	OPTIONS_TOOLTIPS02 = L"Mostra sugg. solo con cursore sull'icona stat.",
	TITLE_CAVESWINDOW = L"Statistiche Complete",
	HEADER_STAT = L"Statistica",
	HEADER_REFERENCE = L"Riferimento",
	HEADER_RECENT = L"Recente",
	MOUSEOVER_NUB = L"Apri statistiche complete del personaggio",
	TOOLTIP_MELEEWEAPDPS = L"Danni al secondo per l'arma. <BR>",
	TOOLTIP_MELEEWEAPDPSRH = L"Danno arma primaria (mano destra):",
	TOOLTIP_MELEEWEAPDPSLH = L"Danno arma secondaria (mano sinistra):",
	TOOLTIP_MELEEWEAPDPSDESC = L"Secondi intercorsi tra i colpi di auto-attacco.<BR>",
	TOOLTIP_MELEEWEAPSPEEDRH = L"Velocit� arma primaria (mano destra):",
	TOOLTIP_MELEEWEAPSPEEDDESC = L"Secondi intercorsi tra i colpi dell'arma.<BR>",
	TOOLTIP_MELEEWEAPSPEEDLH = L"Velocit� arma secondaria (mano sinistra):",
	TOOLTIP_FORTITUDEDESC = L"Il coraggio aggiunge la riduzione di danno oltre che quella fornita dalla Toughness. Il coraggio � inalterato dai debuffs della Toughness o dal softcap di stat di durezza.<BR>",
	TOOLTIP_MELEEPOWERDESC = L"Aumenta il danno della mischia oltre che quello fornito da Strength, compreso danno standard ed auto di attacco. Diverso di resistenza, il potere della mischia non contribuisce a ridurre gli avversari per chance per ostruire o parry gli attacchi. I contributi di indennit� di danno di potere della mischia sono inalterati dai debuffs della Strength o dalla protezione molle di valore di stat di resistenza.<BR>",
	TOOLTIP_RANGEDPOWERDESC = L"Gli aumenti hanno variato danneggiamento in pi� quello del fornito di da abilit� balistica. Diverso di abilit� balistica, il potere variato non contribuisce a ridurre gli avversari per chance per ostruire o eludere gli attacchi. I contributi variati di indennit� di danno di potere sono inalterati dai debuffs balistici di abilit� o dalla protezione molle di abilit� di valore balistico di stat.<BR>",
	TOOLTIP_MAGICPOWERDESC = L"Il danno magico di attacco di aumenti oltre che quello ha fornito da Intelligence. Diverso di intelligenza, il potere magico non contribuisce a ridurre gli avversari per chance per ostruire o interrompere gli attacchi. I contributi di indennit� di danno di potere magico sono inalterati dai debuffs dell'Intelligence o dalla protezione molle di valore di stat di intelligenza.<BR>",
	TOOLTIP_HEALINGPOWERDESC = L"Aumenta la guarigione fatta agli alleati oltre che quella fornita da Willpower. Diverso della forza di volont�, il potere curativo non contribuisce ad aumentare la vostra probabilit� interrompere gli attacchi. I contributi curativi di indennit� di potere sono inalterati dai debuffs della Willpower o dalla protezione molle di valore di stat di forza di volont�.<BR>",
	TITLE_IGNORE = L"Ignori:",
	OPTIONS_IGNORETALISMANS = L"Talismani",
	OPTIONS_IGNOREEVENT = L"Articolo di evento",
}

------------------------------------------------------------------
----  SPANISH
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.SPANISH] = {
	MOUSEOVER_STATICON = L"Stat Icon", -- Localization Required
	MOUSEOVER_STATNAME = L"Stat Name", -- Localization Required
	MOUSEOVER_REFSTAT01 = L"Reference Stat changes, based on stat values from when the player character was logged into and entered the game.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT02 = L"Reference Stat changes, persistent stat values based on Reference values saved upon choosing this option.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT03 = L"Reference Stat changes, based on stat values from when the Character View was opened.  Values reset on open/close of Character View/Expanded Stats.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",	-- Localization Required
	MOUSEOVER_RECENTSTAT = L"Recent Stat changes based on the most recent character modification.<BR>Format is: New Value/Value Change",
	TITLE_OPTIONSWINDOW = L"General Options:", -- Localization Required
	OPTIONS_OPENWITHCV = L"Expanded Stats Opens with Character View", -- Localization Required
	OPTIONS_HIDEWITHDYEMERCHANT = L"Hide Expanded Stats when using Dye Merchant", -- Localization Required
	OPTIONS_REFSTATS = L"Reference Stats:", -- Localization Required
	OPTIONS_VALUES = L"Values:", -- Localization Required
	OPTIONS_VALUES01 = L"Persistent Only for Current Game Session", -- Localization Required
	OPTIONS_VALUES02 = L"Persistent Between Game Sessions", -- Localization Required     
	OPTIONS_VALUES03 = L"Reset on Character View Open/Close", -- Localization Required
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:", -- Localization Required
	OPTIONS_TOOLTIPS01 = L"Show Tooltips", -- Localization Required
	OPTIONS_TOOLTIPS02 = L"Show Tooltips on Stat Icon Mouse Over Only", -- Localization Required
	TITLE_CAVESWINDOW = L"Expanded Stats", -- Localization Required
	HEADER_STAT = L"Stat", -- Localization Required
	HEADER_REFERENCE = L"Reference", -- Localization Required
	HEADER_RECENT = L"Recent", -- Localization Required
	MOUSEOVER_NUB = L"Toggle Character View Expanded Stats", -- Localization Required
	TOOLTIP_MELEEWEAPDPS = L"Damage Per Second for the weapon. <BR>", -- Localization Required
	TOOLTIP_MELEEWEAPDPSRH = L"Main Hand (Right Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSLH = L"Off Hand (Left Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSDESC = L"How many seconds it takes for each auto-attack weapon swing.<BR>",	-- Localization Required
	TOOLTIP_MELEEWEAPSPEEDRH = L"Main Hand (Right Hand) Speed: ", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDDESC = L"How many seconds it takes for each weapon attack swing.<BR>", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDLH = L"Off Hand (Left Hand) Speed: ", -- Localization Required
	TOOLTIP_FORTITUDEDESC = L"Fortitude adds damage reduction in addition to that provided by Toughness. Fortitude is unaffected by Toughness debuffs or the Toughness stat softcap.<BR>", -- Localization Required
	TOOLTIP_MELEEPOWERDESC = L"Increases Melee Damage in addition to that provided by Strength, including Standard and Auto Attack damage. Unlike Strength, Melee Power does not help reduce opponents chance to block or parry attacks. Melee Power damage bonus contributions are unaffected by Strength debuffs or by the Strength stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_RANGEDPOWERDESC = L"Increases Ranged Damage in addition that to provided by Ballistic Skill. Unlike Ballistic Skill, Ranged Power does not help reduce opponents chance to block or evade attacks. Ranged Power damage bonus contributions are unaffected by Ballistic Skill debuffs or by the Ballistic Skill stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_MAGICPOWERDESC = L"Increases Magic Attack Damage in addition to that provided by Intelligence. Unlike Intelligence, Magic Power does not help reduce opponents chance to block or disrupt attacks. Magic Power damage bonus contributions are unaffected by Intelligence debuffs or by the Intelligence stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_HEALINGPOWERDESC = L"Increases Healing done to allies in addition to that provided by Willpower. Unlike Willpower, Healing Power does not help increase your chance to disrupt attacks. Healing Power bonus contributions are unaffected by Willpower debuffs or by the Willpower stat value soft cap.<BR>", -- Localization Required
	TITLE_IGNORE = L"Ignore:", -- Localization Required
	OPTIONS_IGNORETALISMANS = L"Talismans", -- Localization Required
	OPTIONS_IGNOREEVENT = L"Event Item", -- Localization Required
}

------------------------------------------------------------------
----  KOREAN
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.KOREAN] = {
	MOUSEOVER_STATICON = L"Stat Icon", -- Localization Required
	MOUSEOVER_STATNAME = L"Stat Name", -- Localization Required
	MOUSEOVER_REFSTAT01 = L"Reference Stat changes, based on stat values from when the player character was logged into and entered the game.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT02 = L"Reference Stat changes, persistent stat values based on Reference values saved upon choosing this option.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT03 = L"Reference Stat changes, based on stat values from when the Character View was opened.  Values reset on open/close of Character View/Expanded Stats.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",	-- Localization Required
	MOUSEOVER_RECENTSTAT = L"Recent Stat changes based on the most recent character modification.<BR>Format is: New Value/Value Change",
	TITLE_OPTIONSWINDOW = L"General Options:", -- Localization Required
	OPTIONS_OPENWITHCV = L"Expanded Stats Opens with Character View", -- Localization Required
	OPTIONS_HIDEWITHDYEMERCHANT = L"Hide Expanded Stats when using Dye Merchant", -- Localization Required
	OPTIONS_REFSTATS = L"Reference Stats:", -- Localization Required
	OPTIONS_VALUES = L"Values:", -- Localization Required
	OPTIONS_VALUES01 = L"Persistent Only for Current Game Session", -- Localization Required
	OPTIONS_VALUES02 = L"Persistent Between Game Sessions", -- Localization Required     
	OPTIONS_VALUES03 = L"Reset on Character View Open/Close", -- Localization Required
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:", -- Localization Required
	OPTIONS_TOOLTIPS01 = L"Show Tooltips", -- Localization Required
	OPTIONS_TOOLTIPS02 = L"Show Tooltips on Stat Icon Mouse Over Only", -- Localization Required
	TITLE_CAVESWINDOW = L"Expanded Stats", -- Localization Required
	HEADER_STAT = L"Stat", -- Localization Required
	HEADER_REFERENCE = L"Reference", -- Localization Required
	HEADER_RECENT = L"Recent", -- Localization Required
	MOUSEOVER_NUB = L"Toggle Character View Expanded Stats", -- Localization Required
	TOOLTIP_MELEEWEAPDPS = L"Damage Per Second for the weapon. <BR>", -- Localization Required
	TOOLTIP_MELEEWEAPDPSRH = L"Main Hand (Right Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSLH = L"Off Hand (Left Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSDESC = L"How many seconds it takes for each auto-attack weapon swing.<BR>",	-- Localization Required
	TOOLTIP_MELEEWEAPSPEEDRH = L"Main Hand (Right Hand) Speed: ", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDDESC = L"How many seconds it takes for each weapon attack swing.<BR>", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDLH = L"Off Hand (Left Hand) Speed: ", -- Localization Required
	TOOLTIP_FORTITUDEDESC = L"Fortitude adds damage reduction in addition to that provided by Toughness. Fortitude is unaffected by Toughness debuffs or the Toughness stat softcap.<BR>", -- Localization Required
	TOOLTIP_MELEEPOWERDESC = L"Increases Melee Damage in addition to that provided by Strength, including Standard and Auto Attack damage. Unlike Strength, Melee Power does not help reduce opponents chance to block or parry attacks. Melee Power damage bonus contributions are unaffected by Strength debuffs or by the Strength stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_RANGEDPOWERDESC = L"Increases Ranged Damage in addition that to provided by Ballistic Skill. Unlike Ballistic Skill, Ranged Power does not help reduce opponents chance to block or evade attacks. Ranged Power damage bonus contributions are unaffected by Ballistic Skill debuffs or by the Ballistic Skill stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_MAGICPOWERDESC = L"Increases Magic Attack Damage in addition to that provided by Intelligence. Unlike Intelligence, Magic Power does not help reduce opponents chance to block or disrupt attacks. Magic Power damage bonus contributions are unaffected by Intelligence debuffs or by the Intelligence stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_HEALINGPOWERDESC = L"Increases Healing done to allies in addition to that provided by Willpower. Unlike Willpower, Healing Power does not help increase your chance to disrupt attacks. Healing Power bonus contributions are unaffected by Willpower debuffs or by the Willpower stat value soft cap.<BR>", -- Localization Required
	TITLE_IGNORE = L"Ignore:", -- Localization Required
	OPTIONS_IGNORETALISMANS = L"Talismans", -- Localization Required
	OPTIONS_IGNOREEVENT = L"Event Item", -- Localization Required
}

------------------------------------------------------------------
----  S_CHINESE
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.S_CHINESE] = {
	MOUSEOVER_STATICON = L"Stat Icon", -- Localization Required
	MOUSEOVER_STATNAME = L"Stat Name", -- Localization Required
	MOUSEOVER_REFSTAT01 = L"Reference Stat changes, based on stat values from when the player character was logged into and entered the game.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT02 = L"Reference Stat changes, persistent stat values based on Reference values saved upon choosing this option.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT03 = L"Reference Stat changes, based on stat values from when the Character View was opened.  Values reset on open/close of Character View/Expanded Stats.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",	-- Localization Required
	MOUSEOVER_RECENTSTAT = L"Recent Stat changes based on the most recent character modification.<BR>Format is: New Value/Value Change",
	TITLE_OPTIONSWINDOW = L"General Options:", -- Localization Required
	OPTIONS_OPENWITHCV = L"Expanded Stats Opens with Character View", -- Localization Required
	OPTIONS_HIDEWITHDYEMERCHANT = L"Hide Expanded Stats when using Dye Merchant", -- Localization Required
	OPTIONS_REFSTATS = L"Reference Stats:", -- Localization Required
	OPTIONS_VALUES = L"Values:", -- Localization Required
	OPTIONS_VALUES01 = L"Persistent Only for Current Game Session", -- Localization Required
	OPTIONS_VALUES02 = L"Persistent Between Game Sessions", -- Localization Required     
	OPTIONS_VALUES03 = L"Reset on Character View Open/Close", -- Localization Required
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:", -- Localization Required
	OPTIONS_TOOLTIPS01 = L"Show Tooltips", -- Localization Required
	OPTIONS_TOOLTIPS02 = L"Show Tooltips on Stat Icon Mouse Over Only", -- Localization Required
	TITLE_CAVESWINDOW = L"Expanded Stats", -- Localization Required
	HEADER_STAT = L"Stat", -- Localization Required
	HEADER_REFERENCE = L"Reference", -- Localization Required
	HEADER_RECENT = L"Recent", -- Localization Required
	MOUSEOVER_NUB = L"Toggle Character View Expanded Stats", -- Localization Required
	TOOLTIP_MELEEWEAPDPS = L"Damage Per Second for the weapon. <BR>", -- Localization Required
	TOOLTIP_MELEEWEAPDPSRH = L"Main Hand (Right Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSLH = L"Off Hand (Left Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSDESC = L"How many seconds it takes for each auto-attack weapon swing.<BR>",	-- Localization Required
	TOOLTIP_MELEEWEAPSPEEDRH = L"Main Hand (Right Hand) Speed: ", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDDESC = L"How many seconds it takes for each weapon attack swing.<BR>", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDLH = L"Off Hand (Left Hand) Speed: ", -- Localization Required
	TOOLTIP_FORTITUDEDESC = L"Fortitude adds damage reduction in addition to that provided by Toughness. Fortitude is unaffected by Toughness debuffs or the Toughness stat softcap.<BR>", -- Localization Required
	TOOLTIP_MELEEPOWERDESC = L"Increases Melee Damage in addition to that provided by Strength, including Standard and Auto Attack damage. Unlike Strength, Melee Power does not help reduce opponents chance to block or parry attacks. Melee Power damage bonus contributions are unaffected by Strength debuffs or by the Strength stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_RANGEDPOWERDESC = L"Increases Ranged Damage in addition that to provided by Ballistic Skill. Unlike Ballistic Skill, Ranged Power does not help reduce opponents chance to block or evade attacks. Ranged Power damage bonus contributions are unaffected by Ballistic Skill debuffs or by the Ballistic Skill stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_MAGICPOWERDESC = L"Increases Magic Attack Damage in addition to that provided by Intelligence. Unlike Intelligence, Magic Power does not help reduce opponents chance to block or disrupt attacks. Magic Power damage bonus contributions are unaffected by Intelligence debuffs or by the Intelligence stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_HEALINGPOWERDESC = L"Increases Healing done to allies in addition to that provided by Willpower. Unlike Willpower, Healing Power does not help increase your chance to disrupt attacks. Healing Power bonus contributions are unaffected by Willpower debuffs or by the Willpower stat value soft cap.<BR>", -- Localization Required
	TITLE_IGNORE = L"Ignore:", -- Localization Required
	OPTIONS_IGNORETALISMANS = L"Talismans", -- Localization Required
	OPTIONS_IGNOREEVENT = L"Event Item", -- Localization Required
}

------------------------------------------------------------------
----  T_CHINESE
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.T_CHINESE] = {
	MOUSEOVER_STATICON = L"Stat Icon", -- Localization Required
	MOUSEOVER_STATNAME = L"Stat Name", -- Localization Required
	MOUSEOVER_REFSTAT01 = L"Reference Stat changes, based on stat values from when the player character was logged into and entered the game.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT02 = L"Reference Stat changes, persistent stat values based on Reference values saved upon choosing this option.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT03 = L"Reference Stat changes, based on stat values from when the Character View was opened.  Values reset on open/close of Character View/Expanded Stats.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",	-- Localization Required
	MOUSEOVER_RECENTSTAT = L"Recent Stat changes based on the most recent character modification.<BR>Format is: New Value/Value Change",
	TITLE_OPTIONSWINDOW = L"General Options:", -- Localization Required
	OPTIONS_OPENWITHCV = L"Expanded Stats Opens with Character View", -- Localization Required
	OPTIONS_HIDEWITHDYEMERCHANT = L"Hide Expanded Stats when using Dye Merchant", -- Localization Required
	OPTIONS_REFSTATS = L"Reference Stats:", -- Localization Required
	OPTIONS_VALUES = L"Values:", -- Localization Required
	OPTIONS_VALUES01 = L"Persistent Only for Current Game Session", -- Localization Required
	OPTIONS_VALUES02 = L"Persistent Between Game Sessions", -- Localization Required     
	OPTIONS_VALUES03 = L"Reset on Character View Open/Close", -- Localization Required
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:", -- Localization Required
	OPTIONS_TOOLTIPS01 = L"Show Tooltips", -- Localization Required
	OPTIONS_TOOLTIPS02 = L"Show Tooltips on Stat Icon Mouse Over Only", -- Localization Required
	TITLE_CAVESWINDOW = L"Expanded Stats", -- Localization Required
	HEADER_STAT = L"Stat", -- Localization Required
	HEADER_REFERENCE = L"Reference", -- Localization Required
	HEADER_RECENT = L"Recent", -- Localization Required
	MOUSEOVER_NUB = L"Toggle Character View Expanded Stats", -- Localization Required
	TOOLTIP_MELEEWEAPDPS = L"Damage Per Second for the weapon. <BR>", -- Localization Required
	TOOLTIP_MELEEWEAPDPSRH = L"Main Hand (Right Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSLH = L"Off Hand (Left Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSDESC = L"How many seconds it takes for each auto-attack weapon swing.<BR>",	-- Localization Required
	TOOLTIP_MELEEWEAPSPEEDRH = L"Main Hand (Right Hand) Speed: ", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDDESC = L"How many seconds it takes for each weapon attack swing.<BR>", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDLH = L"Off Hand (Left Hand) Speed: ", -- Localization Required
	TOOLTIP_FORTITUDEDESC = L"Fortitude adds damage reduction in addition to that provided by Toughness. Fortitude is unaffected by Toughness debuffs or the Toughness stat softcap.<BR>", -- Localization Required
	TOOLTIP_MELEEPOWERDESC = L"Increases Melee Damage in addition to that provided by Strength, including Standard and Auto Attack damage. Unlike Strength, Melee Power does not help reduce opponents chance to block or parry attacks. Melee Power damage bonus contributions are unaffected by Strength debuffs or by the Strength stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_RANGEDPOWERDESC = L"Increases Ranged Damage in addition that to provided by Ballistic Skill. Unlike Ballistic Skill, Ranged Power does not help reduce opponents chance to block or evade attacks. Ranged Power damage bonus contributions are unaffected by Ballistic Skill debuffs or by the Ballistic Skill stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_MAGICPOWERDESC = L"Increases Magic Attack Damage in addition to that provided by Intelligence. Unlike Intelligence, Magic Power does not help reduce opponents chance to block or disrupt attacks. Magic Power damage bonus contributions are unaffected by Intelligence debuffs or by the Intelligence stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_HEALINGPOWERDESC = L"Increases Healing done to allies in addition to that provided by Willpower. Unlike Willpower, Healing Power does not help increase your chance to disrupt attacks. Healing Power bonus contributions are unaffected by Willpower debuffs or by the Willpower stat value soft cap.<BR>", -- Localization Required
	TITLE_IGNORE = L"Ignore:", -- Localization Required
	OPTIONS_IGNORETALISMANS = L"Talismans", -- Localization Required
	OPTIONS_IGNOREEVENT = L"Event Item", -- Localization Required
}

------------------------------------------------------------------
----  JAPANESE
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.JAPANESE] = {
	MOUSEOVER_STATICON = L"Stat Icon", -- Localization Required
	MOUSEOVER_STATNAME = L"Stat Name", -- Localization Required
	MOUSEOVER_REFSTAT01 = L"Reference Stat changes, based on stat values from when the player character was logged into and entered the game.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT02 = L"Reference Stat changes, persistent stat values based on Reference values saved upon choosing this option.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT03 = L"Reference Stat changes, based on stat values from when the Character View was opened.  Values reset on open/close of Character View/Expanded Stats.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",	-- Localization Required
	MOUSEOVER_RECENTSTAT = L"Recent Stat changes based on the most recent character modification.<BR>Format is: New Value/Value Change",
	TITLE_OPTIONSWINDOW = L"General Options:", -- Localization Required
	OPTIONS_OPENWITHCV = L"Expanded Stats Opens with Character View", -- Localization Required
	OPTIONS_HIDEWITHDYEMERCHANT = L"Hide Expanded Stats when using Dye Merchant", -- Localization Required
	OPTIONS_REFSTATS = L"Reference Stats:", -- Localization Required
	OPTIONS_VALUES = L"Values:", -- Localization Required
	OPTIONS_VALUES01 = L"Persistent Only for Current Game Session", -- Localization Required
	OPTIONS_VALUES02 = L"Persistent Between Game Sessions", -- Localization Required     
	OPTIONS_VALUES03 = L"Reset on Character View Open/Close", -- Localization Required
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:", -- Localization Required
	OPTIONS_TOOLTIPS01 = L"Show Tooltips", -- Localization Required
	OPTIONS_TOOLTIPS02 = L"Show Tooltips on Stat Icon Mouse Over Only", -- Localization Required
	TITLE_CAVESWINDOW = L"Expanded Stats", -- Localization Required
	HEADER_STAT = L"Stat", -- Localization Required
	HEADER_REFERENCE = L"Reference", -- Localization Required
	HEADER_RECENT = L"Recent", -- Localization Required
	MOUSEOVER_NUB = L"Toggle Character View Expanded Stats", -- Localization Required
	TOOLTIP_MELEEWEAPDPS = L"Damage Per Second for the weapon. <BR>", -- Localization Required
	TOOLTIP_MELEEWEAPDPSRH = L"Main Hand (Right Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSLH = L"Off Hand (Left Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSDESC = L"How many seconds it takes for each auto-attack weapon swing.<BR>",	-- Localization Required
	TOOLTIP_MELEEWEAPSPEEDRH = L"Main Hand (Right Hand) Speed: ", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDDESC = L"How many seconds it takes for each weapon attack swing.<BR>", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDLH = L"Off Hand (Left Hand) Speed: ", -- Localization Required
	TOOLTIP_FORTITUDEDESC = L"Fortitude adds damage reduction in addition to that provided by Toughness. Fortitude is unaffected by Toughness debuffs or the Toughness stat softcap.<BR>", -- Localization Required
	TOOLTIP_MELEEPOWERDESC = L"Increases Melee Damage in addition to that provided by Strength, including Standard and Auto Attack damage. Unlike Strength, Melee Power does not help reduce opponents chance to block or parry attacks. Melee Power damage bonus contributions are unaffected by Strength debuffs or by the Strength stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_RANGEDPOWERDESC = L"Increases Ranged Damage in addition that to provided by Ballistic Skill. Unlike Ballistic Skill, Ranged Power does not help reduce opponents chance to block or evade attacks. Ranged Power damage bonus contributions are unaffected by Ballistic Skill debuffs or by the Ballistic Skill stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_MAGICPOWERDESC = L"Increases Magic Attack Damage in addition to that provided by Intelligence. Unlike Intelligence, Magic Power does not help reduce opponents chance to block or disrupt attacks. Magic Power damage bonus contributions are unaffected by Intelligence debuffs or by the Intelligence stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_HEALINGPOWERDESC = L"Increases Healing done to allies in addition to that provided by Willpower. Unlike Willpower, Healing Power does not help increase your chance to disrupt attacks. Healing Power bonus contributions are unaffected by Willpower debuffs or by the Willpower stat value soft cap.<BR>", -- Localization Required
	TITLE_IGNORE = L"Ignore:", -- Localization Required
	OPTIONS_IGNORETALISMANS = L"Talismans", -- Localization Required
	OPTIONS_IGNOREEVENT = L"Event Item", -- Localization Required
}

------------------------------------------------------------------
----  RUSSIAN
------------------------------------------------------------------

CaVES.wstrings[SystemData.Settings.Language.RUSSIAN] = {
	MOUSEOVER_STATICON = L"Stat Icon", -- Localization Required
	MOUSEOVER_STATNAME = L"Stat Name", -- Localization Required
	MOUSEOVER_REFSTAT01 = L"Reference Stat changes, based on stat values from when the player character was logged into and entered the game.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT02 = L"Reference Stat changes, persistent stat values based on Reference values saved upon choosing this option.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value", -- Localization Required
	MOUSEOVER_REFSTAT03 = L"Reference Stat changes, based on stat values from when the Character View was opened.  Values reset on open/close of Character View/Expanded Stats.<BR><BR>Format is: Start Value/Recent Change Compared to Start Value",	-- Localization Required
	MOUSEOVER_RECENTSTAT = L"Recent Stat changes based on the most recent character modification.<BR>Format is: New Value/Value Change",
	TITLE_OPTIONSWINDOW = L"General Options:", -- Localization Required
	OPTIONS_OPENWITHCV = L"Expanded Stats Opens with Character View", -- Localization Required
	OPTIONS_HIDEWITHDYEMERCHANT = L"Hide Expanded Stats when using Dye Merchant", -- Localization Required
	OPTIONS_REFSTATS = L"Reference Stats:", -- Localization Required
	OPTIONS_VALUES = L"Values:", -- Localization Required
	OPTIONS_VALUES01 = L"Persistent Only for Current Game Session", -- Localization Required
	OPTIONS_VALUES02 = L"Persistent Between Game Sessions", -- Localization Required     
	OPTIONS_VALUES03 = L"Reset on Character View Open/Close", -- Localization Required
	OPTIONS_TOOLTIPS = L"Stat Info Tooltips:", -- Localization Required
	OPTIONS_TOOLTIPS01 = L"Show Tooltips", -- Localization Required
	OPTIONS_TOOLTIPS02 = L"Show Tooltips on Stat Icon Mouse Over Only", -- Localization Required
	TITLE_CAVESWINDOW = L"Expanded Stats", -- Localization Required
	HEADER_STAT = L"Stat", -- Localization Required
	HEADER_REFERENCE = L"Reference", -- Localization Required
	HEADER_RECENT = L"Recent", -- Localization Required
	MOUSEOVER_NUB = L"Toggle Character View Expanded Stats", -- Localization Required
	TOOLTIP_MELEEWEAPDPS = L"Damage Per Second for the weapon. <BR>", -- Localization Required
	TOOLTIP_MELEEWEAPDPSRH = L"Main Hand (Right Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSLH = L"Off Hand (Left Hand) Damage: ", -- Localization Required
	TOOLTIP_MELEEWEAPDPSDESC = L"How many seconds it takes for each auto-attack weapon swing.<BR>",	-- Localization Required
	TOOLTIP_MELEEWEAPSPEEDRH = L"Main Hand (Right Hand) Speed: ", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDDESC = L"How many seconds it takes for each weapon attack swing.<BR>", -- Localization Required
	TOOLTIP_MELEEWEAPSPEEDLH = L"Off Hand (Left Hand) Speed: ", -- Localization Required
	TOOLTIP_FORTITUDEDESC = L"Fortitude adds damage reduction in addition to that provided by Toughness. Fortitude is unaffected by Toughness debuffs or the Toughness stat softcap.<BR>", -- Localization Required
	TOOLTIP_MELEEPOWERDESC = L"Increases Melee Damage in addition to that provided by Strength, including Standard and Auto Attack damage. Unlike Strength, Melee Power does not help reduce opponents chance to block or parry attacks. Melee Power damage bonus contributions are unaffected by Strength debuffs or by the Strength stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_RANGEDPOWERDESC = L"Increases Ranged Damage in addition that to provided by Ballistic Skill. Unlike Ballistic Skill, Ranged Power does not help reduce opponents chance to block or evade attacks. Ranged Power damage bonus contributions are unaffected by Ballistic Skill debuffs or by the Ballistic Skill stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_MAGICPOWERDESC = L"Increases Magic Attack Damage in addition to that provided by Intelligence. Unlike Intelligence, Magic Power does not help reduce opponents chance to block or disrupt attacks. Magic Power damage bonus contributions are unaffected by Intelligence debuffs or by the Intelligence stat value soft cap.<BR>", -- Localization Required
	TOOLTIP_HEALINGPOWERDESC = L"Increases Healing done to allies in addition to that provided by Willpower. Unlike Willpower, Healing Power does not help increase your chance to disrupt attacks. Healing Power bonus contributions are unaffected by Willpower debuffs or by the Willpower stat value soft cap.<BR>", -- Localization Required
	TITLE_IGNORE = L"Ignore:", -- Localization Required
	OPTIONS_IGNORETALISMANS = L"Talismans", -- Localization Required
	OPTIONS_IGNOREEVENT = L"Event Item", -- Localization Required
}