Installing CaVES mod for Warhammer Online: Age of Reckoning
------------------------------------------------------------------------

The following instructions assume a "manual" install.  If extracting
from a .zip file, please ensure that the directory structure is
maintained.

For information on how to automatically install and update this mod,
please visit http://www.curse.com and http://war.curse.com/ to learn
about using the Curse Client for automatic updates.

For additional help and support with CaVES please visit 
http://war.curse.com/downloads/war-addons/details/caves.aspx

Installation
--------------------------------

The files can be extracted into your Warhammer Online directory 
(e.g. C:\Program Files\Warhammer Online - Age of Reckoning\) or can
be extracted elsewhere and copied over to the Warhammer Online directory.

  1) Go to your Warhammer Online folder.
     (e.g. C:\Program Files\Warhammer Online - Age of Reckoning\)

  2) Go into the Interface sub-directory or sub-folder.  If an Interface
  	 sub-directory or folder does not exist, create it.

  3) Go into the AddOns sub-directory or sub-folder.  If an AddOns
  	 sub-directory or folder does not exist, create it.

  4) Copy/Extract the CaVES mod folder into the AddOns folder.
  
  5) If you have done everything correctly, the folder should look like
  	 the following example:  
  	 
  	 C:\Program Files\Warhammer Online - Age of Reckoning\Interface\Addons\CaVES.
  	 
  6) Underneath the CaVES directory should be the following files:

 		 \language\CaVESLocalization.lua
 		 \manual\CaVES_Manual.pdf
 		 \source\CaVES.lua
 		 \source\CaVES.xml
 		 \CaVES.mod
 		 \CaVES_Install.txt (the file you are reading)
 		 \CaVES_Readme.txt

For more information and an overview of the features and capabilities of the CaVES mod,
please read the CaVES_Readme.txt.  Additional information about the mod can also be
found in the comment header in the .lua files (\source\CaVES.lua and \language\CaVESLocalization.lua).

For a complete guide on how to use CaVES and an explanation of its interface,
read the included manual, \manual\CaVES_Manual.pdf.

