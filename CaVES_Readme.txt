Title: Character View Expanded Stats (CaVES) v2.2

Author: Alpha_Male (alpha_male@speakeasy.net)

Description:  Character View Expanded Stats (CaVES) takes the five stats category groups that can only be viewed one
							at a time in the standard Character View and displays them all at once in a single window.  This 
							allows players to see the many stats that change and are affected by a single or multiple equipment 
							changes as well as other stat modification events. CaVES is a perfect tool for all classes and all  
							levels, allowing easier optimizing of all character stats.

							CaVES adds a small unobtrusive toggle button or nub to the upper right corner of the Character View.
							CaVES is able to be opened and closed using this nub, or it can be configured to show automatically 
							when the Character View is shown.

							The original stats window is not removed or replaced and CaVES duplicates all of the functionality 
							of the original stats window, such as tooltips, and makes several improvements and enhancements. 
							Enhancements includes the display and color coding of difference values that show whether the stat has 
							made a + or - change.  Additional features include a set of reference stats which allows players to make 
							an analysis	of the effects of their equipment and stat changes compared to a set of stat values that are 
							saved depending on the user selected options.

Features: * Ability to be easily shown/hidden
					* Recent(Current) Stat Value and Difference Display
					* Reference Stat Value and Difference Display
				 	* Context sensitive tooltips for Reference Value description based on user selected options
					* Stat tooltips and mouseovers from Character View also work on stats displayed in CaVES
					* Color coding for stat values and difference values, including resistance caps
					* +/- symbols display for easier readability and for players that have color blindness
					* Configurable User Options
							* Show CaVES on Character View display
							* Stat Reference value persistency settings
							* General and selective display of tooltips
					* Supports Persistent Stat Reference values saved for multiple characters across multiple servers
					* Right and left hand melee weapons stats have been separated into their own stats (4 total now). This
						makes these stat values easier to read.
							* Right Hand DPS
							* Right Hand Speed
							* Left Hand DPS
							* Left Hand Speed
							* New/updated tooltip descriptions for all of these stats
					* Displays "power" stats and associated differences, including associated tooltip descriptions and 
						calculated contributions for the following:
							* Fortitude
							* Melee Power
							* Ranged Power
							* Magic Power
							* Healing Power
					* Reference and Recent(Current) stat display reset buttons
					* Ability to filter out or ignore Talismans installed on gear and equipment. This allows more accurate
						comparisons of equipped gear and equipment or different load out configurations.
					* Ability to filter out or ignore equipped Event slot item.
					* Preserves original Character View stats window and functionality
					* Compatible with Character View Dye Merchant, Brags, and Dungeon countdown windows/tabs
					* Compatible with and supports the following addons:
							* AnywhereTrainer
							* RvRStatsTab
							* RvRStatsUpdated
					* Multiple Language Support (currently includes English, German, and Italian)
					* Includes User Manual

Files:	\language\CaVESLocalization.lua
 		 		\manual\CaVES_Manual.pdf
 				\source\CaVES.lua
 		 		\source\CaVES.xml
 		 		\CaVES.mod
 		 		\CaVES_Install.txt 
 		 		\CaVES_Readme.txt (the file you are reading)

			
Version History: 1.0 - Initial Release
								 1.1 - Fixes for stat background transparency issues caused by WAR 1.2 patch
										 - Created new tintable background .xml template with preset color and alpha
										 - New tintable background template allowed removal of lua script that manually set color and alpha
										 - Removed deprecated .xml template for stat category labels	
								 1.2 - Localization updates
										 - Added German localization
										 - Added Italian localization
										 - Added multiple character support when using the Persistent Reference Stats 
											 option. Values are saved on a unique name per server basis so players
											 can use the same character name across different servers and the values
											 will not be overwritten.
										 - Reworked how initial or starting Reference Stat Values were being handled,
											 it was possible for persistent or saved values to not get saved upon choosing
											 this option especially if the game or interface were shutdown abruptly.
										 - Added additional hooks to handle interface shutdown better, hitting
											 the close window button (X) when running the game in windowed mode 
											 is handled now.
										 - Fixed a global variable that should have been declared locally 
											 (was not causing any issues but is proper way to set it up)
										 - Added version to SavedVariables.lua
										 - Removed some commented out debug lines
										 - Reorganized variable organization	
										 - Updated and added additional script comments
								 1.3 - Implemented new VersionSettings support for .mod file
										 - Implemented new "global" optional attribute for SavedVariables used by CaVES. CaVES still 
											 maintains its multiple characters across multiple servers saved stats and options support
											 and it is all contained under \Warhammer Online - Age of Reckoning\user\settings\GLOBAL\CaVES. 
											 Other instances of the SavedVariables.lua file for CaVES and its associated save directory 
											 can be removed.
										 - Reworked and updated stat calculations, mirroring changes made to the Character View window 
											 in the 1.2.1 patch.
										 - Updated resistances stats color coding introduced in 1.2.1 for diminished resistances stats. 
											 These values appear as orange when a resistance value has hit the diminished value cap.
										 - Fixed Defensive stats mouseover tooltips that were broken as a result of changes
											 made to the Character View window in the 1.2.1 patch.
										 - Added the ability to filter out or ignore Talismans that have been installed on equipment. 
											 The option appears on the main CaVES window and its state is saved between sessions.
										 - Added localization text for currently supported languages for new Ignore Talisman interface strings.
										 - Created a robust and dedicated supported/compatible mods system for CaVES to allow easier
											 mod support/compatibility in the future if necessary.
										 - Fixed function hooks into Character View window to prevent function call chain being possibly 
											 broken, this is more robust support for CaVES and other mods.	
										 - Brought over AnywhereTrainer support into new mod compatiblility system
										 - Added mod support/compatibility for RvRStatsTab by Ermite Chevelu. This should fix any issues
											 other players were experiencing when having CaVES and this mod installed and enabled.
										 - Added mod support/compatibility for RvRStatsUpdated. This should fix any issues
											 other players were experiencing when having CaVES and this mod installed and enabled.
										 - Changes and additions to declaration of global variables for stats.
										 - General cleanup and additional script comments.
										 - Updated CaVES_Manual.doc and associated images.
								 1.4 - Fix for Character Viewer window dimension bugs found in v1.3
										 - Tightened up the bottom frame for the main CaVES window
										 - Fix for RvRStatsTab support that adjusts Character Viewer window width properly depending on if
											 AnywhereTrainer is installed/enabled or not installed/not enabled
								 1.5 - Updated stat calculations, mirroring changes made to the Character Viewer in WAR v1.3 update
										 - Reworked Ignore Talisman functionality due to stat calculation changes in WAR v1.3 update
										 - Removed deprecated functions, overall due to WAR v1.3 update and CaVES changes, CaVES stat
											 calculations and display should be faster
								 1.6 - Floating point display fix for resists stats
										 - Fixed critical hit bonuses for ranged, magic damage, and magic heal showing as twice their values
								 1.7 - Added support for 1.3.1 WARInfo Categories and Careers (CaVES.mod file)
										 - Damage Bonus tooltips working again for both Character View window and CaVES (result of Mythic Fix)
										 - Fixed bonus value calculations for Melee Damage Bonus
										 - Fixed bonus value calculations for Ranged Damage Bonus
										 - Fixed bonus value calculations for Magic Damage Bonus
										 - Fixed stat value calculation and display for Magic Critical Hit Bonus (Heal)
										 - Fixed tooltip display for Magic Critical Hit Bonus (Heal)
								     - Added new Healing Bonus Stat and associated tooltip/mouseover to the Magic Stat section
										 - Changed color coding for Weapon Speed stat changes (DECREASES are GREEN, INCREASES are RED)
										 - Fixed Melee Weapon Speed stat calculation to be inline with recent changes (both Right and Left hands)
										 - Fixed Ranged Weapon Speed stat calculation to be inline with recent changes
								 1.8 - Updated version to 1.3.2
										 - New feature, ignore equipped Event item in the stats calculation
										 - Updated localization for new ignore Event item functionality
								 1.9 - All stats should now display soft cap color coding properly
								 	 	 - Healing Bonus stat has been updated to no longer be displayed as a percentage
								 	 	 - Fixed Dye Window bottom panel misalignment when CaVES is displayed (result of
								 			 latest patch changes to Character View window)
								 	 	 - Fixed additional Dye Window bottom panel misalignment issues when CaVES is displayed and
								 		 	 AnywhereTrainer is also installed (result of latest patch changes to Character View window)
								 	 	 - Fixed tab misalignment for RvRStatsTab when CaVES showing and AnywhereTrainer installed
								 2.0 - Updated version to 1.3.4
										 - Changed string formatting for version number display
								 2.1 - Maintenance Update and Fixes
								 		 - Updated version to 1.4.0
								 		 - CaVES window breaking Character View window (due to patch related Character View window dimension change)
								 		 - Fix for Ignore Talismans bug where items with 2 or more Talisman slots were not having
								 		   the Talisman stat values and differences for slots 2 and above properly calculated and displayed
								 		   when Ignore Talismans was enabled or disabled
								 		 - Fix for potential issue with displaying proper color coded value for Recent stat differences
								 		 - Fix for Healing Bonus value not calculating and displaying proper stat and difference values
								 		 - Fix for Magic Critical Hit Bonus value not calculating and displaying proper stat and difference values
										 - 5 New Bonus stats and associated tooltips with new descriptions added for display:
										 	 - Fortitude (new in game patch 1.4.0)
										 	 - Melee Power
										 	 - Ranged Power
										 	 - Magic Power
										 	 - Healing Power
								 2.2 - New Features and Bug Fixes
										 - Updated version to 1.4.3
										 - Added Options menu item to hide CaVES window when using the Dye Merchant Window
										 - Fixed effective stat value calculation based on battle level with renown, this fixed some stat calculations/display
										 - Fixed Weapon Speed mouseover tooltip value due to autoattack haste contribution for both Left and Right Hands
										 - Optimizations and consolidation of output lines
										 - Fixes and changes to initialization of Options window and values
										 - Updates to documentation and manual for new features

Supported Versions: Warhammer Online v1.4.3

Dependencies: None

Addon Compatability: Designed to be compatible with:
									 	   - AnywhereTrainer by DarthVon (thedpui02@sneakemail.com)
											 - RvRStatsTab by ErmiteChevelu (ermite_chevelu@hotmail.com)
											 - RvRStatsUpdated maintained by Corwynn_Maelstrom, original author Felyza (felyza@gmail.com)

Future Features: Continued updates for multiple language support

Known Issues:  Minor issue.  The CaVES window is a popup window.  This was necessary in order to allow compatibility 
							 with other mods, allow proper spacing of other activated windows such as the the character's
							 item bags, and allows the Character View window to never block the use of CaVES or take
							 focus away.  As a result, CaVES will draw over top of other windows such as the map, etc...  The dye
							 window for the Character View window does this also, probably for many of the same reasons.

Additional Credits: EA/Mythic - Several functions used/duplicated/modified in CaVES are from their core/default scripts,
										related functions are with regards to stat calculation and display. 
										DarthVon (thedpui02@sneakemail.com) - Function used/duplicated from AnywhereTrainer, solved
										UI window positioning issues and allowed AnywhereTrainer compatibility with CaVES.
										Thurwell and the DuffTimer team - For simplicity and non-dependency on other mods or libraries,
										CaVES uses the localization format and functions used by DuffTimer. The variable names have been 
										changed to be specific to the CaVES mod and for personal preference with regards to naming conventions.
										AmonCarroburg for CaVES German localization
										Ziomav for CaVES Italian localization

Special Thanks:	EA/Mythic for a great game and for releasing the API specs
							  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
								www.curse.com and www.curseforge.com for hosting WAR mod files and projects
								Morituri Guild for testing and feedback
								Ominous Latin Name guild on Iron Rock for testing and feedback
								Trouble guild on Iron Rock for support

License/Disclaimers: This addon and it's relevant parts and files are released under the 
										 GNU General Public License version 3 (GPLv3).

Additional Notes:  The above information about the addon can also be found in the comment header in the .lua files 
(\source\CaVES.lua and \language\CaVESLocalization.lua).

For a complete guide on how to use CaVES and an explanation of its interface, read the included manual, CaVES_Manual.pdf
found under the \manual folder in the AddOns directory where CaVES is installed.

For additional help and support with CaVES please visit http://war.curse.com/downloads/war-addons/details/caves.aspx